<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						HERZ オリジナルレザー
						<span class="header-eng">ORIGINAL LEATHER</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br /><br />
				
				<div class="cblk-1">
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/leather/img1.png" />	
					</div>
					
					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h">
							堅牢で革本来の風合いを活かした革
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/leather/img2.png" />
						<p>
							これがHERZの求める理想の革です。そのために多くの時間と手間を必要としますが、素材を厳選し、植物タンニンでなめし、出来るだけ少ない表面加工を行い、素顔に近い革に仕上げています。
      実際に作り手がタンナーと何度も話し合いをし、試作を重ねていく中で完成したHERZオリジナルレザーです。
						</p>
						<div class="clear-both"></div>
					</div>

					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/leather/img3.png" />
						<p class="dotted">
							※タンナーとは <br/>
      皮から革に作り上げる職人さんたちのことをタンナーと呼びます。動物の皮を鞣（なめ）して、腐らないように処理し、素材として使える革に仕上げる。<br/>
      HERZの革製品を作る上でも欠かせない存在です。
						</p>
						<div class="clear-both"></div>
					</div>

					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h">
							HERZで使用している革は大きく分けて2種類
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					<header class="header-content">
						<h3>
							ラティーゴ（ハードレザー）
						</h3>
					</header>
					<br/><br/>
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/leather/img4.png" alt="" />	
							<br/><br/>
						<p>
							スタンダードなスムースレザー。堅牢で厚みがあるのが特徴。ラティーゴレザーの中でも、ハードとソフトレザータイプがあります。どんな鞄にも対応できるように厚みを何種類か分けてお作りしています。
						</p>
					</div>
					
					
					<br/><br/>
					<header class="header-content">
						<h3>
							スターレ（ソフトレザー）
						</h3>
					</header>
					<br/><br/>
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/leather/img5.png" alt="" />	
							<br/><br/>
						<p>
							革本来のシボ・シワが特徴的なソフトレザー。シボの入り具合も一枚の革の中でも全く異なります。革質はやわらかく厚みは、ラティーゴレザーに比べ薄く仕上げています。
						</p>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							革色は5色の中から選んでお作りいただけます
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
					
						<div class="lc-model mt20">
							<div class="lc-model__item">
							<img src="images/leather/img6.png" alt=""/>
							<div class="lc-model__cpt mt10">キャメル</div>
							</div>

							<div class="lc-model__item">
							<img src="images/leather/img7.png" alt=""/>
							<div class="lc-model__cpt mt10">キャメル</div>
							</div>

							<div class="lc-model__item">
							<img src="images/leather/img8.png" alt=""/>
							<div class="lc-model__cpt mt10">キャメル</div>
							</div>

							<div class="lc-model__item">
							<img src="images/leather/img9.png" alt=""/>
							<div class="lc-model__cpt mt10">キャメル</div>
							</div>

							<div class="lc-model__item">
							<img src="images/leather/img10.png" alt=""/>
							<div class="lc-model__cpt mt10">キャメル</div>
							</div>
						</div>
						<p class="f15 mt25"><strong>※革の染色も、職人が手作業で行なっているため、同じ革色でもその時によって、色味の違いが生じます。</strong></p>
					</div>	
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h3>
							キャメル
						</h3>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						  <div class="lprod ml15" >
						   <div class="prod-item">
							<div class="prod-item__thumb"><img src="images/leather/img11.png" alt=""/></div>
							<p class="prod-item__dtl"style="margin-left:-20px; padding-top: 4px;">革：ラティーゴ</p>
						   </div>

						   <div class="prod-item">
							<div class="prod-item__thumb"><img src="images/leather/img12.png" alt=""/></div>
							<p class="prod-item__dtl"style="margin-left: 23px; padding-top: 0px;">革：スターレ</p>
						   </div>
						  </div>
					</div>

					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h3>
							チョコ
						</h3>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
					
					  <div class="lprod ml20">
					   <div class="prod-item">
						<div class="prod-item__thumb"><img src="images/leather/img13.png" alt=""/></div>
						<p class="prod-item__dtl " style="margin-left: -25px;">革：ラティーゴ</p>
					   </div> 

					   <div class="prod-item">
						<div class="prod-item__thumb"><img src="images/leather/img14.png" alt="" class="ml50" style="top: 17px; position:relative" /></div>
						<p class="prod-item__dtl mt10 ml15" >革：スターレ</p>
					   </div>
					  </div>
					</div>
					

					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h3>
							ブラック
						</h3>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
					
						  <div class="lprod " style="margin-left: 19px;">
						   <div class="prod-item">
							<div class="prod-item__thumb"><img src="images/leather/img15.png" alt=""/></div>
							<p class="prod-item__dtl" style="padding-top:15px; margin-left:-19px; top:-8px; position:relative;">革：ラティーゴ</p>
						   </div>

						   <div class="prod-item" style="top:-7px; left:2px; position:relative">
							<div class="prod-item__thumb"><img src="images/leather/img16.png" alt="" style="position:relative; top: 15px;" /></div>
							<p class="prod-item__dtl" style="padding-top:8px; margin-left: 12px;">革：スターレ</p>
						   </div>
						  </div>
					</div>
					

					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h3>
							グリーン
						</h3>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
					
					  <div class="lprod ml15">
					   <div class="prod-item">
						<div class="prod-item__thumb"><img src="images/leather/img17.png" alt=""/></div>
						<p class="prod-item__dtl" style="margin-left: -15px;">革：ラティーゴ</p>
					   </div>

					   <div class="prod-item">
						<div class="prod-item__thumb" style="position:relative; left:20px; top:5px;"><img src="images/leather/img18.png" alt="" class="ml55" /></div>
						<p class="prod-item__dtl ml30">革：スターレ</p>
					   </div>
					  </div>
					</div>

					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h3>
							レッド
						</h3>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
					
					  <div class="lprod ml20">
					   <div class="prod-item">
						<div class="prod-item__thumb"><img src="images/leather/img19.png" alt=""/></div>
						<p class="prod-item__dtl" style="margin-left: -20px;">革：ラティーゴ</p>
					   </div>

					   <div class="prod-item">
						<div class="prod-item__thumb"><img src="images/leather/img20.png" alt="" class="ml35" /></div>
						<p class="prod-item__dtl ml15">革：スターレ</p>
					   </div>
					  </div>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							定番カラー＝キャメルについて
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/leather/img21.png">
						<p>
							HERZの「キャメル」は染色していないままの革（ヌメ革）が日に焼けた時の色を再現して染色して作ってもらっています。<br/>
							  ちょうど少し使い込んだような暖かみを感じる色。<br/>
							  日々使っていると気に留めないけれどふと鞄を見てみると、色の変化が楽しめる。<br/>
							  この色も、その変化も愛着を感じられる理由ひとつなのかもしれません。
						</p>
						<div class="clear-both"></div>
					</div>
					
					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/leather/img22.png">
						<p>
							そして、初めはキャメルのみでしたが、ご要望に添って、黒、チョコ、グリーン、赤の順に展開が増えていきました。
      カラフルになると、作り手の思考が手軽に使える革小物へも移り、少しずつラインナップが充実してきたのです。
						</p>
						<div class="clear-both"></div>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							タンドーソフトレザーについて
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/leather/img23.png" alt=""/>
					</div>
					<br/><br/><br/><br/>
					<header class="header-content">
						<h3>
							極厚で柔らかい、<br />ワイルドなシワが魅力のHERZオリジナルレザー
						</h3>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/leather/img24.png">
						<p>
							HERZ創業者の近藤が長きにわたり求めた、厚くて野性的な表情を持つ革です。<br/>
      深い無数のシワがあり、極厚ながらもしなやかな柔らかさを持つこの革は、大草原を砂煙をあげながら荒々しく走る野生のバイソンを思わせます。 <br/> <br/>

      思い通りの革が仕上がらず、素材開発には20年以上の歳月がかかりましたが、メッセンジャーバッグ（W-3）の誕生で一気にイメージが固まり、オリジナルレザーとして完成しました
						</p>
						<div class="clear-both"></div>
					</div>

					<div class="iblk">
					 <p class="f15">ワイルドさの中にも、あたたかく柔らかな印象のある独特の風合いは、手にした際に他では 得ることができない驚きを実感いただけるはずです。</p>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							昔ながらの製法で時間をかけてなめされた革
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					<br/><br/>
					<div class="iblk rlistf-items mt55">
						<div class="comparison"> 
							<div class="col2 ohidden"> 
								<div class="col2-items"> 
									<div class="comparison-thumb"><img src="images/leather/img25.png" alt=""></div>
								</div> 
								<div class="col2-items col2-items-right">
									<div class="comparison-thumb"><img src="images/leather/img26.png" alt=""></div>
								</div> 
							</div> 
						</div>
						<p class="p-adj">
							多くの時間と手間をかけてでも、革本来の風合いを大切にしたい。それが古くからの技法であるタンニンなめしを選んだ理由です。
						</p>
					</div>
					
					
					<div class="iblk">
					  <div class="section1 mt50">
					   <img class="imgLeft2" src="images/leather/img27.png" alt="" align="left"/>
					   <p>
						植物から抽出したタンニンによって、時間をかけて丹精にゆっくりなめしていく。生産第一の現代社会とまるで逆行した姿勢ですが、この手間が堅牢で味わい深い、良い革をつくりだすのです。
						また、クロームなめし（クローム剤という化合物によるなめし）による革は、燃やすと有害物質が出ますが、タンニンなめしの革はそのまま灰となり土に帰る、環境に優しい手法です。HERZのカバンにどこか愛嬌のある素朴さが感じられるのは、自然に寄り添ったものづくりを心がけているからかもしれません。<strong>自然からの産物である革を、自然によってなめし、染色する。これが40年以上、同じ革を選び続けたHERZのこだわりです。</strong>
					   </p>
					  </div>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							厳選された素材
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					<br/><br/>
					<div class="iblk">
					  <p class="f15 mt20">革鞄を丈夫に仕上げる為に、素材にも耐久性のある原皮（なめす前の皮）を選んでいます。 原皮は、成牛の中でも強度と耐久性のある「ステアハイド」と呼ばれるものを使用しています。その中から比較的硬く厚い皮を厳選しています。</p>

					  <p class="f15 mt20">革鞄を丈夫に仕上げる為に、素材にも耐久性のある原皮（なめす前の皮）を選んでいます。 原皮は、成牛の中でも強度と耐久性のある「ステアハイド」と呼ばれるものを使用しています。その中から比較的硬く厚い皮を厳選しています。</p>

					  <p class="f15 mt20">
					   参考までに・・・<br/>
					   原皮は牛の成長にしたがって、種類が分けられています。若い順に「カーフ」「キップ」「カウ」「ステア」の４種類があります。人間と同じように、成長した動物の皮は堅牢で丈夫ですが、若いものは柔らかく皮のキメが細かいという特徴があります。
					  </p>
					</div>
					

					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
