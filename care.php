<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						お手入れ方法
						<span class="header-eng">HOW TO CARE</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br/><br/>
				
				<div class="cblk-1">
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/updt-care/img1.jpg" />	
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							革は乾燥すると弱くなります。そのままにしておくとひび割れ・やぶれなどに発展します
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>
							HERZで通常使用しているラティーゴ・スターレと呼んでいるオリジナルレザーは、製品の特性上、適度な革の張り、硬さを持たす為、革をなめす際に含ませるオイル、グリースの量を調整して仕上げを行っています。<br/>
							このため、製品をご使用いただく上で革が乾燥してくることがあります。 
						</p>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							オイルメンテナンス
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>
							まず製品をご使用いただく前に、革の表面全体に保革用のオイル（ラナパー、ミンクオイル等）を塗っていただくことをお勧めします。<br/>
							また、ご使用いただく中で表面が乾燥した際にも保革用オイルをご使用下さい。オイルを塗る度に、色が濃くなり表面にツヤが出てくる革独特の変化をお楽しみいただけます。 
						</p>
						
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax content-pc mr25 mb25" src="images/updt-care/img2.jpg" />
						<h3 class="mb20">
							1.ブラッシング、乾拭き
						</h3>
						<p>
							使用後の鞄をブラッシングまたは乾拭きして、鞄に付いた ホコリや汚れを落とします 
						</p>
						<img class="sp-img-nofloat sp-img-wmax content-sp mb25" src="images/updt-care/img2.jpg" />
						<div class="clear-both"></div>
						
						<br/>
						
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax content-pc mr25 mb25" src="images/updt-care/img3.jpg" />
						<h3 class="mb20">
							2.オイル塗布 
						</h3>
						<p>
							ごく少量を柔らかい布など（ラナバーの場合は付属のスポンジ）にとり、 薄く全体に均等に延ばすように塗ります。 初めてオイルを塗る方は、目立たない底部などから塗って頂くと良いです。 
						</p>
						<img class="sp-img-nofloat sp-img-wmax content-sp mb25" src="images/updt-care/img3.jpg" />
						<div class="clear-both"></div>
						
						<br/>
						
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax content-pc mr25 mb25" src="images/updt-care/img4.jpg" />
						<h3 class="mb20">
							3.鞄に油分を浸透させる
						</h3>
						<p>
							オイル塗布直後は鞄が多少べたついています。 油分が浸透するまで鞄をおいておきましょう。
						</p>
						<img class="sp-img-nofloat sp-img-wmax content-sp mb25" src="images/updt-care/img4.jpg" />
						<div class="clear-both"></div>
						
						<br/>
						
						<h5 class="fcred">
							オイル塗布の頻度について
						</h5>
						<p class="fcred">
							革鞄は約2か月に1回程度、オイルを塗り込んで下さい。 比較的、手に触れる機会の多い革小物はバッグよりも塗布する頻度は少なくても結構です。
						</p>
						<h5 class="fcred">
							イタリアンレザーにはオイル塗布は不要です
						</h5>
						<p class="fcred">
							一部製品（IL、Gではじまる品番の付いた製品）に使用しているイタリアンレザーは充分にオイルを含ませて仕上げていますので、 オイルメンテナンスは必要ありません。布で乾拭きしていただくだけで綺麗なツヤが出てきます。 						
						</p>
						<p>
							<a class="anc link-1 font-ryumin-pro" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png">Organイタリアンレザーについて</a>
						<p>
						
						<div class="iblk iblk-8 ">
							<h5>
								関連リンク
							</h5>
							<ul>
								<li class="li-item">
									<img src="images/updt-care/img5.jpg">
									<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">作り手が教えるメンテナンス </a>
									<div class="clear-both"></div>
								</li>
								<li class="li-item last-item">
									<img src="images/updt-care/img6.jpg">
									<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">メンテナンス用品 </a>
									<div class="clear-both"></div>
								</li>
								<div class="clear-both"></div>
							</ul>
						</div>
						
					</div>
						
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							革が水に濡れてしまった場合
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>
							水に濡れてしまった場合は浸み込む前に対応すると効果があります。<br/>
							雨の日のご使用などで革が濡れてしまった場合、乾いた後に水染みが残る場合があります。<br/>
							水染みを防ぐには、革全体を優しく水拭きした後、自然乾燥させて下さい。<br/>
							早く乾かそうとして、ドライヤーなどの高温に近づけるのは避けてください。<br/>
							ひび割れや痛みの原因になりますので、充分ご注意ください。
						</p>
						

						
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax content-pc mr25 mb25" src="images/updt-care/img7.jpg" />
						<h3 class="mb20">
							1.水染みがついた時 
						</h3>
						<p>
							このまま放置すると、乾いた後に染みが残ります。 乾いた後の染みを取り除くのは難しいので、 水染みが付いているうちに対応してください。 
						</p>
						<img class="sp-img-nofloat sp-img-wmax content-sp mb25" src="images/updt-care/img7.jpg" />
						<div class="clear-both"></div>
						
						<br/>
						
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax content-pc mr25 mb25" src="images/updt-care/img8.jpg" />
						<h3 class="mb20">
							2.水拭きで馴染ませる 
						</h3>
						<p>
							水で絞った布巾を使い、全体に水染みと同じぐらい 馴染ませるように水拭きするとシミとして残りにくくなります。 
						</p>
						<img class="sp-img-nofloat sp-img-wmax content-sp mb25" src="images/updt-care/img8.jpg" />
						<div class="clear-both"></div>
						
						<br/>
						
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax content-pc mr25 mb25" src="images/updt-care/img9.jpg" />
						<h3 class="mb20">
							3.革を乾かす
						</h3>
						<p>
							水拭き後、乾いたタオルでたたくようにして水気を除き、 風通しの良い場所で陰干しして革を乾かします。 
						</p>
						<img class="sp-img-nofloat sp-img-wmax content-sp mb25" src="images/updt-care/img9.jpg" />
						<div class="clear-both"></div>
						
						<br/>
						
						<p class="fcred">
							革が濡れた状態で強い力で擦ったり、過度な力を加えると、色ムラ、変形の原因となります。 また、濡れた革は油分が不足することがありますので、充分に乾燥した後、保革用オイルを塗って下さい。 水拭きでも落ちない染みについては落とすことができません 
						</p>
						
						<div class="iblk iblk-8">
							<h5>
								関連リンク
							</h5>
							<ul>
								<li class="li-item last-item li-item-single">
									<img src="images/updt-care/img10.jpg">
									<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">雨に濡れてしまった！気になる雨染みどうなるの？</a>
									<div class="clear-both"></div>
								</li>
								<div class="clear-both"></div>
							</ul>
						</div>
					</div>	
						
						
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							キズ、ヨゴレがついた場合
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>
							表面上に付着した汚れであれば、市販の消しゴムで軽く擦ることによって汚れが落ちます。<br/>
							表面上のかすかなキズであれば、水拭きをすることで目立たなくなります。<br/>
							革の中まで染み込んだ汚れ、深く付いてしまったキズについては、消すことができません。<br/>
							キズ、汚れどちらの場合も完全に消すことができないのが、革のひとつの特徴です。<br/>
							目立たなくするという考えで扱って頂ければ、製品を長くご愛用いただけると考えております。
						</p>
						
						<div class="iblk iblk-22">
							<ul>
								<li class="li-item">
									<article>
										<div class="article-feature-image">
											<img class="" src="images/updt-care/img11.jpg">
										</div>
										<p>
											強くこすると、色が抜けてしまう事もあるので、軽くこするようにして下さい
										</p>
									</article>
								</li>
								<li class="li-item">
									<article>
										<div class="article-feature-image">
											<img class="" src="images/updt-care/img12.jpg">
										</div>
										<p>
											水染みの時と同じ要領で、該当箇所を拭いた後、自然乾燥させて下さい。
										</p>
									</article>								
								</li>
								<div class="clear-both"></div>
							</ul>
						</div>
						
						<div class="iblk iblk-8 ">
							<h5>
								関連リンク
							</h5>
							<ul>
								<li class="li-item last-item li-item-single">
									<img src="images/updt-care/img13.jpg">
									<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">バッグにシミがついてしまった時の対応方法 </a>
									<div class="clear-both"></div>
								</li>
								<div class="clear-both"></div>
							</ul>
						</div>
					</div>
					
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							色落ち、カビについて
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>
							より自然のままで革の仕上げを行う為、染色もまた昔ながらの天然素材による手法を用いています。<br/>
							その為、摩擦や水濡れ等によって革からの色移りが起こります。<br/>
							淡い色の衣服でご使用いただく際には、色移りにご注意ください。<br/>
							カビは一度発生すると除去が困難なので、ご使用後は型崩れを防ぐために新聞紙などの詰め物をしていただき、湿気の少ない風通しの良い場所での保管をお勧め致します。 
						</p>
						
						
						<div class="iblk iblk-22">
							<ul>
								<li class="li-item">
									<article>
										<div class="article-feature-image">
											<img class="" src="images/updt-care/img14.jpg">
										</div>
										<p>
											淡い色の衣服は、特に色移りが起こりやすいのでご注意下さい。
										</p>
									</article>
								</li>
								<li class="li-item">
									<article>
										<div class="article-feature-image">
											<img class="" src="images/updt-care/img15.jpg">
										</div>
										<p>
											鞄を使用しない時は中に詰め物を入れて、風通しのよい場所に保管して下さい。
										</p>
									</article>								
								</li>
								<div class="clear-both"></div>
							</ul>
						</div>
						
					</div>
						
						
					<br/><br/>
						
						
				</div>
			</div>
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
