<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-special-contents has-aside">
			
			<div class="ablk-1 header-breadcrumb">
				<p class="breadcrumb">
					<a class="anc link-3" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar-special.php');
			?>

			<div class="site-content">
				
				<div class="ablk-1 site-content-breadcrumb">
					<p class="breadcrumb">
						<a class="anc link-3" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
					</p>
				</div>
				
				<div class="ablk-1 special-content">
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2>
							特集
							<span class="header-eng">-SPECIAL CONTENTS-</span>
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					
					<div class="iblk-9">
						<article>
							<div class="article-feature-image taped">
								<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png">
								<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png">
								<a class="anc-img" href="#">
									<img src="images/updt-special-list/article-img1.jpg" />
								</a>
							</div>
							<h3 class="header-category">
								2015/00/00 &nbsp;&nbsp;
								<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png">商品企画</a>
							</h3>
							<h3 class="header-article">
								<a href="#">
									ペーパークラフトプレゼント2014 「その1 品番A-20」
								</a>
							</h3>
							<p>
								ご注文いただいた商品と一緒にペーパークラフトの型紙を送ります。型紙には説明文も記載していますので、その内容に沿って、工作してみて下さい！！定規とカッターと糊があれば完成します！！
							</p>
							<a class="anc link-2" href="#"><img src="images/updt-common/ico-arrow-right-black.png">続きを読む</a>
						</article>
					</div>
					
					<header class="header-content">
						<h2>
							特集一覧
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					
					<div class="iblk-10">
						<ul>
							<li class="li-item">
								<article>
									<div class="article-feature-image taped">
										<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png">
										<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png">
										<a class="anc-img" href="#">
											<img src="images/updt-special-list/special-list-thumb-01.jpg" />
										</a>
									</div>
									<h3 class="header-category">
										2015/00/00
										<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png">商品企画</a>
									</h3>
									<h3 class="header-article">
										<a href="#">
											リュックの試作 ～定番化を目指して 作り手：村松～
										</a>
									</h3>
								</article>								
							</li>
							<li class="li-item">
								<article>
									<div class="article-feature-image taped">
										<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png">
										<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png">
										<a class="anc-img" href="#">
											<img src="images/updt-special-list/special-list-thumb-02.jpg" />
										</a>
									</div>
									<h3 class="header-category">
										2015/00/00
										<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png">商品企画</a>
									</h3>
									<h3 class="header-article">
										<a href="#">
											創業者とカバン作ろう ～作り手：ナカムラ編 vol.2～
										</a>
									</h3>
								</article>								
							</li>
							<li class="li-item">
								<article>
									<div class="article-feature-image taped">
										<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png">
										<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png">
										<a class="anc-img" href="#">
											<img src="images/updt-special-list/special-list-thumb-03.jpg" />
										</a>
									</div>
									<h3 class="header-category">
										2015/00/00
										<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png">商品企画</a>
									</h3>
									<h3 class="header-article">
										<a href="#">
											リュックの試作 ～定番化を目指して 作り手：村松～
										</a>
									</h3>
								</article>								
							</li>
							<li class="li-item">
								<article>
									<div class="article-feature-image taped">
										<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png">
										<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png">
										<a class="anc-img" href="#">
											<img src="images/updt-special-list/special-list-thumb-04.jpg" />
										</a>
									</div>
									<h3 class="header-category">
										2015/00/00
										<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png">商品企画</a>
									</h3>
									<h3 class="header-article">
										<a href="#">
											創業者とカバン作ろう ～作り手：ナカムラ編 vol.2～
										</a>
									</h3>
								</article>								
							</li>
							<li class="li-item">
								<article>
									<div class="article-feature-image taped">
										<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png">
										<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png">
										<a class="anc-img" href="#">
											<img src="images/updt-special-list/special-list-thumb-05.jpg" />
										</a>
									</div>
									<h3 class="header-category">
										2015/00/00
										<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png">商品企画</a>
									</h3>
									<h3 class="header-article">
										<a href="#">
											リュックの試作 ～定番化を目指して 作り手：村松～
										</a>
									</h3>
								</article>								
							</li>
							<li class="li-item">
								<article>
									<div class="article-feature-image taped">
										<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png">
										<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png">
										<a class="anc-img" href="#">
											<img src="images/updt-special-list/special-list-thumb-06.jpg" />
										</a>
									</div>
									<h3 class="header-category">
										2015/00/00
										<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png">商品企画</a>
									</h3>
									<h3 class="header-article">
										<a href="#">
											創業者とカバン作ろう ～作り手：ナカムラ編 vol.2～
										</a>
									</h3>
								</article>								
							</li>
							<li class="li-item">
								<article>
									<div class="article-feature-image taped">
										<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png">
										<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png">
										<a class="anc-img" href="#">
											<img src="images/updt-special-list/special-list-thumb-07.jpg" />
										</a>
									</div>
									<h3 class="header-category">
										2015/00/00
										<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png">商品企画</a>
									</h3>
									<h3 class="header-article">
										<a href="#">
											リュックの試作 ～定番化を目指して 作り手：村松～
										</a>
									</h3>
								</article>								
							</li>
							<li class="li-item">
								<article>
									<div class="article-feature-image taped">
										<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png">
										<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png">
										<a class="anc-img" href="#">
											<img src="images/updt-special-list/special-list-thumb-08.jpg" />
										</a>
									</div>
									<h3 class="header-category">
										2015/00/00
										<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png">商品企画</a>
									</h3>
									<h3 class="header-article">
										<a href="#">
											創業者とカバン作ろう ～作り手：ナカムラ編 vol.2～
										</a>
									</h3>
								</article>								
							</li>
							<li class="li-item">
								<article>
									<div class="article-feature-image taped">
										<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png">
										<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png">
										<a class="anc-img" href="#">
											<img src="images/updt-special-list/special-list-thumb-09.jpg" />
										</a>
									</div>
									<h3 class="header-category">
										2015/00/00
										<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png">商品企画</a>
									</h3>
									<h3 class="header-article">
										<a href="#">
											リュックの試作 ～定番化を目指して 作り手：村松～
										</a>
									</h3>
								</article>								
							</li>
							<li class="li-item">
								<article>
									<div class="article-feature-image taped">
										<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png">
										<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png">
										<a class="anc-img" href="#">
											<img src="images/updt-special-list/special-list-thumb-10.jpg" />
										</a>
									</div>
									<h3 class="header-category">
										2015/00/00
										<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png">商品企画</a>
									</h3>
									<h3 class="header-article">
										<a href="#">
											創業者とカバン作ろう ～作り手：ナカムラ編 vol.2～
										</a>
									</h3>
								</article>								
							</li>
							
							<div class="clear-both"></div>
						</ul>
					</div>
					
					
					
					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
