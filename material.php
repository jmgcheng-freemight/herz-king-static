<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						素材
						<span class="header-eng">MATERIAL</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				<br /><br />
				<div class="ablk-1">
					<div class="iblk-0">
						<img class="sp-img-wmax" src="images/material/img1.png" />
					</div>
					<br/><br/><br/><br/>
					<header class="header-content">
						<h3>
							堅牢さを求めて選んだ素材がHERZ独特のデザインを生み出しました
						</h3>
						<div class="accent-1 mt15"></div>
					</header>
					<br /><br />
					<div class="iblk-0">
						<p>
							HERZの素材選びはいたってシンプル。それが丈夫であるかどうかです。<br />
							もちろん、長く使う上で、金具類などパーツの交換が必要になりますが、その時も見た目のデザインを損なわぬよう、ベーシックな形のものを選び使い続けています。<br />
							飽きのこないベーシックな素材を使うことにより、いつでも修理に対応できるというサービスもご提供できます。
						</p>
					</div>
					<br/><br/><br/><br/>
					<header class="header-content">
						<h3>
							革
						</h3>
						<div class="accent-1 mt15"></div>
					</header>
					<br /><br />
									
					<div class="iblk-0">	
						<h3 class="mbottom30">質の良い革をそのまま贅沢に使う</h3>
						<div class="image-wrapper">
							<img src="images/material/img2.png" alt="">
						</div>
						<br /><br />
						<p class="f15">
							HERZでいう質の良い革とは、見た目が均一で綺麗な革ではありません。<br>
							むしろその逆。革本来のキズやシワを隠す表面加工を最小限にとどめ、植物タンニンで時間をかけて丹精になめしたオイルレザーを使っています。<br>
							使い込み、時を経るごとに色に深みと艶が増し、使う人ならではの豊かで味わい深い表情を見せてくれます。<br>
							Organや直営店限定モデルで使用している革は、イタリアのタンナーから厳選した革を選んでいます。
						</p>						
					</div>
					
					<div class="iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/material/img3.png">
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">HERZオリジナルレザー</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<img src="images/material/img4.png">
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">Organイタリアンレザー</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>					
					
					<br/><br/><br/><br/>
					<header class="header-content">
						<h3>
							ミシン糸
						</h3>
						<div class="accent-1 mt15"></div>
					</header>
					<br /><br />
					<div class="iblk-0">
						<div class="mt30">
							<img src="images/material/img5.png" alt=""> 
						</div>					
						<p class="f15">
							HERZのカバンのステッチは、配色･糸の太さ・縫い目幅などに生きた表情をみせています。<br>
							革はもともと非常に丈夫なものですが、それを縫い付けている糸が弱くては革製品の意味がありません。革に負けない丈夫な縫製、そこからHERZの太いステッチが生まれたのです。
						</p>
						
						
						<div class="rlistf-items">
							<div class="box50"> <img src="images/material/img6.png" alt=""> </div>
							<div class="box50 padLeft1">
							<p>
								厚い革にしっかりと喰い込んだステッチは、HERZ製品の大きな特徴となっています。<br>
								革に開く針穴は意外にも耐久性を求める上では弱点でもあります。
							</p>
							<br>
							<p>
								その針穴をひとつでも減らす努力がHERZのステッチを作っています。HERZの使う糸は「皮革」を対象として品質を徹底的に追求した「皮革用ミシン糸の本格派」と言われる糸です。艶のある絹の風合いを持った変質しにくい丈夫な糸です。HERZではその中でも、最も太い糸（＃0）をメインに使用して、より丈夫な革製品作りを目指しています。
							</p>
							</div>
						</div>						
						
					</div>
					
					
					
					<br/><br/><br/><br/>
					<header class="header-content">
						<h3>
							金具、ファスナー
						</h3>
						<div class="accent-1 mt15"></div>
					</header>
					<br /><br />
					<div class="iblk-0">
						<div class="mt30">
							<img src="images/material/img7.png" alt=""> 
						</div>					
						<p class="f15">
							HERZのカバンに使用されている金具は、それぞれのカバンの構造を考え、様々な素材の中からより使用に耐える力強いものを厳選しています。<br>
							長く使うことを考えて選んだベーシックな金具によって、独特なデザインの製品が作り出されています。
						</p>						
						
						<div class="rlistf-items">
							<div class="box50"> <img src="images/material/img8.png" alt=""> </div>
							<div class="box50 padLeft1">
								<p>
									ファスナーもより丈夫であるために、しっかりした大きめのものを使用し、ファスナーの持つフィーリングを大切にしたデザインで製作されています。<br>
									使用しているファスナーはJIS強度検査・英国式耐久試験をクリアした質の高いファスナーです。<br>
									堅牢さを追及した道具は、持ち主に手入れされ、大切に使い込まれていくという「心の技」のこもった美しさがあるのです。
								</p>
							</div>
						</div>						
						
					</div>
					
					<br/><br/><br/><br/>
					
				</div>
				
				
				
			</div>
			

			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
