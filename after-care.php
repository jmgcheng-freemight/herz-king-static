<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						HERZのアフターケア
						<span class="header-eng">AFTER CARE</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br /><br />
				
				<div class="cblk-1">
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/updt-after-care/img1.jpg" />	
					</div>
					
					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h">
							毎日使って頂くことが最高の手入れとなります
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>
							皮革製品のお手入れは少し面倒です。でもほんの少し手を掛けてあげる事によって、愛着が湧き、カバンの寿命はぐっと延びます。<br/>
							日常でのお手入れが、味のある革を育てる第一歩となるのです。<br/>
							お手入れしながら長く大切に使い続けていくこと。これこそが本当の贅沢だとHERZは考えています。
						</p>
						<p>
							当工房がお作りした革製品のアフターケアは大きく分けて二つに分かれます。
						</p>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							1.使い手さんによる定期的なお手入れ
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/updt-after-care/img2.jpg" />
						<p>
							オイル塗布など基本的なお手入れについては、使い手の方に行って頂きます。それにより、鞄は長持ちし、手を掛けることで更に愛着が増すことでしょう。<br/>
							オイルメンテナンスの方法や雨染み、キズ、ヨゴレの対応など。日々お使い頂く中で、ご愛用者の方が出来るお手入れ方法をご案内します。
						</p>
						<div class="clear-both"></div>
					</div>
					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/updt-after-care/img3.jpg" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />お手入れ方法について詳しく見る</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<img src="images/updt-after-care/img4.jpg" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />メンテナンス用品</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							2.HERZが行う修理と加工
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/updt-after-care/img5.jpg" />
						<p>
							丈夫で長く使える鞄作りを心掛けていますが、壊れないわけではありません。<br/>
							普段のお手入れを怠りますと、修理が必要になる期間も早くなります。<br/>
							特に金具類などは使っていく中で、どうしても摩耗しますので、定期的に交換が必要です。<br/>
							その時、お使いの鞄を拝見し、その状態にふさわしい方法で作り手が丁寧に修理いたします。<br/>
							HERZの鞄はお使い頂く限り、出来るだけの修理・加工をいたします。
						</p>
					</div>
					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item last-item">
								<img src="images/updt-after-care/img6.jpg" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />修理について詳しく見る</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
					<br/><br/><br/><br/>
					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
