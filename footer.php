<div id="accent"></div> <!-- #accent -->
<div id="p-footer-wrapper"> <!-- #p-footer-wrapper -->
 <div id="p-footer"> <!-- #p-footer -->
  <div id="footer-logo">
   <img src="images/common/bot-logo.png" alt="">
   <p class="slogan">
   時を経てこそ解る味わいがある。使い込んでこそ伝わる温もりがある。<br>
   デザインから製作まで１人の作り手が心を込めて最後まで仕上げるもの作り。<br>
   それがヘルツのスピリット。</p>
  </div>
  <div id="footer-nav">
   <ul class="menu">
    <li><a href="https://twitter.com/herz_bag" target="_blank"><img class="icon" src="images/common/icons/tw-bot.png" alt=""/>TWITTER</a></li>
    <li><a href="https://www.facebook.com/herzbag" target="_blank"><img class="icon" src="images/common/icons/fb-bot.png" alt=""/>FACEBOOK</a></li>
    <li><a href="https://www.herz-bag.jp/webshop/mailmagazine.html" target="_blank"><img class="icon" src="images/common/icons/th-bot.png" alt=""/>TUMBLER</a></li>
    <li><a href="https://www.herz-bag.jp/webshop/mailmagazine.html" target="_blank"><img class="icon" src="images/common/icons/msg-bot.png" alt=""/>MAIL MAGAZINE</a></li>
    <li><a href=""><img class="icon" src="images/common/icons/map-bot.png" alt=""/>SITE MAP</a></li>
    <li><a href="https://www.herz-bag.jp/webshop/" target="_blank"><img class="icon" src="images/common/icons/cart-bot.png" alt=""/>ONLINE SHOP</a></li>
   </ul>
   <p class="copyright">Copyright (c) 2000-2015 HERZ Co.,Ltd. All Rights Reserved.</p>
  </div>
 </div> <!-- // #p-footer -->
</div> <!-- // #p-footer-wrapper -->

</div> <!-- // #p-wrap -->
</body>
</html>