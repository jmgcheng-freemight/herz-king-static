<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						ミネルバボックスについて
						<span class="header-eng">ABOUT MINERVA BOX</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br /><br />
				
				<div class="cblk-1">
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/minerva-box/img1.png" />	
					</div>
					
					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h">
							ミネルバボックス(Minerva Box)について
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>
							ミネルバボックスはイタリアのタンナー「バダラッシィ・カルロ社」で一枚一枚丁寧な手仕事によって、作られています。フィレンツェのサンタクローチェ地区で10世紀以上の歴史を持つ”バケッタ製法”と呼
ばれる、手鞣し・手染めで仕上げた高級素材の革です。<br />
革質はソフトレザータイプで、HERZオリジナルレザーの「スターレ」に近い革になります。
						</p>
						<p>
							バタラッシィ・カルロ社が作る革で、ミネルバボックスとは反対に表面の皺が少ないスムースレザータイプの「ミネルバリスシオ」というイタリアンレザーもHERZでは使用しています。
						</p>
					</div>

					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/minerva-box/img2.png" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />ミネルバリスシオ</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<img src="images/minerva-box/img3.png" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />HERZオリジナルレザー</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							特徴的なシュリンク（シボを出す）加工
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					<br />
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/minerva-box/img4.png" width="100%" />
						</div>
						<div class="box50">
							<p>ミネルバボックスという革の特徴は、まず自然本来の風合い豊かな表情であること。<br />
シュリンク加工と呼ばれる、表面にシボを出すシワ加工が特徴的な革です。<br />
部位による革質差により、加工をした後でもそれぞれシボの入り具合が異なります。<br />
シボが強く入っている箇所もあれば、比較的少ない箇所もあります。<br />
そのため、同じ鞄でも箇所によってはシボの入り具合が全く異なります。<br />
その一つひとつの表情・個性を楽しんで下さい。</p>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							楽しめる革の経年変化（エイジング）
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					<br />
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/minerva-box/img5.png" width="100%" />
						</div>
						<div class="box50">
							<p>植物タンニンで鞣した牛革に牛脚油（すね骨や無蹄足を煮沸して採取した100%ピュアなオイル）でたっぷりと時間をかけて加脂する製法です。<br />
加工に時間がかかり脂が浸透しにくい反面、使い込んだ時に独特の色艶がでることと、一旦加脂したオイルが抜けにくいという特徴があります。</p>
						</div>
						<div class="clear-both"></div>
					</div>
<br/><br/>
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/minerva-box/img6.png" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />経年変化について</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>

					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							ちょっとしたキズが付いた時には・・・
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					<br />
					<div class="iblk">
							<p>自然な風合いを生かすため、表面に大きな加工を施しておりません。<br />
そのため、革の表面の小さなキズやシワが隠れにくく、爪傷が付きやすいという特徴がありますが、オイルを多く含んでいるので、指で軽く揉み込んであげるとキズが目立ちにくくなります。<br />
使い始めの内はオイル等による保革は必要ありません。</p>
							<img class="sp-img-wmax" src="images/minerva-box/img7.png" width="100%" />

					</div>
					
<br/><br/>
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/minerva-box/img8.png" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />ミネルバボックス</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<img src="images/minerva-box/img9.png" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />ミネルバリスシオ</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<img src="images/minerva-box/img10.png" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />リバースレザー</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<img src="images/minerva-box/img11.png" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />ユーフラテ</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
					<br/><br/><br/><br/>
					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
