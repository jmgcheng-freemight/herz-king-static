<!DOCTYPE html>
<head>
 <title></title>
 <meta http-equiv="Content-Style-Type" content="text/css">
 <meta name="robots" content="noindex,nofollow,noarchive" />
 <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
 <meta http-equiv="Content-Script-Type" content="text/javascript">
 <meta http-equiv="Content-type" content="text/html; charset=utf-8">
 <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0">

 <link rel="stylesheet" href="css/style.css">
 <link rel="stylesheet" href="css/index.css">

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
 <script type="text/javascript" src="js/jquery.js" charset="utf-8"></script>
 <script type="text/javascript" src="js/jquery.page-scroller.js" charset="utf-8"></script>
</head>
<body>
<div id="p-wrap"> <!-- #p-wrap -->

<div id="p-header-wrapper"> <!-- #p-header-wrapper -->
 <div id="p-header"> <!-- #p-header -->
  <div id="header-logo"> <!-- #header-logo -->
   <img src="images/common/logo-top.png" alt="">
   <p class="slogan">革鞄/ハンドメイドレザーバッグのHERZ公式サイト</p>
  </div> <!-- // #header-logo -->

  <div id="header-container"> <!-- #header-container -->
   <div id="header-nav"> <!-- #header-nav -->
    
    <ul class="top-menu"> <!-- .top-menu -->
     <li><a href="http://www.herz-bag.jp/" target="_blank"><img style="vertical-align: middle;" src="images/common/icons/icon-star.png" alt=""/>特集</a></li>
     <li><a href="http://www.herz-bag.jp/blog/" target="_blank"><img style="vertical-align: middle;" src="images/common/icons/icon-pen.png" alt=""/>ブログ</a></li>
     <li><a href="http://www.herz-bag.jp/blog/category/press/" target="_blank"><img style="vertical-align: middle;" src="images/common/icons/icon-menu.png" alt=""/>プレスリリース</a></li>
     <li><a href="" target="_blank"><img style="vertical-align: middle;" src="images/common/icons/icon-msg.png" alt=""/>お問い合わせ</a></li>
     <li><a href="https://www.organ-leather.com/" target="_blank"><img style="vertical-align: middle;" src="images/common/icons/icon-org.png" alt=""/>Organ</a></li>
    </ul> <!-- // .top-menu -->
    
    <ul class="p-nav"> <!-- #p-nav -->
     <li id="p-nav__items"><a href="http://www.herz-bag.jp/aboutus/" target="_blank">ヘルツについて</a></li>
     <li id="p-nav__items"><a href="http://www.herz-bag.jp/shop/" target="_blank">直営店</a></li>
     <li id="p-nav__items"><a href="http://www.herz-bag.jp/factory/" target="_blank">工房</a></li>
     <li id="p-nav__items"><a href="http://www.herz-bag.jp/material/" target="_blank">素材</a></li>
     <li id="p-nav__items"><a href="http://www.herz-bag.jp/repair/" target="_blank">アフターケア</a></li>
     <li id="p-nav__items"><a href="https://www.herz-bag.jp/webshop/" target="_blank">オンラインショップ</a></li>
    </ul> <!-- // #p-nav -->
   
   </div> <!-- // #header-nav -->
  </div> <!-- // #header-container -->
 </div> <!-- // #p-header -->
</div> <!-- // #p-header-wrapper -->
<div id="accent"></div> <!-- #accent -->