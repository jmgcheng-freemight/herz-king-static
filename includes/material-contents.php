<div id="main-wrapper">

	<div id="section-wrapper" style="margin-bottom:80px;">
		<div id="accent-3" style="margin-bottom: 15px;"></div>
		<h3 id="main-title" class="main-title-attention">素材<span class="mini-text-attention" style="padding-left:10px;">MATERIAL</span></h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<div id="image-wrapper">
			<img src="images/material/img1.png" alt="" />
		</div>
	</div>
	
	<span id="section" name="elem1"></span>
	<div id="section-wrapper" style="margin-bottom:90px;">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px; ">堅牢さを求めて選んだ素材がHERZ独特のデザインを生み出しました</h3>
		<div id="accent-3" style="margin-top: 11px;"></div>
		<p class="f15" style="margin-top: 13px;">
			HERZの素材選びはいたってシンプル。それが丈夫であるかどうかです。<br/>
			もちろん、長く使う上で、金具類などパーツの交換が必要になりますが、その時も見た目のデザインを損なわぬよう、ベーシックな形のものを選び使い続けています。<br/>
			飽きのこないベーシックな素材を使うことにより、いつでも修理に対応できるというサービスもご提供できます。
		</p>
	</div> <!-- #section-wrapper -->
	
	<span id="section" name="elem1"></span>
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">革</h3>
		<div id="accent-3" style="margin-top: 6px;"></div>
		<br/><br/>
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">質の良い革をそのまま贅沢に使う</h3>
		
		<div id="image-wrapper">
			<img src="images/material/img2.png" alt="" />
		</div>
		<p class="f15" style="margin-top:30px;">
			HERZでいう質の良い革とは、見た目が均一で綺麗な革ではありません。<br/>
			むしろその逆。革本来のキズやシワを隠す表面加工を最小限にとどめ、植物タンニンで時間をかけて丹精になめしたオイルレザーを使っています。<br/>
			使い込み、時を経るごとに色に深みと艶が増し、使う人ならではの豊かで味わい深い表情を見せてくれます。<br/>
			Organや直営店限定モデルで使用している革は、イタリアのタンナーから厳選した革を選んでいます。
		</p>
		<div class="sd-cont09" style="margin-top:40px; margin-bottom:none; padding-bottom:2px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal; margin-bottom:10px;">関連リンク</h4>
			<div class="sd-cont09-items" >
				<div class="sd-cont09-items__img"> <img src="images/material/img3.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							HERZオリジナルレザー
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items" >
				<div class="sd-cont09-items__img"> <img src="images/material/img4.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							Organイタリアンレザー
						</a>
					</span>
				</div>
			</div>
		</div>
	</div> <!-- #section-wrapper -->
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">ミシン糸</h3>
		<div id="accent-3" style="margin-top: 6px;"></div>
		<div style="margin-bottom: 5px; margin-top:20px;">
			<img src="images/material/img5.png" alt=""> 
		</div>
		<p class="f15" style="margin-top: 10px;">
			HERZのカバンのステッチは、配色･糸の太さ・縫い目幅などに生きた表情をみせています。<br/>
			革はもともと非常に丈夫なものですが、それを縫い付けている糸が弱くては革製品の意味がありません。革に負けない丈夫な縫製、そこからHERZの太いステッチが生まれたのです。
		</p>
		<div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/material/img6.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
				<p>
					厚い革にしっかりと喰い込んだステッチは、HERZ製品の大きな特徴となっています。<br/>
					革に開く針穴は意外にも耐久性を求める上では弱点でもあります。
				</p>
				<br/>
				<p>
					その針穴をひとつでも減らす努力がHERZのステッチを作っています。HERZの使う糸は「皮革」を対象として品質を徹底的に追求した「皮革用ミシン糸の本格派」と言われる糸です。艶のある絹の風合いを持った変質しにくい丈夫な糸です。HERZではその中でも、最も太い糸（＃0）をメインに使用して、より丈夫な革製品作りを目指しています。
				</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			
		</div> <!-- # - .rlistf -->
	</div>	
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="">金具、ファスナー</h3>
		<div id="accent-3" style="margin-top: 11px;"></div>
		<div style="margin-bottom:10px; margin-top:20px;">
			<img src="images/material/img7.png" alt=""> 
		</div>
		<p class="f15" style="">
			HERZのカバンに使用されている金具は、それぞれのカバンの構造を考え、様々な素材の中からより使用に耐える力強いものを厳選しています。<br/>
			長く使うことを考えて選んだベーシックな金具によって、独特なデザインの製品が作り出されています。
		</p>
		<div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top: 35px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/material/img8.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
				<p>
					ファスナーもより丈夫であるために、しっかりした大きめのものを使用し、ファスナーの持つフィーリングを大切にしたデザインで製作されています。<br/>
					使用しているファスナーはJIS強度検査・英国式耐久試験をクリアした質の高いファスナーです。<br/>
					堅牢さを追及した道具は、持ち主に手入れされ、大切に使い込まれていくという「心の技」のこもった美しさがあるのです。
				</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			
		</div> <!-- # - .rlistf -->
	</div>
	
</div> <!-- // #main-wrapper -->