<div id="main-wrapper">

	<div id="section-wrapper">
	<div id="accent-3" style="margin-bottom: 11px;"></div>
	<h3 id="main-title" class="font-ryumin">HERZ刻印について<span class="mini-text">-ABOUT ENGRAVED-</span></h3>
	<div id="accent-3" style="margin-top: 11px;"></div>
	<div id="image-wrapper">
	<img src="images/kokuin/img1.png" alt="" />
	</div>
	</div>
 

	 <span id="section" name="elem3"></span>
	 <div id="section-wrapper">
	  <h3 id="main-title" class="font-ryumin" style="">HERZ刻印の仕様変更について(2012年11月1日～)</h3>
	  <div id="accent-3" style="margin-top: 13px;"></div>

	  <p class="f15" style="margin-top: 13px; margin-bottom: 25px;">
		HERZ刻印の仕様を変更しました。今後、製作する商品については、下記写真のようにHERZロゴの下にMADE IN JAPANの刻印が入る様になります。
		</p>

	  <div style="margin-bottom: 25px">
	   <img src="images/kokuin/img2.png" alt="" />
	   <img src="images/kokuin/img3.png" style="margin-left:30px;" />
	   <!-- 
	   <img src="images/aging/img10.png" alt="" style="margin-left: 13px;"/>
	   <img src="images/aging/img12.png" alt="" style="margin-top: 37px;" />
	   <img src="images/aging/img13.png" alt="" style="margin: 37px 0 0 13px;" />
	   -->
	  </div>
	  <p class="f15" style="margin-top: 13px; margin-bottom:5px;">
		各商品ページの写真は今までの刻印で掲載されているモデルもありますが、出来上がりは新しい刻印が入ります。
		</p>
		<p style="color:#cc0000; margin-bottom:5px;">
			<strong>※一部商品によっては、配置の関係上、HERZロゴのみの刻印になる商品もあります。</strong>
		</p>
		<p style="color:#cc0000;">
			<strong>※刻印のデザインを選びことは出来ません。</strong>
		</p>
	  
	  
		<img src="images/kokuin/img4.png" alt="" style="display: block; margin-top: 45px;">
	  
	 </div> <!-- #section-wrapper --> 


</div> <!-- // #main-wrapper -->