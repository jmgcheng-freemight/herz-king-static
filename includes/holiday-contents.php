<div id="main-wrapper">

	<div id="section-wrapper">
		<div id="accent-3" style="margin-bottom: 15px;"></div>
		<h3 id="main-title" class="main-title-attention">年末年始お休みのお知らせ<span class="mini-text-attention" style="padding-left:10px;">-HOLIDAY-</span></h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<div id="image-wrapper">
			<img src="images/holiday/img1.png" alt="" />
		</div>
		<p class="f15" style="margin-top: 40px;">
			いつもHERZをご愛顧頂き、誠にありがとうございます。<br/>
			誠に勝手ながら下記の期間中、年末年始による休業とさせていただきます。<br/>
			各店舗ごとで休業期間が異なりますので、事前にご確認を宜しくお願いいたします。
		</p>
		<p class="f15" style="margin-top: 25px;">
			<strong>
			【オンラインショップの休業期間 ： 10月6日（火） ～ 10月7日（水）】
			</strong><br/>
			休業中も通常通り、お買い物が可能です。<br/>
			ただ、ご注文確認やお問い合わせ等のご返事は10月8日（木）以降、順次ご返信となります。<br/>
			予めご理解の程、宜しくお願い致します。
		</p>
		<div class="comp-profile-row3" style="margin-top:30px;">
			<div id="comp-table">
				<div class="table-1">
					<div class="table-row-1">
						<div class="table-cell table-cell-left" style="width: 176px;">
							<p class="f15">
								本店
							</p>
						</div>
						<div class="table-cell table-cell-right">
							<p>
								<span class="font-color-darkred font-weight-bold">
									10月6日（火） 
								</span>
								※10月8日（木）より通常営業となります。
							</p>
						</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">
							<p class="f15">
								Organ
							</p>
						</div>
						<div class="table-cell table-cell-right">
							<p>
								<span class="font-color-darkred font-weight-bold">
									10月6日（火）～ 10月7日（水）
								</span>
								※10月8日（木）より通常営業となります。
							</p>
						</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">
							<p class="f15">
								FACTORY SHOP
							</p>
						</div>
						<div class="table-cell table-cell-right">
							<p>
								<span class="font-color-darkred font-weight-bold">
									10月6日（火）
								</span>
								※10月8日（木）より通常営業となります。
							</p>
						</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">
							<p class="f15">
								RESO.
							</p>
						</div>
						<div class="table-cell table-cell-right">
							<p>
								<span class="font-color-darkred font-weight-bold">
									10月6日（火）
								</span>
								※10月8日（木）より通常営業となります。
							</p>
						</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">
							<p class="f15">
								大阪店
							</p>
						</div>
						<div class="table-cell table-cell-right">
							<p>
								<span class="font-color-darkred font-weight-bold">
									10月6日（火）
								</span>
								※10月8日（木）より通常営業となります。
							</p>
						</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">
							<p class="f15">
								仙台店
							</p>
						</div>
						<div class="table-cell table-cell-right">
							<p>
								<span class="font-color-darkred font-weight-bold">
									10月6日（火） 
								</span>
								※10月8日（木）より通常営業となります。
							</p>
						</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">
							<p class="f15">
								名古屋店
							</p>
						</div>
						<div class="table-cell table-cell-right">
							<p>
								<span class="font-color-darkred font-weight-bold">
									10月6日（火）～ 10月7日（水）
								</span>
								※10月8日（木）より通常営業となります。
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>		
		
		<div class="sd-cont09" style="margin-top:40px; margin-bottom:none; padding-bottom:2px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal; margin-bottom:10px;">関連リンク</h4>
			<div class="sd-cont09-items" style="margin-bottom:30px;">
				<div class="sd-cont09-items__img"> <img src="images/holiday/img2.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							本店
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items" style="margin-bottom:30px;">
				<div class="sd-cont09-items__img"> <img src="images/holiday/img3.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
						Organ
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items" style="margin-bottom:30px;">
				<div class="sd-cont09-items__img"> <img src="images/holiday/img4.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							FACTORY SHOP
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items" style="margin-bottom:30px;">
				<div class="sd-cont09-items__img"> <img src="images/holiday/img5.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							RESO.
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items" style="margin-bottom:30px;">
				<div class="sd-cont09-items__img"> <img src="images/holiday/img6.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							大阪店
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items" style="margin-bottom:30px;">
				<div class="sd-cont09-items__img"> <img src="images/holiday/img7.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							仙台店
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items">
				<div class="sd-cont09-items__img"> <img src="images/holiday/img8.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							名古屋店
						</a>
					</span>
				</div>
			</div>
		</div>
			
			
		
		<img src="images/holiday/img9.png" alt="" style="display: block; margin-top: 45px;">
		
	</div>
	
	
	
	
	
	
	

	
	

</div> <!-- // #main-wrapper -->