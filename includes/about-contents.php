<div id="main-wrapper">

	<div id="section-wrapper">
		<div id="accent-3" style="margin-bottom: 15px;"></div>
		<h3 id="main-title" class="main-title-attention">HERZについて<span class="mini-text-attention" style="padding-left:10px;">ABOUT HERZ</span></h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<div id="image-wrapper">
			<img src="images/about/img1.png" alt="" />
		</div>
		<div style="text-align:center;">
			<p class="f15 font-ryumin p-adj" style="margin-top:30px; margin-bottom:50px; font-size:20px; line-height:46px;">
				HERZは国内に工房・店舗を構える革鞄工房です。<br/>
				1973年、創業者 近藤晃理の<br/>
				「鞄を作ることが楽しくて仕方がない！」<br/>
				そんな思いからHERZは始まりました。
			</p>
			<p class="f15 p-adj" style="margin-top: 13px;">
				一人の青年が始めた鞄屋も40年以上が経ち、少しずつ仲間が増え、<br/>
				現在作り手・スタッフ含め70名ほどがHERZを作っています。<br/>
				作り手は日々革を裁断し、ミシンを踏み、ハンマーを握る。<br/>
				スタッフはその出来たての鞄を皆さんに手渡す。<br/>
				毎日、そんな至極シンプルな活動を続けています。
			</p>
		</div>
	</div>
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">血の通う鞄</h3>
		<div id="accent-3" style="margin-top: 5px;"></div>
		<p class="f15 p-adj" style="margin-top: 13px;">
			HERZではどんな小さな小物でも、お客様にご注文を頂いてから一つ一つお作りしています。そして、革の裁断以外の工程を分業はせずに最初から最後まで一人で作っています。「一人一本」それは創業からずっと変わらないHERZの制作スタイルです。<br/>
			少し時間はかかりますが、一本を通して作る鞄は一つ一つに作り手の人格が宿り、血の通った鞄になる。そう思っています。こうして作った鞄を「温かい」と感じてもらえたら。
		</p>
		<div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/about/img2.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
				<p class="p-adj">
					そして、いつか使う方の「愛着のある道具」となればこんなに嬉しいことはありません。革のカバンと共に過ごす楽しさ、喜び。<br/>
					それをお伝えすることが、HERZの願いです。
				</p>
				<br/>
				<p class="font-weight-bold p-adj">
					HERZはドイツ語でハート（心）。<br/>
					今までもこれからも、心ある鞄作りを続けていきます。
				</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			
		</div> <!-- # - .rlistf -->
	</div>
		
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">HERZ=丈夫</h3>
		<div id="accent-3" style="margin-top: 5px;"></div>
		<p class="f15 p-adj" style="margin-top: 13px;">
			多くの中からせっかく選んで頂いた鞄。少しでも永く使って頂きたいから、私たちは道具として「丈夫」であることを一番に、鞄を作っています。<br/>
			新作を作る時も、日々の制作をする時も、常に考えるのは「丈夫かどうか」。「永く使える物づくり」はヘルツの芯であり、永く使えるようにするにはどう作ればいいのかからスタートするのです。<br/>
			もちろん、摩耗する金具は交換をしながら付き合っていく事が前提ですが、修理に耐えうる作りを心がけています。
		</p>
		<div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/about/img3.png" alt=""> </div>
				<div class="rlistf-items__dtl" style="width: 340px"> <!-- .rlistf-items__dtl -->
				<p class="p-adj">
					丈夫かどうかを考えると、やっぱり革はより厚く、糸はより太く、金具はより大きいものが良い。だから、革はなるべく漉かずに厚いまま使用し、番手の大きな太い糸で力強く縫い上げ、目に留まる大きな金具でしっかりと留めています。
				</p>
				<br/>
				<p class="p-adj">
					革・ステッチ・金具、それはすべて丈夫さのための手段ですが、不思議とそれがデザインの一部として息づいています。だからHERZの鞄は「力強く生き生きしている」と言って頂けるのかもしれません。
				</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
		</div> <!-- # - .rlistf -->
		<div class="sd-cont09" style="margin-top:40px; margin-bottom:none;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal">関連リンク</h4>
			<div class="sd-cont09-items" >
				<div class="sd-cont09-items__img"> <img src="images/about/img4.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							HERZの鞄
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items" >
				<div class="sd-cont09-items__img"> <img src="images/about/img5.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							HERZの歴史
						</a>
					</span>
				</div>
			</div>
		</div>
	</div> 
	
</div> <!-- // #main-wrapper -->