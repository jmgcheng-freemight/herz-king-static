<!DOCTYPE HTML>
<html lang="ja">
	<head>
		<meta charset="utf-8" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0;">
		<link rel="stylesheet" href="css/reset.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="js/slick/slick.css"/>
		<link rel="stylesheet" type="text/css" href="js/slick/slick-theme.css"/>
		<link rel="stylesheet" href="css/updt-common.css" type="text/css" />
		<link rel="stylesheet" href="css/updt-style.css" type="text/css" />
		<link rel="stylesheet" href="css/updt-style-nxn.css" type="text/css" />
		<link rel="stylesheet" href="css/media-common.css" type="text/css" />
		<link rel="stylesheet" href="css/media-common-nxn.css" type="text/css" />
		<link rel="stylesheet" href="css/jquery.bxslider.css" rel="stylesheet" />
		<link rel="stylesheet" href="css/jquery.bxslider2.css" rel="stylesheet" />
		<title>HERZ</title>
	</head>
	<body>
		<div id="site-header-wrapper">
			<header id="site-header">
				<div class="banner col col-1 float-left">
					<h1 class="logo">
						<a href="http://herz.demo-page.jp/">
							HERZ
						</a>
					</h1>
					<p class="logo-note">
						革鞄/ハンドメイドレザーバッグのHERZ公式サイト
					</p>
					<div class="clear-both"></div>
				</div>
				<div class="col col-2 float-left">
					<nav class="site-menu site-header-menu-1">
						<ul>
							<li class="li-item ico-star">
								<a href="http://herz.demo-page.jp/special">特集</a>
							</li>
							<li class="li-item ico-pen">
								<a href="http://herz.demo-page.jp/blog">ブログ</a>
							</li>
							<li class="li-item ico-press">
								<a href="http://herz.demo-page.jp/press">プレスリリース</a>
							</li>
							<li class="li-item ico-contact">
								<a href="https://www.herz-bag.jp/webshop/contact/index.php">お問い合わせ</a>
							</li>
							<li class="li-item ico-organ last-item">
								<a href="https://www.organ-leather.com/">Organ</a>
							</li>
							<div class="clear-both"></div>
						</ul>
					</nav>
					<div class="clear-both"></div>
					<nav class="site-menu site-header-menu-2">
						<ul>
							<li class="li-item">
								<a href="http://herz.demo-page.jp/aboutus">ヘルツについて</a>
								<ul>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/aboutus/products/">
											HERZの鞄
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/aboutus/history">
											HERZの歴史
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/aboutus/history">
											知ってほしいこと
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/aboutus/company">
											会社概要
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/aboutus/recruit">
											採用情報
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/aboutus/shoplist">
											取扱店
										</a>
									</li>
								</ul>
							</li>
							<li class="li-item">
								<a href="http://herz.demo-page.jp/shop">直営店</a>
								<ul>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/shop">
											本店
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/factory">
											FACTORY SHOP
										</a>
									</li>
									<li class="li-item">
										<a href="http://www.organ-leather.com/" target="_blank">
											Organ
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/shop/reso/">
											RESO.
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/shop/osaka/">
											大阪店
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/shop/sendai/">
											仙台店
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/shop/nagoya/">
											名古屋店
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/shop/nagoya/">
											出張販売店
										</a>
									</li>
								</ul>
							</li>
							<li class="li-item">
								<a href="http://herz.demo-page.jp/factory">工房</a>
							</li>
							<li class="li-item">
								<a href="http://herz.demo-page.jp/material/">素材</a>
								<ul>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/material/herzleather/">
											HERZオリジナルレザー
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/material/organleather/">
											Organイタリアンレザー
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/sitemap/%E7%B5%8C%E5%B9%B4%E5%A4%89%E5%8C%96%E3%82%92%E6%A5%BD%E3%81%97%E3%82%80">
											経年変化を楽しむ
										</a>
									</li>
								</ul>
							</li>
							<li class="li-item">
								<a href="http://herz.demo-page.jp/aftercare">アフターケア</a>
								<ul>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/aftercare/repair">
											お手入れ方法
										</a>
									</li>
									<li class="li-item">
										<a href="http://herz.demo-page.jp/aftercare/repair">
											修理について
										</a>
									</li>
								</ul>
							</li>
							<li class="li-item last-item">
								<a href="https://www.herz-bag.jp/webshop/">オンラインショップ</a>
							</li>
							<div class="clear-both"></div>
						</ul>
					</nav>
					<div class="clear-both"></div>
					<nav class="site-menu site-header-menu-3">
						<ul class="list list-sp-menu" id="js-list">
							<li class="li-item">
								<a href="http://herz.demo-page.jp/shop">
									<img src="images/updt-common/ico-shop.png" />
								</a>
							</li>
							<li class="li-item last-item">
								<a href="#" class="js-menu">
									<img src="images/updt-common/ico-menu.png" />
								</a>
								
								<ul class="list list-sp-menu-sub list-hidden">
									<li class="list-item">
										
										<div class="nav nav-sp-menu nav-sp-menu-bg-brown-gray">
											
											<ul class="list list-item-bordered-bottom ">
												<li class="list-item">
													<a href="http://herz.demo-page.jp/aboutus" class="link link-sub-opener js-sub-menu" >
														ヘルツについて <i class="sprite sprite-plus-box"></i>
													</a>
													
													<ul class="list list-sp-menu-sub-quaker list-hidden">
														<li class="list-item">
															<a href="http://herz.demo-page.jp/aboutus/products/" class="link link-sub-opener">
																HERZの鞄
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/aboutus/history" class="link link-sub-opener">
																HERZの歴史
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/aboutus/history" class="link link-sub-opener">
																知ってほしいこと
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/aboutus/company" class="link link-sub-opener">
																会社概要
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/aboutus/recruit" class="link link-sub-opener">
																採用情報
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/aboutus/shoplist" class="link link-sub-opener">
																取扱店
															</a>
														</li>
													</ul>
													
												</li>
												<li class="list-item">
													<a href="http://herz.demo-page.jp/shop" class="link link-sub-opener js-sub-menu">
														直営店 <i class="sprite sprite-plus-box"></i>
													</a>
													
													<ul class="list list-sp-menu-sub-quaker list-hidden">
														<li class="list-item">
															<a href="http://herz.demo-page.jp/shop" class="link link-sub-opener">
																本店
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/factory" class="link link-sub-opener">
																FACTORY SHOP
															</a>
														</li>
														<li class="list-item">
															<a href="http://www.organ-leather.com/" class="link link-sub-opener" target="_blank">
																Organ
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/shop/reso/" class="link link-sub-opener">
																RESO.
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/shop/osaka/" class="link link-sub-opener">
																大阪店
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/shop/sendai/" class="link link-sub-opener">
																仙台店
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/shop/nagoya/" class="link link-sub-opener">
																名古屋店
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/shop/nagoya/" class="link link-sub-opener">
																出張販売店
															</a>
														</li>
													</ul>
													
												</li>
												<li class="list-item">
													<a href="http://herz.demo-page.jp/factory" class="link link-sub-opener">
														工房 <i class="sprite sprite-plus-box"></i>
													</a>
												</li>
												<li class="list-item">
													<a href="http://herz.demo-page.jp/material/" class="link link-sub-opener js-sub-menu">
														素材 <i class="sprite sprite-plus-box"></i>
													</a>
													
													<ul class="list list-sp-menu-sub-quaker list-hidden">
														<li class="list-item">
															<a href="http://herz.demo-page.jp/material/herzleather/" class="link link-sub-opener">
																HERZオリジナルレザー
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/material/organleather/" class="link link-sub-opener">
																Organイタリアンレザー
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/sitemap/%E7%B5%8C%E5%B9%B4%E5%A4%89%E5%8C%96%E3%82%92%E6%A5%BD%E3%81%97%E3%82%80" class="link link-sub-opener">
																経年変化を楽しむ
															</a>
														</li>
													</ul>
													
												</li>
												<li class="list-item">
													<a href="http://herz.demo-page.jp/aftercare" class="link link-sub-opener js-sub-menu">
														アフターケア <i class="sprite sprite-plus-box"></i>
													</a>
													
													<ul class="list list-sp-menu-sub-quaker list-hidden">
														<li class="list-item">
															<a href="http://herz.demo-page.jp/aftercare/repair" class="link link-sub-opener">
																お手入れ方法
															</a>
														</li>
														<li class="list-item">
															<a href="http://herz.demo-page.jp/aftercare/repair" class="link link-sub-opener">
																修理について
															</a>
														</li>
													</ul>
													
												</li>
												<li class="list-item">
													<a href="https://www.herz-bag.jp/webshop/" class="link link-sub-opener">
														オンラインショップ <i class="sprite sprite-plus-box"></i>
													</a>
												</li>
												<li class="list-item">
													<div class="nav nav-sp-menu nav-sp-menu-bg-brown">
														<table class="table table-menu align-center">
															<tr>
																<td>
																	
																	<a href="http://herz.demo-page.jp/special" class="link link-padded">
																		<i class="sprite sprite-star"></i> 特集
																	</a>
																</td>
																<td>
																	<a href="http://herz.demo-page.jp/blog" class="link link-padded">
																		<i class="sprite sprite-pen"></i> ブログ
																	</a>
																</td>
															</tr>
															<tr>
																<td>
																	<a href="http://herz.demo-page.jp/press" class="link link-padded">
																		<i class="sprite sprite-press"></i> プレスリリース
																	</a>
																</td>
																<td>
																	<a href="https://www.herz-bag.jp/webshop/contact/index.php" class="link link-padded">
																		<i class="sprite sprite-contact"></i> お問い合わせ
																	</a>
																</td>
															</tr>
															<tr>
																<td colspan="2">
																	<a href="https://www.organ-leather.com/" class="link link-padded">
																		<i class="sprite sprite-organ"></i> Organ
																	</a>
																</td>
															</tr>
														</table>
														
													</div>
												</li>
											</ul>
											
										</div>
										
									</li>
								</ul>

							</li>
						</ul>
					</nav>
				</div>
				<div class="clear-both"></div>
			</header>
		</div>