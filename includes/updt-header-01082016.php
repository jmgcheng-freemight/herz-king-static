<!DOCTYPE HTML>
<html lang="ja">
	<head>
		<meta charset="utf-8" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0;">
		<link rel="stylesheet" href="css/reset.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="js/slick/slick.css"/>
		<link rel="stylesheet" type="text/css" href="js/slick/slick-theme.css"/>
		<link rel="stylesheet" href="css/updt-common.css" type="text/css" />
		<link rel="stylesheet" href="css/updt-style.css" type="text/css" />
		<link rel="stylesheet" href="css/media-common.css" type="text/css" />
		<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="js/slick/slick.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
		<title>HERZ</title>
	</head>
	<body>
		<div id="site-header-wrapper">
			<header id="site-header">
				<div class="banner col col-1 float-left">
					<h1 class="logo">
						<a href="#">
							HERZ
						</a>
					</h1>
					<p class="logo-note">
						革鞄/ハンドメイドレザーバッグのHERZ公式サイト
					</p>
					<div class="clear-both"></div>
				</div>
				<div class="col col-2 float-left">
					<nav class="site-menu site-header-menu-1">
						<ul>
							<li class="li-item ico-star">
								<a href="#">特集</a>
							</li>
							<li class="li-item ico-pen">
								<a href="#">ブログ</a>
							</li>
							<li class="li-item ico-press">
								<a href="#">プレスリリース</a>
							</li>
							<li class="li-item ico-contact">
								<a href="#">お問い合わせ</a>
							</li>
							<li class="li-item ico-organ last-item">
								<a href="#">Organ</a>
							</li>
							<div class="clear-both"></div>
						</ul>
					</nav>
					<div class="clear-both"></div>
					<nav class="site-menu site-header-menu-2">
						<ul>
							<li class="li-item">
								<a href="#">ヘルツについて</a>
							</li>
							<li class="li-item">
								<a href="#">直営店</a>
							</li>
							<li class="li-item">
								<a href="#">工房</a>
							</li>
							<li class="li-item">
								<a href="#">素材</a>
							</li>
							<li class="li-item">
								<a href="#">アフターケア</a>
							</li>
							<li class="li-item last-item">
								<a href="#">オンラインショップ</a>
							</li>
							<div class="clear-both"></div>
						</ul>
					</nav>
					<div class="clear-both"></div>
					<nav class="site-menu site-header-menu-3">
						<ul>
							<li class="li-item">
								<a href="#">
									<img src="images/updt-common/ico-shop.png" />
								</a>
							</li>
							<li class="li-item last-item">
								<a href="#">
									<img src="images/updt-common/ico-menu.png" />
								</a>
							</li>
						</ul>
					</nav>
				</div>
				<div class="clear-both"></div>
			</header>
		</div>