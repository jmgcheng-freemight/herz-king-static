		
		<div class="clear-both"></div>
		<div id="site-footer-wrapper">
			<footer id="site-footer">
				<div class="banner col col-1 float-left">
					<h1 class="logo">
						<a href="#">HERZ</a>
					</h1>
					<p class="slogan">
						時を経てこそ解る味わいがある。使い込んでこそ伝わる温もりがある。<br/>
						デザインから製作まで１人の作り手が心を込めて最後まで仕上げるもの作り。<br/>
						それがヘルツのスピリット。
					</p>
					<div class="clear-both"></div>
				</div>
				<div class="col col-2 float-left">
					<nav class="site-menu site-footer-menu">
						<ul>
							<li class="li-item ico-twitter">
								<a href="#">TWITTER</a>
							</li>
							<li class="li-item ico-fb">
								<a href="#">FACEBOOK</a>
							</li>
							<li class="li-item ico-tumbler">
								<a href="#">TUMBLER</a>
							</li>
							<li class="li-item ico-mail-mag">
								<a href="#">MAIL MAGAZINE</a>
							</li>
							<li class="li-item ico-sitemap">
								<a href="#">SITE MAP</a>
							</li>
							<li class="li-item ico-online-shop last-item">
								<a href="#">ONLINE SHOP</a>
							</li>
							<div class="clear-both"></div>
						</ul>
					</nav>
					<div class="clear-both"></div>
					<p class="copyright">
						Copyright (c) 2000-2015 HERZ Co.,Ltd. All Rights Reserved.
					</p>
				</div>
				<div class="clear-both"></div>
			</footer>
		</div>
		
		<a href="#" class="page-top-img"><img src="images/updt-common/page-top.png" alt=""/></a>
		
	</body>
</html>