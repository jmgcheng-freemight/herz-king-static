<div id="main-wrapper">

	<div id="section-wrapper">
		<div id="accent-3" class="mb10"></div>
		<h3 id="main-title" class="font-ryumin">HERZの歴史<span class="mini-text">HISTORY</span></h3>
		<div id="accent-3" class="mt10"></div>
		<div id="image-wrapper" class="mb40">
			<img src="images/history/img1.png" alt="" />
		</div>
		<p class="f15 mb30">
			創業者の近藤晃理がひょんなことから1枚の革と出会い、初めて革袋を作った時に感じた「作りたい!楽しい!!」その気持がHERZのはじまりでした。少しずつ道具が増え作れる鞄が増えていき、近藤の鞄作りに賛同する仲間も増え、HERZの輪はほんとうに少しずつ広がっていきました。「丈夫な鞄」「道具としての鞄」を作り続けて40数年。これまでのHERZの歩みが、使う方のほんの少しのプラスの楽しみになれば幸いです。
		</p>
		<nav class="site-menu history-year-menu mb60">
			<ul>
				<li>
					<span class="glyph-icon glyph-icon-arrow"></span>
					<a href="#" class="font-ryumin">
						1970年代
					</a>
				</li>
				<li>
					<span class="glyph-icon glyph-icon-arrow"></span>
					<a href="#" class="font-ryumin">
						1980年代
					</a>
				</li>
				<li>
					<span class="glyph-icon glyph-icon-arrow"></span>
					<a href="#" class="font-ryumin">
						1990年代
					</a>
				</li>
				<li>
					<span class="glyph-icon glyph-icon-arrow"></span>
					<a href="#" class="font-ryumin">
						2000年代
					</a>
				</li>
				<li class="history-year-menu-li-last">
					<span class="glyph-icon glyph-icon-arrow"></span>
					<a href="#" class="font-ryumin">
						2010年代
					</a>
				</li>
			</ul>
			<div class="clear-both"></div>
		</nav>
		
		<div class="company-history">
			<h3 class="company-history-h3 company-history-h3-first font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">1970年頃</span>「創業者近藤、革と出会う」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img2.png" />
			<p>
				「引っ越すんだけど、物置にあった革いらない？」と当時勤めていた会社の同僚から1枚の革を貰った創業者の近藤晃理。「革ってこんなんなってるんだ」と初めて革を知ることに。<br/> 早速貰った革で建築用の製図やデザイン画を入れる為のB3サイズの革袋を作ります。作ってみると周りからの反応も良く、何より作る事が楽しくて仕方がなかったそうです。この時の「これが仕事になったらどんなに楽しいだろう！」この気持ちがHERZの原点です。
			</p>
			<div class="mb80"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">1973年</span>「本格的に鞄作りを始める＝HERZの誕生」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img3.png" />
			<p>
				近藤は「1枚の革」を貰ってからというもの、自分で立ち上げたデザイン会社の仕事の合間を見つけては趣味程度に細々と革袋を作る日々を送っていました。そこへ大学の同級生から「共同で創作の場を借りない？」と持ちかけられます。これがきっかけとなり「それなら本格的に鞄作りをしたい！」と、屋号を「HERZ」と決め本格的に鞄作りを始めることに。
			</p>
			<div class="mb25"></div>
			<p class="mb20">
				赤坂にある半地下駐車場（ガレージ）の半分を友人が、もう半分を近藤が革鞄工房＆デザインの場として改装し、使い始めたのでした。ミシンを持っていなかった近藤は、革紐をからげたバッグやカシメで留めるだけの鞄を中心に作り始めます。
			</p>
			<img class="mr25 company-history-article-image" src="images/history/img4.png" />
			<img class="company-history-article-image" src="images/history/img5.png" />
			<div class="mb80 clear-both"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">1975年</span>「ミシンを手に入れる」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img6.png" />
			<p>
				ミシンを探していたところに「浅草の鞄屋さんが店を閉めるんだけどミシンいらない？」と声をかけてもらい、初めての革用ミシン「シンガー5K」を手に入れます。<br/> 
				5Kはシンガーミシンの中でも極厚物用のミシンで、図らずも当時近藤が作っていた厚い革に合うものでした。HERZの特徴である「厚い革を太い糸で縫う、丈夫な鞄」を作り続けてこれたのも「このミシンのおかげ」だといいます。
			</p>
			<div class="mb25"></div>
			<p>
				そして、この頃から今でも定番商品として残る鞄達の原型が、沢山誕生しました。
			</p>
			<div class="mb20"></div>
			<div class="image-gallery company-history-gallery">
				<div class="image-gallery-slide">
					<img class="" src="images/history/img7.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							元祖箱型2wayショルダーバッグ【A-23】
						</a>
					</p>
				</div>
				<div class="image-gallery-slide">
					<img class="" src="images/history/img8.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							太ベルト・ショルダーバッグ【A-40】
						</a>
					</p>
				</div>
				<div class="image-gallery-slide image-gallery-slide-last">
					<img class="" src="images/history/img9.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							 ハードギャジット・カメラバッグ【N-7】
						</a>
					</p>
				</div>
				<div class="clear-both"></div>
			</div>
			<div class="mb40"></div>
			<p>
				ちょうどこの頃銀座と新宿にHERZの鞄を取り扱ってくれるお店を見つけます。<br/>
				鞄の売り方がわからなかった近藤は、「現物を見てもらうのが一番だ！」と、仕上がった鞄を持ち歩いて「鞄を作ったんだけど売ってくれませんか？」と銀座、新宿を練り歩いたそうです。「非常識だ！」「いらない」と散々な反応だったようですが、有難いことに面白がってくれる鞄屋さんがいくつかあり、鞄を置いてくれるようになります。<br/>
				この頃、雑誌に取り上げられたこともあり、鞄は一向に売れない日々でしたが「教えてほしい」という人が沢山いたそうです。雇うことは出来ないからと教室をやってみた近藤。毎日多い時で20～30人が集まり、作りたいものを作っては売れた時に皆で喜んで、和気あいあいと活動していたようです。
			</p>
			<div class="mb30"></div>
			<img class="mr25 company-history-article-image" src="images/history/img10.png" />
			<img class="company-history-article-image" src="images/history/img11.png" />
			<div class="mb80 clear-both"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">1970年後半</span>「オリジナルレザーの誕生」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img12.png" />
			<p>
				鞄を作り始めた当初は、お客さんの要望に合った革をその都度秋葉原にある革屋さんへ行き一枚一枚買っていた近藤。そんなある時、浅草の革屋さんで今のHERZのオリジナルレザー、ラティーゴハードレザーの様な極厚のヌメ革を見つけます。そのなんとも言えない素朴な魅力に「いいなぁ、いつかはあんな革で鞄を作りたい・・」と強く思ったそうです。
			</p>
			<div class="mb30"></div>
			<p>
				そうこうするうちに、鞄を作り始めて2年が経ちやっとその革を買えるようになります。お店の人から「使うと味が出ますよ。」という説明を受け「やっぱりものすごくいいな」と。これはもう感覚だったそう。
			</p>
			<div class="mb30"></div>
			<p>
				それからまた月日が経ったころ、革屋さんで買うには足りないほど沢山の革が必要になります。そこで革屋さんの助けもありオリジナルの革を作ってもらうことに。以前から「ヌメ革をちょっと使ったような色にしたいな」と思っていた近藤は、折角だからということで、オリジナルの色に染めて革を作ってもらうことに。何枚ものヌメ革に、時間を調節して日に当て「コレだ！」と思う色の革を革屋さんに持って行き再現してもらったといいます。
			</p>
			<div class="mb30"></div>
			<p class="font-ryumin f17">
				「もちろん、1000年も前から皮革の文化が根付いているイタリアの革は最高級。オイルもふんだんに入っていて、まさに革そのものの雰囲気を存分に楽しめるから大好き。だけど、あの売れなかった時代に目にした、今のラティーゴみたいな素朴な革の魅力、それへのあこがれは消えない」
			</p>
			<div class="mb30"></div>
			<p>
				40年近くたった今でも、近藤はこう話します。<br/>
				HERZの象徴であるラティーゴハードレザーのキャメルは、この近藤の強い憧れから誕生したようです。
			</p>
			<div class="mb30"></div>
			<p>
				そしてその後、もう少しカジュアルな鞄を作りたいと内縫いができる少し柔らかさのある革（ラティーゴソフトレザー）が生まれ少しずつ鞄のバリエーションも増えていきました。
			</p>
			<div class="mb80"></div>
			<div class="image-gallery company-history-gallery company-history-gallery-320">
				<div class="image-gallery-slide">
					<img class="" src="images/history/img13.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							フトタイプの2wayショルダービジネス(BF-1)
						</a>
					</p>
				</div>
				<div class="image-gallery-slide image-gallery-slide-last">
					<img class="" src="images/history/img14.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							 ベーシック二本ベルトリュック(R-7)
						</a>
					</p>
				</div>
				<div class="clear-both"></div>
			</div>
			<div class="mb80"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">1983年</span>「東京都渋谷区に店舗兼工房として青山本店オープン」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img15.png" />
			<p>
				赤坂のガレージから渋谷に場所を移し、本店として店舗兼工房を構えます。場所を借りた当時、とにかくお金がなかったということでコンクリートのまま、ダクトもむき出し、暑いから上にファンだけつけて、ワンフロアを前半分はお店、仕切った後ろ半分を工房スペースに。<br/>
				資金がない為にやむなくそうなった空間での革鞄作りは、やっていることはクラフトなのに、現場は無機質で真新しく雑誌に「前衛的」「パリの路地裏にあるような工房」と取り上げられたこともあったそうです。 そして、HERZ一番のベテランの作り手：NEZはこの頃入社します。 パリの路地裏にあるような工房」と取り上げられたこともあったそうです。そして、HERZ一番のベテランの作り手：NEZはこの頃入社します。
			</p>
			<div class="mb40"></div>
			<img class="mr25 company-history-article-image" src="images/history/img16.png" />
			<img class="company-history-article-image" src="images/history/img17.png" />
			<div class="mb80 clear-both"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">1980年代後半</span>「小物が誕生し、革色も増える。」
			</h3>
			<p>
				鞄作りに力を入れていた為、当時小物類はほとんどありませんでした。そんな中でもペンケースは、近藤が万年筆などを持ち歩くという理由から今あるペンケースの殆どがこの頃に生まれました。<br/>その後、スタッフからの「お財布がほしい！」と声が上がりスタンダードな二つ折り財布がが誕生し、革色も黒、チョコ、グリーン、赤の順に展開が増えていきました。革がカラフルになると、作り手の思考が手軽に使える革小物へも移り、少しずつ小物のラインナップも充実していきます。
			</p>
			<div class="mb40"></div>
			<div class="image-gallery company-history-gallery company-history-gallery-320">
				<div class="image-gallery-slide">
					<img class="" src="images/history/img18.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							トレー式ペンケース(KP-5)
						</a>
					</p>
				</div>
				<div class="image-gallery-slide image-gallery-slide-last">
					<img class="" src="images/history/img19.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							 二つ折り財布(WS-5)
						</a>
					</p>
				</div>
				<div class="clear-both"></div>
			</div>
			<div class="mb100"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">1994年</span>「渋谷に工房増設」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img20.png" />
			<p>
				作る量も人数も増えに増え手狭になった為、青山本店の近く（現渋谷工房の地下）に工房スペースを借りることに。新入社員であった鈴木（裁断担当の作り手）と近藤で、ペンキを塗ったり、什器を運んだりと一から工房を作り上げます。
			</p>
			<div class="mb70"></div>
			<p>
				その後、地下のみだった工房の1階スペースも借りることになり、自然と工房前で試作したサンプル品などを販売し始めます。それがFACTORY SHOPのはじまりに。
			</p>
			<div class="mb10"></div>
			<a href="" class="link font-ryumin"> <img src="images/aging/arrow-right.png" alt="">FACTORY SHOP</a>
			<div class="mb10"></div>
			<p>
				そして、この頃HERZとして初めてのダレスバッグを作り手：NEZが作り、その後近藤がダレスバッグをもう少しカジュアルに持てるようにと、HERZのロングセラーとなるソフトダレスバッグを生み出します。
			</p>
			<div class="mb30"></div>
			<div class="image-gallery company-history-gallery company-history-gallery-320">
				<div class="image-gallery-slide">
					<img class="" src="images/history/img21.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							 ダレスバッグ・スタンダードタイプ【BJ-2】
						</a>
					</p>
				</div>
				<div class="image-gallery-slide image-gallery-slide-last">
					<img class="" src="images/history/img22.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							 ソフトダレスバッグ【BJ-68】
						</a>
					</p>
				</div>
				<div class="clear-both"></div>
			</div>
			<div class="mb90"></div>

			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">2000年</span>「オンラインショップオープン」
			</h3>
			<!-- 
			<img class="mr25 company-history-article-image" src="images/history/img23.png" />
			<a href="#" class="font-ryumin">オンラインショップ</a>
			<p>
				ウェブ上での直営販売（オンラインショップ）をひっそりと開始。日本では大手ECモールがやっと立ち上がった頃でした。
			</p>
			<div class="mb30"></div>
			<p>
				認知度もまだ低く、HERZにはその道のプロもいませんでしたが、好奇心旺盛な近藤の「面白そうだからやってみよう！」そんな一言で、当時在籍していたスタッフたちでなんとか立ち上げます。
			</p>
			<div class="mb70"></div>
			-->
			
			<!-- 
			-->
			<div class="mb30"></div>
			<div class="image-gallery company-history-gallery company-history-gallery-320">
				<div class="image-gallery-slide">
					<img class="" src="images/history/img23.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							 オンラインショップ
						</a>
					</p>
				</div>
				<div class="image-gallery-slide image-gallery-slide-ponly image-gallery-slide-last">
					<p>
						ウェブ上での直営販売（オンラインショップ）をひっそりと開始。日本では大手ECモールがやっと立ち上がった頃でした。
					</p>
					<div class="mb30"></div>
					<p>
						認知度もまだ低く、HERZにはその道のプロもいませんでしたが、好奇心旺盛な近藤の「面白そうだからやってみよう！」そんな一言で、当時在籍していたスタッフたちでなんとか立ち上げます。
					</p>
				</div>
				<div class="clear-both"></div>
			</div>
			<div class="mb70"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">2001年</span>「青山通りにTOM DICK＆HARRYオープン」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img24.png" />
			<p>
				青山本店がだんだん軌道に乗り、初めに作った鞄たちも定番商品として定着し始めた頃。<br/> 少しずつ順調になるに連れて、初めて鞄を作った時の感動みたいなものが段々と薄れてしまっていることに気づいた近藤。
			</p>
			<div class="mb120"></div>
			<p>
				「すごいのができたぞー！」と皆に言ってしまうくらいの気持ちをもう一度思い出したい！そんな思いから、初期モデルよりももっとゴツい物を作ってみようと思い立ちWシリーズが生まれます。
			</p>
			<div class="mb30"></div>
			<div class="image-gallery company-history-gallery">
				<div class="image-gallery-slide">
					<img class="" src="images/history/img25.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							ボンサック【W-1】
						</a>
					</p>
				</div>
				<div class="image-gallery-slide">
					<img class="" src="images/history/img26.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							メッセンジャーバッグ【W-3】
						</a>
					</p>
				</div>
				<div class="image-gallery-slide image-gallery-slide-last">
					<img class="" src="images/history/img27.png" />
					<p>
						<span class="glyph-icon glyph-icon-arrow"></span>
						<a href="#" class="font-ryumin">
							 パラシュートリュック【W-5】
						</a>
					</p>
				</div>
				<div class="clear-both"></div>
			</div>
			<div class="mb30"></div>
			<p>
				このWシリーズを置ける店舗、そして定番商品を扱う青山本店にはおけない様な皆の作った物をなんでも置ける挑戦の場として屋号を「TOM DICK＆HARRY」とし青山通りにお店をオープンします。
			</p>
			<div class="mb30"></div>
			<img class="mr25 company-history-article-image" src="images/history/img28.png" />
			<p>
				ちなみに「TOM DICK＆HARRY」の意味は「有象無象」「みんな」。<br/> 「HERZの誰もかれもが面白い商品を置けるお店に！」「みんなに愛されるお店に！」そんな思いが込められていました。
			</p>
			<div class="mb120"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">2007年</span>「博多に工房を構える」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img29.png" />
			<p>
				HERZの製品を沢山取り扱ってくださるお店さんが博多にあったこと、そして都会の便利さと自然の豊かさが混在している感度の高い街に工房を構えることで、色々なモノ・コトを吸収できればということで、作る事に専念する場として博多工房を構えます。
			</p>
			<div class="mb120"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">2009年12月</span>「渋谷工房の2軒隣にOrganオープン」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img30.png" />
			<p>
				HERZとは異なる革素材（イタリアンレザー）で、道具としての鞄の可能性を探る工房兼お店としてOrganをオープン。初期メンバーは作り手：桃井、マリ、村松。<br/> 当時まだ若手だった3人で新商品を生み出すべく試作をする日々を送ります。店舗の佇まい、制作する物の雰囲気もHERZとはまた違ったよりシンプルな形での道具としての鞄を追求しています。
			</p>
			<a href="" class="link font-ryumin"> <img src="images/aging/arrow-right.png" alt="">Organ BRAND SITE</a>
			<div class="mb90"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">2011年3月</span>「初の他県出店となるHERZ大阪店オープン」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img31.png" />
			<p>
				それまで直営店舗は東京のみでしたが、遠方からのお客様、特に関西からのお客様が多くなり「東京の他でも鞄を見たい！」という何とも嬉しいお言葉を沢山頂くようになります。そこでまずやったのが、「青山本店を出張させちゃおう！」という出張販売でした。第一回目を一番要望の多かった大阪で実施。
			</p>
			<div class="mb60"></div>
			<p>
				当日は本当に大盛況でたくさんのお客様が来てくださり「こんなに喜んでくれる人が、大阪にこんなにも沢山いるんだ」と実感します。<br/> その翌年も大阪での出張販売を行い、大阪に店舗を作りたいという気持ちが皆の中で固まっていきます。第一回目の出張販売から2年後、現代表の野口と作り手：根本が中心となり大阪に他県初の工房兼店舗としてHERZ大阪店をオープンします。
			</p>
			<a href="" class="link font-ryumin"> <img src="images/aging/arrow-right.png" alt="">HERZ大阪店</a>
			<div class="mb90"></div>
			
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">2012年11月</span>「HERZ仙台店オープン」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img32.png" />
			<p>
				渋谷工房の製作リーダーであった作り手の清水が、地元仙台でお店を開きたいとの強い希望から仙台にゆかりのある作り手：増澤と共にHERZ仙台店をオープンします。とにかく器用なこの二人。<br/>前職で土木作業に関わっていた事もあり、棚や什器はもちろんスケルトンの状態から遮音シート張り、石膏ボード張りの段階から全てを自分たちで作り上げたクラフト感溢れる店内になっています。
			</p>
			<a href="" class="link font-ryumin"> <img src="images/aging/arrow-right.png" alt="">HERZ仙台店</a>
			<div class="mb90"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">2013年</span>「創業者 近藤晃理 一線を退く」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img33.png" />
			<p>
				これまでずっとHERZを引っ張ってきた創業者の近藤晃理が一線を退き、代表が野口裕明に変わります。
			</p>
			<a href="" class="link font-ryumin"> <img src="images/aging/arrow-right.png" alt="">40THサイト「創業者と二代目によるざっくばらんなHERZ談」</a>
			<br/>
			<div class="mb120"></div>
			
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">2013年12月</span>「青山本店･TOM DICK＆HARRYクローズ」
			</h3>
			<p>
				「HERZの全商品を一堂にお披露目できる空間を作りたい。」<br/>
				その強い思いから青山本店・TOM DICK ＆ HARR Y・工房を統合したHERZ最大の工房兼店舗をオープンさせるため、両店を一旦クローズすることに。
			</p>
			<div class="mb35"></div>
			<div class="image-gallery company-history-gallery company-history-gallery-320">
				<div class="image-gallery-slide">
					<img class="" src="images/history/img34.png" />
					<p>
						 青山本店
					</p>
				</div>
				<div class="image-gallery-slide image-gallery-slide-ponly image-gallery-slide-last">
					<p>
						ヘルツらしいハードレザーを使ったクラシックモデルやWシリーズ、ビジネスバッグなどかっちりとした鞄やワイルドな男性的な鞄を扱っていた青山本店。
					</p>
				</div>
				<div class="clear-both"></div>
			</div>
			<div class="mb40"></div>
			<div class="image-gallery company-history-gallery company-history-gallery-320">
				<div class="image-gallery-slide">
					<img class="" src="images/history/img35.png" />
					<p>
						 TOM DICK＆HARRY
					</p>
				</div>
				<div class="image-gallery-slide image-gallery-slide-ponly image-gallery-slide-last">
					<p>
						オープン当初はワイルドなWシリーズ等を置いていましたが、徐々に本店とは違った軽めでポップな商品を扱うお店へと変わっていったTOM DICK＆HARRY。<br/>どちらのお店も最後まで、色々なお客様に愛して頂けたお店でした。
					</p>
				</div>
				<div class="clear-both"></div>
			</div>
			<div class="mb90"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">2014年1月</span>「HERZ本店オープン」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img36.png" />
			<p>
				創業当初の青山本店のようにお店の奥に工房があり、作り手の顔が見える空間で鞄を販売したいとの思いから、青山本店・TOM DICK ＆ HARRY・工房を統合したHERZ最大の店舗兼工房を渋谷区神宮前にオープンします。<br/>
				これまでの店舗同様、代表野口、作り手：北野、スタッフ：上広を中心に店舗、工房内の棚やディスプレイ、作業台等を手作りし本店を作り上げていきました。そして、HERZ本店には創業時に近藤が革を買いに行っていた秋葉原の革屋さんで使用していた什器を沢山使わせていただいています。<br/>
				HERZ本店のオープン数カ月前に閉店してしまったその革屋さんでは、直前までHERZの製品を販売してくださっていました。そんな縁もあり、新しさとこれまでのHERZを感じるお店兼工房となりました。
			</p>
			<a href="" class="link font-ryumin"> <img src="images/aging/arrow-right.png" alt="">HERZ本店</a>
			<br/>
			<a href="" class="link font-ryumin"> <img src="images/aging/arrow-right.png" alt="">HERZ本店ができるまで</a>
			<br/>
			<div class="mb90"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">2014年5月</span>「青山本店のあった場所にRESO.オープン」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img37.png" />
			<p>
				青山本店の閉店が決まった頃、スタッフの中で「青山本店の場所を残したい」という声が多く上がります。そこで青山本店と同じ場所にベテランNEZと作り手ZONO二人によるお店「RESO.」をオープンします。HERZでは通常行っていない鞄のフルオーダーを中心に、新しい素材・ジャンルの物を作る工房兼店舗です。
			</p>
			<a href="" class="link font-ryumin"> <img src="images/aging/arrow-right.png" alt="">RESO.</a>
			<div class="mb90"></div>
			
			<h3 class="company-history-h3 font-ryumin">
				<span class="glyph-icon glyph-icon-bullet-brown company-history-header-bullet"></span>
				<span class="company-history-header-year">2016年</span>「HERZ名古屋店オープン」
			</h3>
			<img class="mr25 company-history-article-image" src="images/history/img38.png" />
			<p>
				出張販売を企画した際、開催希望地として大阪の次に要望が多かったのが名古屋でした。第二回目の出張販売は名古屋でも行い、東海地方にお住まいの皆さんがとても歓迎して下さいました。<br/>
				出張販売の発起人である現代表の野口は「いつかは名古屋にも直営店を。」そんな風に考えていたようです。約一年の工房期間を経て、2016年1月にオープンしました。比較的若手のメンバーが中心となり、エネルギーのある店舗に出来上がっています。
			</p>
			<a href="" class="link font-ryumin"> <img src="images/aging/arrow-right.png" alt="">HERZ名古屋店</a>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<div class="mb60"></div>
			<div class="clear-both"></div>
		</div>
	</div>
</div> <!-- // #main-wrapper -->