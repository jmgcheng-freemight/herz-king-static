<div id="main-wrapper">

 <div id="section-wrapper">
  <div id="accent-3" style="margin-bottom: 16px;"></div>
  <h3 id="main-title">HERZ オリジナルレザー<span class="mini-text">-ORIGINAL LEATHER-</span></h3>
  <div id="accent-3" style="margin-top: 11px;"></div>
  <div id="image-wrapper">
   <img src="images/leather/img1.png" alt="" />
  </div>
 </div>

 <div id="section-wrapper">
  <h3 id="main-title">堅牢で革本来の風合いを活かした革</h3>
  <div id="accent-3" style="margin-top: 4px;"></div>

  <div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
   <div class="rlistf-items mt20"><!-- .rlistf-items -->
    <div class="rlistf-items__img"> <img src="images/leather/img2.png" alt=""/> </div>
    <div class="rlistf-items__dtl" style="margin-top: -7px;"> <!-- .rlistf-items__dtl -->
     <p>これがHERZの求める理想の革です。そのために多くの時間と手間を必要としますが、素材を厳選し、植物タンニンでなめし、出来るだけ少ない表面加工を行い、素顔に近い革に仕上げています。
      実際に作り手がタンナーと何度も話し合いをし、試作を重ねていく中で完成したHERZオリジナルレザーです。</p>
    </div> <!-- // .rlistf-items__dtl -->
   </div><!-- // .rlistf-items -->

   <div class="rlistf-items mt50"><!-- .rlistf-items -->
    <div class="rlistf-items__img"> <img src="images/leather/img3.png" alt=""/> </div>
    <div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
     <p class="dotted">
      ※タンナーとは <br/>
      皮から革に作り上げる職人さんたちのことをタンナーと呼びます。動物の皮を鞣（なめ）して、腐らないように処理し、素材として使える革に仕上げる。<br/>
      HERZの革製品を作る上でも欠かせない存在です。
     </p>
    </div> <!-- // .rlistf-items__dtl -->
   </div><!-- // .rlistf-items -->
  </div> <!-- # - .rlistf -->
 </div> <!-- #section-wrapper -->

 <span id="section" name="elem2"></span>
 <div id="section-wrapper">
  <h3 id="main-title">HERZで使用している革は大きく分けて2種類</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>

  <h3 id="main-title" style="display: block; margin: 50px 0;">ラティーゴ（ハードレザー）</h3>
  <img src="images/leather/img4.png" alt=""/>
  <p class="f15 mt15">
   スタンダードなスムースレザー。堅牢で厚みがあるのが特徴。ラティーゴレザーの中でも、ハードとソフトレザータイプがあります。どんな鞄にも対応できるように厚みを何種類か分けてお作りしています。
  </p>

  <h3 id="main-title" style="display: block; margin: 50px 0;">スターレ（ソフトレザー）</h3>
  <img src="images/leather/img5.png" alt=""/>
  <p class="f15 mt10">
   革本来のシボ・シワが特徴的なソフトレザー。シボの入り具合も一枚の革の中でも全く異なります。革質はやわらかく厚みは、ラティーゴレザーに比べ薄く仕上げています。
  </p>
 </div> <!-- #section-wrapper -->

 <span id="section" name="elem3"></span>
 <div id="section-wrapper">
  <h3 id="main-title">革色は5色の中から選んでお作りいただけます</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>

  <div class="lc-model mt20"> <!-- lc-model -->
   <div class="lc-model__item"> <!-- lc-model__item -->
    <img src="images/leather/img6.png" alt=""/>
    <div class="lc-model__cpt mt10">キャメル</div>
   </div> <!-- // lc-model__item -->

   <div class="lc-model__item"> <!-- lc-model__item -->
    <img src="images/leather/img7.png" alt=""/>
    <div class="lc-model__cpt mt10">キャメル</div>
   </div> <!-- // lc-model__item -->

   <div class="lc-model__item"> <!-- lc-model__item -->
    <img src="images/leather/img8.png" alt=""/>
    <div class="lc-model__cpt mt10">キャメル</div>
   </div> <!-- // lc-model__item -->

   <div class="lc-model__item"> <!-- lc-model__item -->
    <img src="images/leather/img9.png" alt=""/>
    <div class="lc-model__cpt mt10">キャメル</div>
   </div> <!-- // lc-model__item -->

   <div class="lc-model__item"> <!-- lc-model__item -->
    <img src="images/leather/img10.png" alt=""/>
    <div class="lc-model__cpt mt10">キャメル</div>
   </div> <!-- // lc-model__item -->
  </div> <!-- //lc-model -->

  <p class="f15 mt25"><strong>※革の染色も、職人が手作業で行なっているため、同じ革色でもその時によって、色味の違いが生じます。</strong></p>

  <h3 id="main-title" style="display: block; margin: 50px 0;">キャメル</h3>
  <div class="lprod ml15" > <!-- .lprod -->
   <div class="prod-item"> <!-- .prod-item -->
    <div class="prod-item__thumb"><img src="images/leather/img11.png" alt=""/></div>
    <p class="prod-item__dtl"style="margin-left:-20px; padding-top: 4px;">革：ラティーゴ</p>
   </div> <!-- // .prod-item -->

   <div class="prod-item"> <!-- .prod-item -->
    <div class="prod-item__thumb"><img src="images/leather/img12.png" alt=""/></div>
    <p class="prod-item__dtl"style="margin-left: 23px; padding-top: 0px;">革：スターレ</p>
   </div> <!-- // .prod-item -->
  </div> <!--// .lprod -->


  <h3 id="main-title" style="display: block; margin: 56px 0;">チョコ</h3>
  <div class="lprod ml20"> <!-- .lprod -->
   <div class="prod-item"> <!-- .prod-item -->
    <div class="prod-item__thumb"><img src="images/leather/img13.png" alt=""/></div>
    <p class="prod-item__dtl " style="margin-left: -25px;">革：ラティーゴ</p>
   </div> <!-- // .prod-item -->

   <div class="prod-item"> <!-- .prod-item -->
    <div class="prod-item__thumb"><img src="images/leather/img14.png" alt="" class="ml50" style="top: 17px; position:relative" /></div>
    <p class="prod-item__dtl mt10 ml15" >革：スターレ</p>
   </div> <!-- // .prod-item -->
  </div> <!--// .lprod -->


  <h3 id="main-title" style="display: block; margin: 50px 0;">ブラック</h3>
  <div class="lprod " style="margin-left: 19px;"> <!-- .lprod -->
   <div class="prod-item"> <!-- .prod-item -->
    <div class="prod-item__thumb"><img src="images/leather/img15.png" alt=""/></div>
    <p class="prod-item__dtl" style="padding-top:15px; margin-left:-19px; top:-8px; position:relative;">革：ラティーゴ</p>
   </div> <!-- // .prod-item -->

   <div class="prod-item" style="top:-7px; left:2px; position:relative"> <!-- .prod-item -->
    <div class="prod-item__thumb"><img src="images/leather/img16.png" alt="" style="position:relative; top: 15px;" /></div>
    <p class="prod-item__dtl" style="padding-top:8px; margin-left: 12px;">革：スターレ</p>
   </div> <!-- // .prod-item -->
  </div> <!--// .lprod -->

  <h3 id="main-title" style="display: block; margin: 50px 0;">グリーン</h3>
  <div class="lprod ml15"> <!-- .lprod -->
   <div class="prod-item"> <!-- .prod-item -->
    <div class="prod-item__thumb"><img src="images/leather/img17.png" alt=""/></div>
    <p class="prod-item__dtl" style="margin-left: -15px;">革：ラティーゴ</p>
   </div> <!-- // .prod-item -->

   <div class="prod-item"> <!-- .prod-item -->
    <div class="prod-item__thumb" style="position:relative; left:20px; top:5px;"><img src="images/leather/img18.png" alt="" class="ml55" /></div>
    <p class="prod-item__dtl ml30">革：スターレ</p>
   </div> <!-- // .prod-item -->
  </div> <!--// .lprod -->

  <h3 id="main-title" style="display: block; margin: 50px 0;">レッド</h3>
  <div class="lprod ml20"> <!-- .lprod -->
   <div class="prod-item"> <!-- .prod-item -->
    <div class="prod-item__thumb"><img src="images/leather/img19.png" alt=""/></div>
    <p class="prod-item__dtl" style="margin-left: -20px;">革：ラティーゴ</p>
   </div> <!-- // .prod-item -->

   <div class="prod-item"> <!-- .prod-item -->
    <div class="prod-item__thumb"><img src="images/leather/img20.png" alt="" class="ml35" /></div>
    <p class="prod-item__dtl ml15">革：スターレ</p>
   </div> <!-- // .prod-item -->
  </div> <!--// .lprod -->
 </div> <!-- #section-wrapper -->

 <div id="section-wrapper">
  <h3 id="main-title">定番カラー＝キャメルについて</h3>
  <div id="accent-3" style="margin-top: 4px;"></div>

  <div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
   <div class="rlistf-items mt20 mb50"><!-- .rlistf-items -->
    <div class="rlistf-items__img"> <img src="images/leather/img21.png" alt=""/> </div>
    <div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
     <p style="position:relative; top:-4px;">
      HERZの「キャメル」は染色していないままの革（ヌメ革）が日に焼けた時の色を再現して染色して作ってもらっています。<br/>
      ちょうど少し使い込んだような暖かみを感じる色。<br/>
      日々使っていると気に留めないけれどふと鞄を見てみると、色の変化が楽しめる。<br/>
      この色も、その変化も愛着を感じられる理由ひとつなのかもしれません。
     </p>
    </div> <!-- // .rlistf-items__dtl -->
   </div><!-- // .rlistf-items -->

   <div class="rlistf-items" style="margin-top: 33px;"><!-- .rlistf-items -->
    <div class="rlistf-items__img"> <img src="images/leather/img22.png" alt=""/> </div>
    <div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
     <p style="position:relative; top:-7px;">そして、初めはキャメルのみでしたが、ご要望に添って、黒、チョコ、グリーン、赤の順に展開が増えていきました。
      カラフルになると、作り手の思考が手軽に使える革小物へも移り、少しずつラインナップが充実してきたのです。</p>
    </div> <!-- // .rlistf-items__dtl -->
   </div><!-- // .rlistf-items -->
  </div> <!-- # - .rlistf -->
 </div> <!-- #section-wrapper -->

 <span id="section" name="elem5"></span>
 <div id="section-wrapper">
  <h3 id="main-title">タンドーソフトレザーについて</h3>
  <div id="accent-3" style="margin-top: 9px; margin-bottom: 20px;"></div>

  <img src="images/leather/img23.png" alt=""/>

  <h3 id="main-title" style="display: block; margin-top: 56px">極厚で柔らかい、</h3>
  <h3 id="main-title" style="display: block; margin: 20px 0 50px;">ワイルドなシワが魅力のHERZオリジナルレザー</h3>

  <div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
   <div class="rlistf-items mt20"><!-- .rlistf-items -->
    <div class="rlistf-items__img"style="position:relative; top: 3px;"> <img src="images/leather/img24.png" alt=""  />
    </div>
    <div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
     <p style="position:relative; top: -4px;">
      HERZ創業者の近藤が長きにわたり求めた、厚くて野性的な表情を持つ革です。<br/>
      深い無数のシワがあり、極厚ながらもしなやかな柔らかさを持つこの革は、大草原を砂煙をあげながら荒々しく走る野生のバイソンを思わせます。 <br/> <br/>

      思い通りの革が仕上がらず、素材開発には20年以上の歳月がかかりましたが、メッセンジャーバッグ（W-3）の誕生で一気にイメージが固まり、オリジナルレザーとして完成しました
     </p>
    </div> <!-- // .rlistf-items__dtl -->
   </div><!-- // .rlistf-items -->
  </div> <!-- # - .rlistf -->

  <p class="f15" style="margin-top: 48px;">ワイルドさの中にも、あたたかく柔らかな印象のある独特の風合いは、手にした際に他では 得ることができない驚きを実感いただけるはずです。</p>

 </div> <!-- #section-wrapper -->

 <span id="section" name="elem6"></span>
 <div id="section-wrapper">
  <h3 id="main-title">昔ながらの製法で時間をかけてなめされた革</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>

  <div class="mt20">
   <img src="images/leather/img25.png" alt=""/>
   <img src="images/leather/img26.png" alt="" style="margin-left: 13px;" />
  </div>

  <p class="f15 mt15">多くの時間と手間をかけてでも、革本来の風合いを大切にしたい。それが古くからの技法であるタンニンなめしを選んだ理由です。
  </p>

  <div class="section1 mt50">
   <img src="images/leather/img27.png" alt="" style="float: left; margin-top: 7px; padding: 0 15px
   10px 0"/>
   <p>
    植物から抽出したタンニンによって、時間をかけて丹精にゆっくりなめしていく。生産第一の現代社会とまるで逆行した姿勢ですが、この手間が堅牢で味わい深い、良い革をつくりだすのです。
    また、クロームなめし（クローム剤という化合物によるなめし）による革は、燃やすと有害物質が出ますが、タンニンなめしの革はそのまま灰となり土に帰る、環境に優しい手法です。HERZのカバンにどこか愛嬌のある素朴さが感じられるのは、自然に寄り添ったものづくりを心がけているからかもしれません。<strong>自然からの産物である革を、自然によってなめし、染色する。これが40年以上、同じ革を選び続けたHERZのこだわりです。</strong>
   </p>
  </div>
 </div> <!-- #section-wrapper -->

 <span id="section" name="elem7"></span>
 <div id="section-wrapper">
  <h3 id="main-title">厳選された素材</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>

  <p class="f15 mt20">革鞄を丈夫に仕上げる為に、素材にも耐久性のある原皮（なめす前の皮）を選んでいます。 原皮は、成牛の中でも強度と耐久性のある「ステアハイド」と呼ばれるものを使用しています。その中から比較的硬く厚い皮を厳選しています。</p>

  <p class="f15" style="margin-top: 25px;">革鞄を丈夫に仕上げる為に、素材にも耐久性のある原皮（なめす前の皮）を選んでいます。 原皮は、成牛の中でも強度と耐久性のある「ステアハイド」と呼ばれるものを使用しています。その中から比較的硬く厚い皮を厳選しています。</p>

  <p class="f15" style="margin-top: 25px;">
   参考までに・・・<br/>
   原皮は牛の成長にしたがって、種類が分けられています。若い順に「カーフ」「キップ」「カウ」「ステア」の４種類があります。人間と同じように、成長した動物の皮は堅牢で丈夫ですが、若いものは柔らかく皮のキメが細かいという特徴があります。
  </p>
 </div> <!-- #section-wrapper -->



</div> <!-- // #main-wrapper -->