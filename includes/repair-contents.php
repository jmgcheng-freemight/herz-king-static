<div id="main-wrapper">

	<div id="section-wrapper" style="margin-bottom:85px;">
		<div id="accent-3" style="margin-bottom: 11px;"></div>
		<h3 id="main-title" class="main-title-attention">修理について<span class="mini-text-attention" style="padding-left:10px;">REPAIR</span></h3>
		<div id="accent-3" style="margin-top: 11px;"></div>
		<div id="image-wrapper">
			<img src="images/repair/img1.png" alt="" />
		</div>
	</div>
	
	<span id="section" name="elem1"></span>
	<div id="section-wrapper" style="margin-bottom:75px;">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">HERZの鞄はお使いいただく限り、出来るだけの修理・加工をいたします</h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<div id="l-desc" class="rlistf" style="margin-bottom:30px;"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top:20px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/repair/img2.png" alt=""/> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p>
						お使いの鞄を拝見し、その状態にふさわしい方法で丁寧に修理いたします。修理方法が何通りかある場合は、ご希望やご予算によって納得のいく方法をお選び頂いております。<br/><br/>
						ご購入頂いた革製品は、心を込めて出来るだけの修理をさせて頂きます。<br/>
						それがHERZの「こころ」です。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
		</div> <!-- # - .rlistf -->
	</div> <!-- #section-wrapper -->
	
	<span id="section" name="elem2"></span>
	<div id="section-wrapper" style="margin-bottom:85px;">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">カバンの修理例</h3>
		<div id="accent-3" style="margin-top: 8px;"></div>
		<p class="f15" style="margin-top: 13px;">
			鞄の底が破れていたため、補強革を加えて強度が増す様に加工を施しました。
		</p>
		<div id="comparison"> <!-- #comparison -->
			<div class="col2" style="overflow:hidden;"> <!-- .col2 -->
				<div class="col2-items"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/repair/img3.png" alt=""/></div>
					<p class="comparison-dtl" style="text-align:center; font-weight:bold;">
						修理前
					</p>
				</div> <!-- // .col2-items -->
				<div class="col2-items"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/repair/img4.png" alt=""/></div>
					<p class="comparison-dtl" style="text-align:center; font-weight:bold;">
						修理後
					</p>
				</div> <!-- // .col2-items -->
			</div> <!-- // .col2 -->
		</div> <!-- // #comparison -->
	</div> <!-- #section-wrapper -->
	
	<span id="section" name="elem1"></span>
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">修理の受付方法</h3>
		<div id="accent-3" style="margin-top:8px;"></div>
		<p class="f15" style="margin-top: 13px;">
			直営店舗にご来店頂くか、メールにてお問い合わせ頂ければ受付可能です。<br/>
			その際、お見積り・納期等のご案内をさせて頂きます。<br/>
			※修理可否の判断の為、実際に現物の状態を拝見させて頂いてから詳細のご連絡をいたします。<br/>
			※尚、修理は当工房の製品のみ受付可能です。他メーカー様の商品はお断りしております。
		</p>
		<div class="sd-cont09" style="margin-top:40px; margin-bottom:none; padding-bottom:2px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal; margin-bottom:10px;">関連リンク</h4>
			<div class="sd-cont09-items">
				<div class="sd-cont09-items__img"> <img src="images/repair/img5.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:20px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						HERZ店舗一覧
					</span>
				</div>
			</div>
			<div class="sd-cont09-items">
				<div class="sd-cont09-items__img"> <img src="images/repair/img6.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						お問い合わせ
					</span>
				</div>
			</div>
		</div>
	</div> <!-- #section-wrapper -->
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" >修理、加工の一例と費用</h3>
		<div id="accent-3" style="margin-top: 8px;"></div>
		<p class="f15" style="margin-top: 13px;">
			※お使いの鞄のタイプや使用状態によって、修理費用は変わります。
		</p>
		<div id="comparison" style="margin-top:10px;"> <!-- #comparison -->
			<div class="col2" style="overflow:hidden;"> <!-- .col2 -->
				<div class="col2-items" style="height:380px;"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/repair/img7.png" alt=""></div>
					<h4 class="comparison-title">金具交換</h4>
					<p class="comparison-dtl" style="line-height:30px; margin-top:0px;">
						金額: 540円～(税込)<br/>
						負担の大きい箇所は摩耗するので、交換が必要です。
					</p>
				</div> <!-- // .col2-items -->
				<div class="col2-items" style="height:390px;"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/repair/img8.png" alt=""></div>
					<h4 class="comparison-title">根革交換</h4>
					<p class="comparison-dtl" style="line-height:30px; margin-top:0px;">
						金額: 1,080円～(税込)<br/>
						鞄本体のショルダー・リュック結合部の革の交換も可能です。
					</p>
				</div> <!-- // .col2-items -->
				<div class="col2-items" style="height:390px;"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/repair/img9.png" alt=""></div>
					<h4 class="comparison-title">ファスナー交換</h4>
					<p class="comparison-dtl" style="line-height:30px; margin-top:0px;">
						金額: 4,320円～(税込)<br/>
						ファスナー・金具ともに修理は可能です。
					</p>
				</div> <!-- // .col2-items -->
				<div class="col2-items" style="height:390px;"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/repair/img10.png" alt=""></div>
					<h4 class="comparison-title">取っ手交換</h4>
					<p class="comparison-dtl" style="line-height:30px; margin-top:0px;">
						金額: 4,860円～(税込)<br/>
						革の消耗が激しい場合、新しい革で交換します。
					</p>
				</div> <!-- // .col2-items -->
				<div class="col2-items" style="height:390px;"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/repair/img11.png" alt=""></div>
					<h4 class="comparison-title">錠前交換</h4>
					<p class="comparison-dtl" style="line-height:30px; margin-top:0px;">
						金額: 4,320円～(税込)<br/>
						開閉を重ねることで、錠前も交換が必要になることもあります。
					</p>
				</div> <!-- // .col2-items -->
				<div class="col2-items" style="height:390px;"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/repair/img12.png" alt=""></div>
					<h4 class="comparison-title">やぶれ補修</h4>
					<p class="comparison-dtl" style="line-height:30px; margin-top:0px;">
						金額: 5,400円～(税込)<br/>
						角部分の革がやぶれた場合、新たな革を被せて補修します。
					</p>
				</div> <!-- // .col2-items -->
				<div class="col2-items" style="height:390px;"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/repair/img13.png" alt=""></div>
					<h4 class="comparison-title">角部分の剥がれ</h4>
					<p class="comparison-dtl" style="line-height:30px; margin-top:0px;">
						金額: 540円～(税込)<br/>
						糊づけをしている部分は使ってゆくうちに剥がれることがございます。その場合、新たに糊づけする補修が可能です。
					</p>
				</div> <!-- // .col2-items -->
				<div class="col2-items" style="height:390px;"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/repair/img14.png" alt=""></div>
					<h4 class="comparison-title">ホック交換</h4>
					<p class="comparison-dtl" style="line-height:30px; margin-top:0px;">
						金額: 540円～(税込)<br/>
						留め外しを繰り返すと、ホックの頭側にあるバネが外れてしまうことがあります。<br/>
						受け側も交換の場合は1,080円～となります。
					</p>
				</div> <!-- // .col2-items -->
			</div> <!-- // .col2 -->
		</div> <!-- // #comparison -->
		
	 </div>
	
	

</div> <!-- // #main-wrapper -->