<div class="middle-content-container align-left" >
	<div class="mid-row-1">
		<img src="images/sidebar-slice/mid-line-hr.png" alt=""/>
		<h3 class="middle-text-title font-ryumin" style="line-height:30px;">工房について<span class="mini-text">-FACTORY-</span></h3>
		<img src="images/sidebar-slice/mid-line-hr.png" alt="" />
		<div class="middle-slider" style="margin-top:30px;">
			<ul class="bxslider2">
			<li><img src="images/middle/middle-image.png" /></li>
			<li><img src="images/middle/middle-image.png" /></li>
			<li><img src="images/middle/middle-image.png" /></li>
			<li><img src="images/middle/middle-image.png" /></li>
			</ul>
		</div>
	</div>

	<div class="mid-row-02">
		<h3 class="middle-text-title">本当の意味でのメイドインジャパン<span class="mini-text"></span></h3>
		<a href="" target="_blank" ><h3 class="float-right"><img src="images/arrow.png" alt=""></h3></a>
		<img src="images/sidebar-slice/mid-line-hr.png" alt="" />
		<p class="middle-sub-text">HERZ創業者である近藤晃理の、「作りたい！楽しい！」という想いから全てが始まった鞄屋。</br>
		機械的な分業体制によるモノ作りではなく、手間と時間がかかっても、作り手の作りたい気持ちを大事にした鞄作りを創業からずっと続けています。
		</p>
		<div class="mid-row-02-cat">
		<img src="images/middle/middle-img-02.png" class="middle-img2 float-left" alt="">
		<p class="content-img2">一人の作り手が一通り仕上げ、作り手の人格が宿
		るような、血が通っているものを「HERZの鞄」と
		したい。<br><br>
		HERZのカバンのステッチに目をこらしてみて下さ
		い。少し曲がっているかもしれません。<br><br>
		でも、そこからミシンを踏みしめる作り手の息吹
		が感じられるのではないでしょうか。<br>
		革の個性（シワやバラキズ）と同じように、作っ
		た人の顔が見える鞄作りをしています。
		</p>
		</div>
	</div>

	<div class="mid-row-2">
	<h3 class="middle-text-title">鞄作りの工程<span class="mini-text"></span></h3>
	<a href="" target="_blank" ><h3 class="float-right"><img src="images/arrow.png" alt=""></h3></a>
	<img src="images/sidebar-slice/mid-line-hr.png" alt="" />
	<p class="middle-sub-text" style="font-weight: 400;">一枚の革からたくさんの工程を経て、立体的な鞄が仕上がります。<br>
	HERZでは、革の裁断を除く全ての工程を一人の作り手が完成まで手掛けます。<br>
	鞄作りには、大きく分けて5つの工程があります。<br>
	</p>
	<div class="mid-cat-01">
	<h3 class="middle-text-title">1.裁断<span class="mini-text"></span></h3>
	<div class="middle-inner-1">
	<img src="images/middle/middle-img-03.png" class="middle-img3 float-left" alt="">
	<p class="content-img3">
		全ては革の裁断から始まります。<br>
		HERZでは革の裁断専任の作り手が日々製作する全
		てのの裁断をしています。<br>
		<b>
			革の特性や一つ一つ製品の構造を分かっていない
			と出来ない裁断は作り手の中でも経験を積んだ者
			が担当しています。
		</b>
	</p>
	</div>
	<div class="middle-inner-2">
	<img src="images/middle/middle-img-04.png" class="middle-img4 float-left" alt="">
	<p class="content-img4">
	一つのモデルでも型紙は数十パターンあります。<br>
	定番品も多いHERZでは膨大な量の型紙がストック
	されています。
	</p>
	</div>
	</div>
	<div class="mid-cat-02">
	<h3 class="middle-text-title">2.漉き<span class="mini-text"></span></h3>
	<div class="middle-inner2-1">
	<img src="images/middle/middle-img-05.png" class="middle-img3 float-left" alt="">
	<p class="content-img-cat-02">
	盤面やマチなど革が重なり合う箇所は端部分を漉
	きます。<br>
	革の漉きは最も難しい工程の一つです。<br>
	薄めに漉けば後の工程である縫製や成形も楽です
	が、漉きすぎると丈夫でなくなります。<b>HERZで
	は丈夫さを第一に考えたモノ作りをしているので
	、漉きも出来るだけ厚い革のままで。このさじ加
	減が非常に難しいのです。</b><br>
	</p>
	</div>
	<div class="middle-inner2-2">
	<img src="images/middle/middle-img-06.png" class="middle-img4 float-left" alt="">
	<p class="content-img4"><b>
	ミシンや漉き機など使用する機材のメンテネンス
	も作り手自身が毎日行っています。</b>
	</p>
	</div>
	</div>
	<div class="mid-cat-03">
	<h3 class="middle-text-title">3.磨き<span class="mini-text"></span></h3>
	<div class="middle-inner3-1">
	<img src="images/middle/middle-img-07.png" class="middle-img3 float-left" alt="">
	<p class="content-img3">
	<b>革の切り口（コバ面）に染料を入れて磨く工程は
	全て手作業です。</b>製作机の端に革をあわせて、ぎ
	ゅっと力を込めて磨きます。
	</p>
	</div>
	<div class="middle-inner3-2">
	<img src="images/middle/middle-img-08.png" class="middle-img4 float-left" alt="">
	<p class="content-img4">HERZの鞄から自然で優しい感じを受けると言われ
	ることがあります。<br>
	手作業で革の切り口を磨いているからかもしれま
	せん。ある時は指先で優しく。<br>
	ある時は腕全体で力強く。革の表面にまで磨きが
	入ること。<br>
	これは手作業でなくてはできないことなのです。<br>
	</p>
	</div>
	</div>
	<div class="mid-cat-04">
	<h3 class="middle-text-title">4.縫製<span class="mini-text"></span></h3>
	<div class="middle-inner4-1">
		<img src="images/middle/middle-img-09.png" class="middle-img3 float-left" alt="" style="margin-right:18px;" />
		<img src="images/middle/middle-img-10.png" class="middle-img4 float-left" alt="" style="margin-right:0px;" />
	</div>
	<div class="content-row4-img">
	<p>鞄作りの醍醐味でもあるミシンでの縫製工程。<b>作る鞄や小物によって、ミシンも使い分けています。</b>
	太い針と太い糸で力強くステッチを刻む　HERZの顔とも呼べます。
	</p>
	</div>
	</div>
	<div class="mid-cat-05">
	<h3 class="middle-text-title">5.成型<span class="mini-text"></span></h3>
	<div class="middle-inner5-1">
	<img src="images/middle/middle-img-11.png" class="middle-img3 float-left" alt="">
	<p class="content-img5">
	内縫いの鞄や小物は縫製した後、鞄を表面にひっく
	り返す工程があります。<br>
	一見、簡単そうに見えますが、厚い革で作っている
	ので、ひっくり返すのにも技術が必要です。その後
	、全体的に形を整えれば完成です。<br>
	</p>
	</div>
	<div class="middle-inner5-2">
	<img src="images/middle/middle-img-12.png" class="middle-img4 float-left" alt="">
	<p class="content-img5">
	他にもたくさんの工程がありますが、こうして<br>
	HERZの鞄は作られます。<br>
	<b>一人の作り手が完成まで手掛けるので、同じ商品
	でも磨きの入り具合やステッチのピッチなど一点
	一点異なります。</b>その鞄の個性をお楽しみくださ<br>
	い。<br><br><br>
	</p>
	</div>
	<p class="img5bot-text float-left">※完成品</p>
	</div>
	</div> <!-- end row 2 -->





	<div class="mid-row-3">
	<h3 class="middle-text-title">HERZの道具<span class="mini-text"></span></h3>
	<img src="images/sidebar-slice/mid-line-hr.png" alt="" /><p class="middle-sub-text-row-3">
	クラフトで一般的な用途で使われている道具たちは、HERZの鞄を作るために使うとだいたい耐えられ
	ません。厚い革と太い糸で武骨に仕上げるHERZの鞄作りを実現するには、通常とは一線を画した、あ
	る意味、強引な道具の使い方をする時も間々あります。<br><br>その作業に「普通は」使わないアイテムから骨董市や金物屋さんで見つけた掘り出し物、ホームセンタ
	ーでよくある工具、はたまた自分で加工して使いやすくした逸品まで作り手の「手」となる工具を一部
	ご紹介します。<br>
	</p><div class="row3-cat-01">
	<h3 class="middle-text-title">ハンマー<span class="mini-text"></span></h3>
	<div class="row3-middle-inner1-1">
	<img src="images/middle/row2-middle-img1.png" class="middle-img float-left" alt="">
	<p class="content-row3">
	カシメやホックを留めたり、革パーツどうしの糊
	づけを定着させたり。特に金具を留める際のハン
	マーは、HERZの分厚い革を挟んで打つにはかなり
	の力が必要です。<br>
	工程によって、小さいものやプラスティックのも
	のなど使い分けています。<br>
	</p>
	</div>
	</div>
	<div class="row3-cat-02">
	<h3 class="middle-text-title">ポンチ、楕円、R取り<span class="mini-text"></span></h3>
	<div class="row3-middle-inner2-1">
		<img src="images/middle/row2-middle-img2.png" class="middle-img float-left" alt="" style="margin-right:18px;">
		<img src="images/middle/row2-middle-img3.png" class="middle-img float-left" alt="" style="margin-right:0px;">
	</div>
	<div class="row3-content-img-cat02"><p>穴を開けたり、革の端をカットしたりする時に使います。大きさと形状によって、十数種類あり、仕上
	がった時の見た目を左右する重要なパーツです。</p></div>
	</div>
	<div class="row3-cat-03">
	<h3 class="middle-text-title">トウフ<span class="mini-text"></span></h3>
	<div class="row3-middle-inner3-1">
	<img src="images/middle/row2-middle-img4.png" class="middle-img float-left" alt="">
	<p class="content-row3">
	金具を留めるために下に当てる金属製の台。直方
	体が多いことからいつの間にかその名前に。まさ
	に縁の下の力持ちといったアイテム。<br>
	</p>
	</div>
	</div>
	<div class="row3-cat-04">
	<h3 class="middle-text-title">打ち棒<span class="mini-text"></span></h3>
	<div class="row3-middle-inner4-1">
	<img src="images/middle/row2-middle-img5.png" class="middle-img float-left" alt="">
	<p class="content-row3">
	カシメやホックを留める際に、金具に当てて打つ
	ためのもの。長年打ち込むと、上部が変形し、き
	のこような形になります。<br>
	</p>
	</div>
	</div>
	<div class="row3-cat-05">
	<h3 class="middle-text-title">罫書き、デバイダー<span class="mini-text"></span></h3>
	<div class="row3-middle-inner5-1">
	<img src="images/middle/row2-middle-img6.png" class="middle-img float-left" alt="" style="margin-right:18px;" />
	<img src="images/middle/row2-middle-img7.png" class="middle-img float-left" alt="" style="margin-right:0px;" />
	</div>
	<br><br>
	<div class="row3-content-img-cat05"><p>革を裁断したり、金具を取り付けるための印をつける道具。デバイダーは、平行線を引く時に使うけが
	き。骨董市で調達する作り手も多いです。<br>
	</p></div>
	</div>
	<div class="row3-cat-06">
	<h3 class="middle-text-title">ピーラー、鉋<span class="mini-text"></span></h3>
	<div class="row3-middle-inner6-1">
	<img src="images/middle/row2-middle-img8.png" class="middle-img float-left" alt="">
	<p class="content-row3">
	革の縁をななめに、時に垂直に削る時の道具。な
	なめの削りは特にウォッシュ加工など、ナチュラルな雰囲気の鞄やカジュア
	ル感のある鞄によく使います。<br>
	</p>
	</div>
	</div>
	<div class="row3-cat-07">
	<h3 class="middle-text-title">やっとこ、ペンチ<span class="mini-text"></span></h3>
	<div class="row3-middle-inner7-1">
	<img src="images/middle/row2-middle-img9.png" class="middle-img float-left" alt="">
	<p class="content-row3">
	革パーツを糊付けして貼りあわせる時などに使用
	します。革が傷つかないよう、先端を丸く削ったり革を当てた
	り各々で加工しています。<br>
	</p>
	</div>
	</div>
	<div class="row3-cat-08">
	<h3 class="middle-text-title">ガリガリ、糊ケース<span class="mini-text"></span></h3><div class="row3-middle-inner8-1">
	<img src="images/middle/row2-middle-img10.png" class="middle-img float-left" alt="">
	<p class="content-row3">
	糊をつけやすくするために革の床面を「ガリガリ
	」加工する道具。金属やすりだったり、金属ブラ
	スだったり、謎の骨董品だったり。<br/>
	革を貼りあわせる糊の専用ケースは、日本では珍しいようです。
	</p>
	</div>
	</div>
	</div>
	
	<div class="mid-row-4">
	<h3 class="middle-text-title">博多工房について<span class="mini-text"></span></h3>
	<img src="images/sidebar-slice/mid-line-hr.png" alt="" />
	<div class="mid-row4-cat1">
	<img src="images/middle/row4-middle-img1.png" alt=""/>
	</div>
	<div class="mid-row4-cat2">
	<img class="img-pad" src="images/middle/row4-middle-img2.png" alt="" style="padding-right:44px;" />
	<img class="img-pad" src="images/middle/row4-middle-img3.png" alt="" style="padding-right:44px;" />
	<img class="img-pad" src="images/middle/row4-middle-img4.png" alt="" style="margin-right:0px; padding-right:0px;"/>
	</div>
	<div class="row4-content-text">
	<p>
	感度の高い場所に工房を構えることで、HERZらしさを磨いていく。<br><br>
	HERZ(ヘルツ)は、博多にも工房を構えています。<br>
	博多にあって、渋谷にないもの。<br>
	それは都会の便利さと自然の豊かさが混在していることです。感度の高い街々に工房を構えることで、
	色々なモノ・コトを吸収し、HERZらしさを磨いていく。変わらない心でモノ作りをするために必要な
	ことだと思っています。<br>
	</p>
	<div class="table-row4">
	<div class="factory-table">
	<div class="factory-table-row">
	<div class="factory-table-cell table-cell-left">住所</div>
	<div class="factory-table-cell table-cell-right">福岡県福岡市博多区博多駅前3-16-10</div>
	</div>
	<div class="factory-table-row">
	<div class="factory-table-cell table-cell-left">定休日</div>
	<div class="factory-table-cell table-cell-right">日曜・祭日</div>
	</div>
	<div class="factory-table-row">
	<div class="factory-table-cell table-cell-left">営業時間</div>
	<div class="factory-table-cell table-cell-right">平日 11:00-19:00<br><br>土曜 11:00-19:00<br><br><b>商品の販売・修理の受付等は対応しておりません。</b><br/><br>時間はその日の製作時間によって変わります。</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	
</div>