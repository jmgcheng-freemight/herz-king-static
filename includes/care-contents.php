<div id="main-wrapper">

	<div id="section-wrapper">
		<div id="accent-3" style="margin-bottom: 11px;"></div>
		<h3 id="main-title" class="font-ryumin">お手入れ方法<span class="mini-text">HOW TO CARE</span></h3>
		<div id="accent-3" style="margin-top: 11px;"></div>
		<div id="image-wrapper">
			<img src="images/care/img1.png" alt="" />
		</div>
	</div>

	<span id="section" name="elem1"></span>
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">革は乾燥すると弱くなります。そのままにしておくとひび割れ・やぶれなどに発展します</h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<p class="f15" style="margin-top: 13px;">
			HERZで通常使用しているラティーゴ・スターレと呼んでいるオリジナルレザーは、製品の特性上、適度な革の張り、硬さを持たす為、革をなめす際に含ませるオイル、グリースの量を調整して仕上げを行っています。
			<br/>
			このため、製品をご使用いただく上で革が乾燥してくることがあります。
		</p>
	</div> <!-- #section-wrapper -->
	
	<span id="section" name="elem1"></span>
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">オイルメンテナンス</h3>
		<div id="accent-3" style="margin-top: 9px;"></div>
		<p class="f15" style="margin-top: 13px;">
			まず製品をご使用いただく前に、革の表面全体に保革用のオイル（ラナパー、ミンクオイル等）を塗っていただくことをお勧めします。
			<br/>
			また、ご使用いただく中で表面が乾燥した際にも保革用オイルをご使用下さい。オイルを塗る度に、色が濃くなり表面にツヤが出てくる革独特の変化をお楽しみいただけます。
		</p>
		<div id="l-desc" class="rlistf" style="margin-bottom:30px;"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top: 30px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/care/img2.png" alt=""/> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<h3 class="font-ryumin" style="color:#5c2700; font-size:30px; line-height:30px; margin-bottom:30px;">
						1.ブラッシング、乾拭き
					</h3>
					<p>
						使用後の鞄をブラッシングまたは乾拭きして、鞄に付いた ホコリや汚れを落とします
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/care/img3.png" alt=""/> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<h3 class="font-ryumin" style="color:#5c2700; font-size:30px; line-height:30px; margin-bottom:30px;">
						2.オイル塗布
					</h3>
					<p>
						ごく少量を柔らかい布など（ラナバーの場合は付属のスポンジ）にとり、 薄く全体に均等に延ばすように塗ります。 初めてオイルを塗る方は、目立たない底部などから塗って頂くと良いです。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/care/img4.png" alt=""/> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<h3 class="font-ryumin" style="color:#5c2700; font-size:30px; line-height:30px; margin-bottom:30px;">
						3.鞄に油分を浸透させる
					</h3>
					<p>
						オイル塗布直後は鞄が多少べたついています。<br/>
						油分が浸透するまで鞄をおいておきましょう。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
		</div> <!-- # - .rlistf -->

		<span style="display:block; margin-bottom:40px;"></span>
		<h3 class="font-h3-1 font-color-darkred">
			オイル塗布の頻度について
		</h3>
		<p class="font-color-darkred font-p-1" >
			革鞄は約2か月に1回程度、オイルを塗り込んで下さい。 比較的、手に触れる機会の多い革小物はバッグよりも塗布する頻度は少なくても結構です。
		</p>
		<h3 class="font-h3-1 font-color-darkred">
			イタリアンレザーにはオイル塗布は不要です
		</h3>
		<p class="font-color-darkred font-p-1" style="margin-bottom:4px;">
			一部製品（IL、Gではじまる品番の付いた製品）に使用しているイタリアンレザーは充分にオイルを含ませて仕上げていますので、 オイルメンテナンスは必要ありません。布で乾拭きしていただくだけで綺麗なツヤが出てきます。
		</p>
		<a href="" class="font-ryumin link" style="font-weight:normal; font-size:16px;"> <img src="images/aging/arrow-right.png" alt="">Organイタリアンレザーについて</a>
		
		<div class="sd-cont09" style="margin-top:40px; margin-bottom:none; padding-bottom:2px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal; margin-bottom:10px;">関連リンク</h4>
			<div class="sd-cont09-items">
				<div class="sd-cont09-items__img"> <img src="images/care/img5.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:20px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						作り手が教えるメンテナンス
					</span>
				</div>
			</div>
			<div class="sd-cont09-items">
				<div class="sd-cont09-items__img"> <img src="images/care/img6.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						メンテナンス用品
					</span>
				</div>
			</div>
		</div>
	</div> <!-- #section-wrapper -->
	
	<span id="section" name="elem1"></span>
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">革が水に濡れてしまった場合</h3>
		<div id="accent-3" style="margin-top: 11px;"></div>
		<p class="f15" style="margin-top: 13px;">
			水に濡れてしまった場合は浸み込む前に対応すると効果があります。<br/>
			雨の日のご使用などで革が濡れてしまった場合、乾いた後に水染みが残る場合があります。<br/>
			水染みを防ぐには、革全体を優しく水拭きした後、自然乾燥させて下さい。<br/>
			早く乾かそうとして、ドライヤーなどの高温に近づけるのは避けてください。<br/>
			ひび割れや痛みの原因になりますので、充分ご注意ください。<br/>
		</p>
		<div id="l-desc" class="rlistf" style="margin-bottom:30px;"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top: 35px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/care/img7.png" alt=""/> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<h3 class="font-ryumin" style="color:#5c2700; font-size:30px; line-height:30px; margin-bottom:25px;">
						1.水染みがついた時
					</h3>
					<p>
						このまま放置すると、乾いた後に染みが残ります。 乾いた後の染みを取り除くのは難しいので、 水染みが付いているうちに対応してください。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/care/img8.png" alt=""/> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<h3 class="font-ryumin" style="color:#5c2700; font-size:30px; line-height:30px; margin-bottom:55px;">
						2.水拭きで馴染ませる
					</h3>
					<p>
						水で絞った布巾を使い、全体に水染みと同じぐらい 馴染ませるように水拭きするとシミとして残りにくくなります。 
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/care/img9.png" alt=""/> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<h3 class="font-ryumin" style="color:#5c2700; font-size:30px; line-height:30px; margin-bottom:30px;">
						3.革を乾かす
					</h3>
					<p>
						水拭き後、乾いたタオルでたたくようにして水気を除き、 風通しの良い場所で陰干しして革を乾かします。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
		</div> <!-- # - .rlistf -->

		<p class="font-color-darkred font-p-1" >
			革が濡れた状態で強い力で擦ったり、過度な力を加えると、色ムラ、変形の原因となります。 また、濡れた革は油分が不足することがありますので、充分に乾燥した後、保革用オイルを塗って下さい。 水拭きでも落ちない染みについては落とすことができません
		</p>
		
		<!-- 
		<div class="sd-cont09" style="margin-top:40px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal">関連リンク</h4>
			<div class="sd-cont09-items" style="width:100%;">
				<div class="sd-cont09-items__img"> <img src="images/care/img10.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:20px; width:530px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						雨に濡れてしまった！気になる雨染みどうなるの？
					</span>
				</div>
			</div>
		</div>
		-->
		<div class="sd-cont09" style="margin-top:35px; margin-bottom:none; padding-bottom:2px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal; margin-bottom:10px;">関連リンク</h4>
			<div class="sd-cont09-items" style="width:83%;">
				<div class="sd-cont09-items__img"> <img src="images/care/img10.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px; width:420px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							雨に濡れてしまった！気になる雨染みどうなるの？
						</a>
					</span>
				</div>
			</div>
		</div>
	</div> <!-- #section-wrapper -->

	<span id="section" name="elem2"></span>
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">キズ、ヨゴレがついた場合</h3>
		<div id="accent-3" style="margin-top: 11px;"></div>
		<p class="f15" style="margin-top: 13px;">
			表面上に付着した汚れであれば、市販の消しゴムで軽く擦ることによって汚れが落ちます。<br/>
			表面上のかすかなキズであれば、水拭きをすることで目立たなくなります。<br/>
			革の中まで染み込んだ汚れ、深く付いてしまったキズについては、消すことができません。<br/>
			キズ、汚れどちらの場合も完全に消すことができないのが、革のひとつの特徴です。<br/>
			目立たなくするという考えで扱って頂ければ、製品を長くご愛用いただけると考えております。
		</p>
		<div id="comparison" style="margin-top:30px;"> <!-- #comparison -->
			<div class="col2" style="overflow:hidden;"> <!-- .col2 -->
				<div class="col2-items"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/care/img11.png" alt=""/></div>
					<p class="comparison-dtl" style="margin-top:0px; line-height:28px;">強くこすると、色が抜けてしまう事もあるので、軽くこするようにして下さい。</p>
				</div> <!-- // .col2-items -->
				<div class="col2-items"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/care/img12.png" alt=""/></div>
					<p class="comparison-dtl" style="margin-top:0px; line-height:28px;">水染みの時と同じ要領で、該当箇所を拭いた後、自然乾燥させて下さい。</p>
				</div> <!-- // .col2-items -->
			</div> <!-- // .col2 -->
		</div> <!-- // #comparison -->
		<div class="sd-cont09" style="margin-top:35px; margin-bottom:none; padding-bottom:2px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal; margin-bottom:10px;">関連リンク</h4>
			<div class="sd-cont09-items" style="width:83%;">
				<div class="sd-cont09-items__img"> <img src="images/care/img13.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px; width:420px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							バッグにシミがついてしまった時の対応方法
						</a>
					</span>
				</div>
			</div>
		</div>
	</div> <!-- #section-wrapper -->
	
	<span id="section" name="elem2"></span>
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">色落ち、カビについて</h3>
		<div id="accent-3" style="margin-top: 6px;"></div>
		<p class="f15" style="margin-top: 13px;">
			より自然のままで革の仕上げを行う為、染色もまた昔ながらの天然素材による手法を用いています。<br/>
			その為、摩擦や水濡れ等によって革からの色移りが起こります。<br/>
			淡い色の衣服でご使用いただく際には、色移りにご注意ください。<br/>
			カビは一度発生すると除去が困難なので、ご使用後は型崩れを防ぐために新聞紙などの詰め物をしていただき、湿気の少ない風通しの良い場所での保管をお勧め致します。
		</p>
		<div id="comparison" style="margin-top:30px;"> <!-- #comparison -->
			<div class="col2" style="overflow:hidden;"> <!-- .col2 -->
				<div class="col2-items"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/care/img14.png" alt=""/></div>
					<p class="comparison-dtl" style="margin-top:0px; line-height:28px;">淡い色の衣服は、特に色移りが起こりやすいのでご注意下さい。</p>
				</div> <!-- // .col2-items -->
				<div class="col2-items"> <!-- .col2-items -->
					<div class="comparison-thumb"><img src="images/care/img15.png" alt=""/></div>
					<p class="comparison-dtl" style="margin-top:0px; line-height:28px;">鞄を使用しない時は中に詰め物を入れて、風通しのよい場所に保管して下さい。</p>
				</div> <!-- // .col2-items -->
			</div> <!-- // .col2 -->
		</div> <!-- // #comparison -->
	</div> <!-- #section-wrapper -->
	

</div> <!-- // #main-wrapper -->