<div id="main-wrapper">

	<div id="section-wrapper" style="margin-bottom:90px;">
		<div id="accent-3" style="margin-bottom: 11px;"></div>
		<h3 id="main-title" class="main-title-attention">ミネルバボックスについて<span class="mini-text-attention" style="padding-left:10px;">-ABOUT MINERVA BOX-</span></h3>
		<div id="accent-3" style="margin-top: 11px;"></div>
		<div id="image-wrapper">
			<img src="images/minerva-box/img1.png" alt="" />
		</div>
	</div>
	
	<span id="section" name="elem1"></span>
	<div id="section-wrapper" style="margin-bottom:80px;">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">ミネルバボックス(Minerva Box)について</h3>
		<div id="accent-3" style="margin-top: 8px;"></div>
		<p class="f15" style="margin-top: 13px;">
			ミネルバボックスはイタリアのタンナー「バダラッシィ・カルロ社」で一枚一枚丁寧な手仕事によって、作られています。フィレンツェのサンタクローチェ地区で10世紀以上の歴史を持つ”バケッタ製法”と呼<br/>
			ばれる、手鞣し・手染めで仕上げた高級素材の革です。<br/>
			革質はソフトレザータイプで、HERZオリジナルレザーの「スターレ」に近い革になります。
		</p>
		<p class="f15" style="margin-top: 30px;">
			バタラッシィ・カルロ社が作る革で、ミネルバボックスとは反対に表面の皺が少ないスムースレザータイプの「ミネルバリスシオ」というイタリアンレザーもHERZでは使用しています。
		</p>
		
		<div class="sd-cont09" style="margin-top:30px; margin-bottom:none; padding-bottom:2px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal; margin-bottom:10px;">関連リンク</h4>
			<div class="sd-cont09-items" >
				<div class="sd-cont09-items__img"> <img src="images/minerva-box/img2.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							ミネルバリスシオ
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items" >
				<div class="sd-cont09-items__img"> <img src="images/minerva-box/img3.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							HERZオリジナルレザー
						</a>
					</span>
				</div>
			</div>
		</div>
	</div> <!-- #section-wrapper -->
	
	<div id="section-wrapper" style="margin-bottom:70px;">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">特徴的なシュリンク（シボを出す）加工</h3>
		<div id="accent-3" style="margin-top:8px;"></div>
		<div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top:20px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/minerva-box/img4.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
				<p>
					ミネルバボックスという革の特徴は、まず自然本来の風合い豊かな表情であること。<br/>
					シュリンク加工と呼ばれる、表面にシボを出すシワ加工が特徴的な革です。<br/>
					部位による革質差により、加工をした後でもそれぞれシボの入り具合が異なります。<br/>
					シボが強く入っている箇所もあれば、比較的少ない箇所もあります。<br/>
					そのため、同じ鞄でも箇所によってはシボの入り具合が全く異なります。<br/>
					その一つひとつの表情・個性を楽しんで下さい。
				</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
		</div> <!-- # - .rlistf -->
	</div>
	
	<div id="section-wrapper" style="margin-bottom:90px;">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">楽しめる革の経年変化（エイジング）</h3>
		<div id="accent-3" style="margin-top:8px;"></div>
		<div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/minerva-box/img5.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
				<p>
					植物タンニンで鞣した牛革に牛脚油（すね骨や無蹄足を煮沸して採取した100%ピュアなオイル）でたっぷりと時間をかけて加脂する製法です。<br/>
					加工に時間がかかり脂が浸透しにくい反面、使い込んだ時に独特の色艶がでることと、一旦加脂したオイルが抜けにくいという特徴があります。
				</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
		</div> <!-- # - .rlistf -->
		
		<div class="sd-cont09" style="margin-top:40px; margin-bottom:none; padding-bottom:2px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal; margin-bottom:10px;">関連リンク</h4>
			<div class="sd-cont09-items" >
				<div class="sd-cont09-items__img"> <img src="images/minerva-box/img6.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							経年変化について
						</a>
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<span id="section" name="elem1"></span>
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">ちょっとしたキズが付いた時には・・・</h3>
		<div id="accent-3" style="margin-top: 8px;"></div>
		<p class="f15" style="margin-top:30px;">
			自然な風合いを生かすため、表面に大きな加工を施しておりません。<br/>
			そのため、革の表面の小さなキズやシワが隠れにくく、爪傷が付きやすいという特徴がありますが、オイルを多く含んでいるので、指で軽く揉み込んであげるとキズが目立ちにくくなります。<br/>
			使い始めの内はオイル等による保革は必要ありません。
		</p>
		<img src="images/minerva-box/img7.png" alt="" style="display: block; margin-top: 35px;">
		<div class="sd-cont09" style="margin-top:40px; margin-bottom:none; padding-bottom:2px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal; margin-bottom:10px;">関連リンク</h4>
			<div class="sd-cont09-items" style="margin-bottom:30px;">
				<div class="sd-cont09-items__img"> <img src="images/minerva-box/img8.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							ミネルバボックス
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items" style="margin-bottom:30px;">
				<div class="sd-cont09-items__img"> <img src="images/minerva-box/img9.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							ミネルバリスシオ
						</a>
					</span>
				</div>
			</div>
						<div class="sd-cont09-items" >
				<div class="sd-cont09-items__img"> <img src="images/minerva-box/img10.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							リバースレザー
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items" >
				<div class="sd-cont09-items__img"> <img src="images/minerva-box/img11.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							ユーフラテ
						</a>
					</span>
				</div>
			</div>
		</div>
	</div> <!-- #section-wrapper -->
	
</div> <!-- // #main-wrapper -->