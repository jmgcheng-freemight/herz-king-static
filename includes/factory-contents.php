<div id="main-wrapper">

	<div id="section-wrapper" class="mb80">
		<div id="accent-3" style="margin-bottom: 15px;"></div>
		<h3 id="main-title" class="main-title-attention">工房について<span class="mini-text-attention" style="padding-left:10px;">-FACTORY-</span></h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<div class="middle-slider" style="margin-top:30px;">
			<ul class="bxslider2">
				<li><img src="images/middle/middle-image.png" /></li>
				<li><img src="images/middle/middle-image.png" /></li>
				<li><img src="images/middle/middle-image.png" /></li>
				<li><img src="images/middle/middle-image.png" /></li>
			</ul>
		</div>
	</div>
	
	<div id="section-wrapper" class="mb80">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">本当の意味でのメイドインジャパン</h3>
		<div id="accent-3" class="mt5"></div>
		<p class="f15 font-weight-bold" style="margin-top: 10px;">
			HERZ創業者である近藤晃理の、「作りたい！楽しい！」という想いから全てが始まった鞄屋。機械的な分業体制によるモノ作りではなく、手間と時間がかかっても、作り手の作りたい気持ちを大事にした鞄作りを創業からずっと続けています。
		</p>
		<div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/middle/middle-img-02.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
				<p class="mb35">
					一人の作り手が一通り仕上げ、作り手の人格が宿るような、血が通っているものを「HERZの鞄」としたい。
				</p>
				<p class="mb35">
					HERZのカバンのステッチに目をこらしてみて下さい。少し曲がっているかもしれません。
				</p>
				<p>
					でも、そこからミシンを踏みしめる作り手の息吹が感じられるのではないでしょうか。<br/>
					革の個性（シワやバラキズ）と同じように、作った人の顔が見える鞄作りをしています。
				</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			
		</div> <!-- # - .rlistf -->
	</div>
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">鞄作りの工程</h3>
		<div id="accent-3" class="mt5"></div>
		<p class="f15 p-adj mt15 mb40" >
			一枚の革からたくさんの工程を経て、立体的な鞄が仕上がります。<br/>
			HERZでは、革の裁断を除く全ての工程を一人の作り手が完成まで手掛けます。<br/>
			鞄作りには、大きく分けて5つの工程があります。
		</p>
		<div id="l-desc" class="rlistf" style="margin-bottom:30px;"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<h3 class="font-ryumin mb20" style="color:#5c2700; font-size:30px;">
					1.裁断
				</h3>
				<div class="rlistf-items__img"> <img src="images/middle/middle-img-03.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						全ては革の裁断から始まります。 <br/>
						HERZでは革の裁断専任の作り手が日々製作する全てのの裁断をしています。 <br/>
						<span class="font-weight-bold">革の特性や一つ一つ製品の構造を分かっていないと出来ない裁断は作り手の中でも経験を積んだ者が担当しています。</span>
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mt55" style=""><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/middle/middle-img-04.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						一つのモデルでも型紙は数十パターンあります。定番品も多いHERZでは膨大な量の型紙がストックされています。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mt55" ><!-- .rlistf-items -->
				<h3 class="font-ryumin mb30" style="color:#5c2700; font-size:30px;">
					2.漉き
				</h3>
				<div class="rlistf-items__img"> <img src="images/middle/middle-img-05.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						盤面やマチなど革が重なり合う箇所は端部分を漉きます。<br/>
						革の漉きは最も難しい工程の一つです。<br/>
						薄めに漉けば後の工程である縫製や成形も楽ですが、漉きすぎると丈夫でなくなります。<span class="font-weight-bold">HERZでは丈夫さを第一に考えたモノ作りをしているので、漉きも出来るだけ厚い革のままで。このさじ加減が非常に難しいのです。</span>

					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/middle/middle-img-06.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						ミシンや漉き機など使用する機材のメンテネンスも作り手自身が毎日行っています。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mt55" ><!-- .rlistf-items -->
				<h3 class="font-ryumin mb30" style="color:#5c2700; font-size:30px;">
					3.磨き
				</h3>
				<div class="rlistf-items__img"> <img src="images/middle/middle-img-07.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						<span class="font-weight-bold">革の切り口（コバ面）に染料を入れて磨く工程は全て手作業です。</span> 製作机の端に革をあわせて、ぎゅっと力を込めて磨きます。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/middle/middle-img-08.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						HERZの鞄から自然で優しい感じを受けると言われることがあります。<br/>
						手作業で革の切り口を磨いているからかもしれません。ある時は指先で優しく。<br/>
						ある時は腕全体で力強く。革の表面にまで磨きが入ること。<br/>
						これは手作業でなくてはできないことなのです。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mt55" ><!-- .rlistf-items -->
				<h3 class="font-ryumin mb20" style="color:#5c2700; font-size:30px;">
					4.縫製
				</h3>
				<div id="comparison"> <!-- #comparison -->
					<div class="col2" style="overflow:hidden;"> <!-- .col2 -->
						<div class="col2-items"> <!-- .col2-items -->
							<div class="comparison-thumb"><img src="images/middle/middle-img-09.png" alt=""></div>
						</div> <!-- // .col2-items -->
						<div class="col2-items"> <!-- .col2-items -->
							<div class="comparison-thumb"><img src="images/middle/middle-img-10.png" alt=""></div>
						</div> <!-- // .col2-items -->
					</div> <!-- // .col2 -->
				</div>
				<p class="p-adj">
					鞄作りの醍醐味でもあるミシンでの縫製工程。 <span class="font-weight-bold">作る鞄や小物によって、ミシンも使い分けています。</span><br/>
					太い針と太い糸で力強くステッチを刻む　HERZの顔とも呼べます。
				</p>
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mt55" ><!-- .rlistf-items -->
				<h3 class="font-ryumin mb30" style="color:#5c2700; font-size:30px;">
					5.成型
				</h3>
				<div class="rlistf-items__img"> <img src="images/middle/middle-img-11.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						内縫いの鞄や小物は縫製した後、鞄を表面にひっくり返す工程があります。<br/>
						一見、簡単そうに見えますが、厚い革で作っているので、ひっくり返すのにも技術が必要です。その後、全体的に形を整えれば完成です。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> 
					<img src="images/middle/middle-img-12.png" alt=""> 
					<p class="p-adj mt5">
						※完成品
					</p>
				</div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						他にもたくさんの工程がありますが、こうしてHERZの鞄は作られます。<br/>
						<span class="font-weight-bold">一人の作り手が完成まで手掛けるので、同じ商品でも磨きの入り具合やステッチのピッチなど一点一点異なります。</span>その鞄の個性をお楽しみください。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
		</div> <!-- # - .rlistf -->
	</div>
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">HERZの道具</h3>
		<div id="accent-3" style="margin-top:8px;"></div>
		<p class="f15 p-adj" style="margin-top: 13px;">
			クラフトで一般的な用途で使われている道具たちは、HERZの鞄を作るために使うとだいたい耐えられません。厚い革と太い糸で武骨に仕上げるHERZの鞄作りを実現するには、通常とは一線を画した、ある意味、強引な道具の使い方をする時も間々あります。
		</p>
		<p class="f15 p-adj" style="margin-top: 13px;">
			その作業に「普通は」使わないアイテムから骨董市や金物屋さんで見つけた掘り出し物、ホームセンターでよくある工具、はたまた自分で加工して使いやすくした逸品まで作り手の「手」となる工具を一部ご紹介します。
		</p>
		<div id="l-desc" class="rlistf mb30""> <!-- # - .rlistf -->
			<div class="rlistf-items mb50 mt40"><!-- .rlistf-items -->
				<h3 class="font-ryumin mb30" style="color:#5c2700; font-size:30px; line-height:30px;">
					ハンマー
				</h3>
				<div class="rlistf-items__img"> <img src="images/middle/row2-middle-img1.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						カシメやホックを留めたり、革パーツどうしの糊づけを定着させたり。特に金具を留める際のハンマーは、HERZの分厚い革を挟んで打つにはかなりの力が必要です。<br/>
						工程によって、小さいものやプラスティックのものなど使い分けています。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mb50 mt40"><!-- .rlistf-items -->
				<h3 class="font-ryumin mb30" style="color:#5c2700; font-size:30px; line-height:30px;">
					ポンチ、楕円、R取り
				</h3>
				<div id="comparison"> <!-- #comparison -->
					<div class="col2" style="overflow:hidden;"> <!-- .col2 -->
						<div class="col2-items"> <!-- .col2-items -->
							<div class="comparison-thumb"><img src="images/middle/row2-middle-img2.png" alt=""></div>
						</div> <!-- // .col2-items -->
						<div class="col2-items"> <!-- .col2-items -->
							<div class="comparison-thumb"><img src="images/middle/row2-middle-img3.png" alt=""></div>
						</div> <!-- // .col2-items -->
					</div> <!-- // .col2 -->
				</div>
				<p class="" style="color:#5C2700; line-height: 30px; font-size: 15px;">
					穴を開けたり、革の端をカットしたりする時に使います。大きさと形状によって、十数種類あり、仕上がった時の見た目を左右する重要なパーツです。
				</p>
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mb50 mt40"><!-- .rlistf-items -->
				<h3 class="font-ryumin mb30" style="color:#5c2700; font-size:30px; line-height:30px;">
					トウフ
				</h3>
				<div class="rlistf-items__img"> <img src="images/middle/row2-middle-img4.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						金具を留めるために下に当てる金属製の台。直方体が多いことからいつの間にかその名前に。まさに縁の下の力持ちといったアイテム。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mb50 mt40"><!-- .rlistf-items -->
				<h3 class="font-ryumin mb30" style="color:#5c2700; font-size:30px; line-height:30px;">
					打ち棒
				</h3>
				<div class="rlistf-items__img"> <img src="images/middle/row2-middle-img5.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						カシメやホックを留める際に、金具に当てて打つためのもの。長年打ち込むと、上部が変形し、きのこような形になります。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mb50 mt40"><!-- .rlistf-items -->
				<h3 class="font-ryumin mb30" style="color:#5c2700; font-size:30px; line-height:30px;">
					罫書き、デバイダー
				</h3>
				<div id="comparison"> <!-- #comparison -->
					<div class="col2" style="overflow:hidden;"> <!-- .col2 -->
						<div class="col2-items"> <!-- .col2-items -->
							<div class="comparison-thumb"><img src="images/middle/row2-middle-img6.png" alt=""></div>
						</div> <!-- // .col2-items -->
						<div class="col2-items"> <!-- .col2-items -->
							<div class="comparison-thumb"><img src="images/middle/row2-middle-img7.png" alt=""></div>
						</div> <!-- // .col2-items -->
					</div> <!-- // .col2 -->
				</div>
				<p class="" style="color:#5C2700; line-height: 30px; font-size: 15px;">
					革を裁断したり、金具を取り付けるための印をつける道具。デバイダーは、平行線を引く時に使うけがき。骨董市で調達する作り手も多いです。
				</p>
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mb50 mt40"><!-- .rlistf-items -->
				<h3 class="font-ryumin mb40" style="color:#5c2700; font-size:30px; line-height:30px;">
					ピーラー、鉋
				</h3>
				<div class="rlistf-items__img"> <img src="images/middle/row2-middle-img8.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						革の縁をななめに、時に垂直に削る時の道具。ななめの削りは特にウォッシュ加工など、ナチュラルな雰囲気の鞄やカジュアル感のある鞄によく使います。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mb50 mt40"><!-- .rlistf-items -->
				<h3 class="font-ryumin mb30" style="color:#5c2700; font-size:30px; line-height:30px;">
					やっとこ、ペンチ
				</h3>
				<div class="rlistf-items__img"> <img src="images/middle/row2-middle-img9.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						革パーツを糊付けして貼りあわせる時などに使用します。革が傷つかないよう、先端を丸く削ったり革を当てたり各々で加工しています。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items mb50 mt40"><!-- .rlistf-items -->
				<h3 class="font-ryumin mb40" style="color:#5c2700; font-size:30px; line-height:30px;">
					ガリガリ、糊ケース
				</h3>
				<div class="rlistf-items__img"> <img src="images/middle/row2-middle-img10.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<p class="p-adj">
						糊をつけやすくするために革の床面を「ガリガリ」加工する道具。金属やすりだったり、金属ブラスだったり、謎の骨董品だったり。革を貼りあわせる糊の専用ケースは、日本では珍しいようです。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
		</div> <!-- # - .rlistf -->
	</div>
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">博多工房について</h3>
		<div id="accent-3" class="mt5" ></div>
		<div id="image-wrapper" class="mt30">
			<img src="images/middle/row4-middle-img1.png" alt="">
		</div>
		<div class="mid-row4-cat2">
			<img class="mr43" src="images/middle/row4-middle-img2.png" alt="" style="" />
			<img class="mr43" src="images/middle/row4-middle-img3.png" alt="" style="" />
			<img class="" src="images/middle/row4-middle-img4.png" alt="" style="" />
		</div>
		<p class="f15 mt20">
			感度の高い場所に工房を構えることで、HERZらしさを磨いていく。
		</p>
		<p class="f15 mt20">
			HERZ(ヘルツ)は、博多にも工房を構えています。<br/>
			博多にあって、渋谷にないもの。<br/>
			それは都会の便利さと自然の豊かさが混在していることです。感度の高い街々に工房を構えることで、色々なモノ・コトを吸収し、HERZらしさを磨いていく。変わらない心でモノ作りをするために必要なことだと思っています。			
		</p>
		<div class="comp-profile-row3" style="margin-top:30px;">
			<div id="comp-table">
				<div class="table-1">
					<div class="table-row-1">
						<div class="table-cell table-cell-left" style="width: 176px;">
							<p class="f15">
								住所
							</p>
						</div>
						<div class="table-cell table-cell-right">
							<p class="f15">
								福岡県福岡市博多区博多駅前3-16-10
							</p>
						</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">
							<p class="f15">
								定休日
							</p>
						</div>
						<div class="table-cell table-cell-right">
							<p class="f15">
								日曜・祭日
							</p>
						</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">
							<p class="f15">
								営業時間
							</p>
						</div>
						<div class="table-cell table-cell-right">
							<p class="f15 mb20">
								平日 11:00-19:00
							</p>
							<p class="f15 mb20">
								土曜 11:00-19:00
							</p>
							<p class="f15 mb20 font-weight-bold">
								商品の販売・修理の受付等は対応しておりません。
							<p class="f15">
							<p>
								時間はその日の製作時間によって変わります。							
							</p>
						</div>
					</div>

				</div>
			</div>
		</div>		
		
		
	</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
</div> <!-- // #main-wrapper -->