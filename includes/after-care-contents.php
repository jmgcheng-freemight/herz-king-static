<div id="main-wrapper">

	<div id="section-wrapper">
	<div id="accent-3" style="margin-bottom: 11px;"></div>
	<h3 id="main-title" class="font-ryumin">HERZ のアフターケア<span class="mini-text">AFTER CARE</span></h3>
	<div id="accent-3" style="margin-top: 11px;"></div>
	<div id="image-wrapper">
	<img src="images/after-care/img1.png" alt="" />
	</div>
	</div>
 
	<span id="section" name="elem7"></span>
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin">毎日使って頂くことが最高の手入れとなります</h3>
		<div id="accent-3" style="margin-top: 9px;"></div>
		<p class="f15" style="margin-top:12px;">皮革製品のお手入れは少し面倒です。でもほんの少し手を掛けてあげる事によって、愛着が湧き、カバンの寿命はぐっと延びます。</p>
		<p class="f15" style="">日常でのお手入れが、味のある革を育てる第一歩となるのです。</p>
		<p class="f15" style="">お手入れしながら長く大切に使い続けていくこと。これこそが本当の贅沢だとHERZは考えています。</p>
		<p class="f15" style="margin-top: 25px;">当工房がお作りした革製品のアフターケアは大きく分けて二つに分かれます。</p>
	</div> <!-- #section-wrapper -->
	
	
	
	 <div id="section-wrapper">
	  <h3 id="main-title" class="font-ryumin">1.使い手さんによる定期的なお手入れ</h3>
	  <div id="accent-3" style="margin-top: 9px;"></div>

	  <div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
	  
	   <div class="rlistf-items mt20"><!-- .rlistf-items -->
		<div class="rlistf-items__img"> <img src="images/after-care/img2.png" alt=""/> </div>
		<div class="rlistf-items__dtl" style="margin-top: -7px;"> <!-- .rlistf-items__dtl -->
		 <p>
			オイル塗布など基本的なお手入れについては、使い手の方に行って頂きます。それにより、鞄は長持ちし、手を掛けることで更に愛着が増すことでしょう。
		 </p>
		 <p>
			オイルメンテナンスの方法や雨染み、キズ、ヨゴレの対応など。日々お使い頂く中で、ご愛用者の方が出来るお手入れ方法をご案内します。
		 </p>
		</div> <!-- // .rlistf-items__dtl -->
	   </div><!-- // .rlistf-items -->
		<div class="sd-cont09" style="margin-top:40px; margin-bottom:none; padding-bottom:2px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal; margin-bottom:10px;">関連リンク</h4>
			<div class="sd-cont09-items">
				<div class="sd-cont09-items__img"> <img src="images/after-care/img3.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							お手入れ方法について詳しく見る
						</a>
					</span>
				</div>
			</div>
			<div class="sd-cont09-items">
				<div class="sd-cont09-items__img"> <img src="images/after-care/img4.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							メンテナンス用品
						</a>
					</span>
				</div>
			</div>
		</div>	   
	  </div> <!-- # - .rlistf -->
	 </div> <!-- #section-wrapper -->
	 
	 
	 
	 <div id="section-wrapper">
	  <h3 id="main-title" class="font-ryumin">2.HERZが行う修理と加工</h3>
	  <div id="accent-3" style="margin-top: 11px;"></div>

	  <div id="l-desc" class="rlistf"> <!-- # - .rlistf -->
	  
	   <div class="rlistf-items " style="margin-top:40px;"><!-- .rlistf-items -->
		<div class="rlistf-items__img"> <img src="images/after-care/img5.png" alt=""/> </div>
		<div class="rlistf-items__dtl" style="margin-top: -7px;"> <!-- .rlistf-items__dtl -->
			<p>
				丈夫で長く使える鞄作りを心掛けていますが、壊れないわけではありません。
			</p>
			<p>
				普段のお手入れを怠りますと、修理が必要になる期間も早くなります。
			</p>
			<p>
				特に金具類などは使っていく中で、どうしても摩耗しますので、定期的に交換が必要です。
			</p>
			<p>
				その時、お使いの鞄を拝見し、その状態にふさわしい方法で作り手が丁寧に修理いたします。
			</p>
			<p>
				HERZの鞄はお使い頂く限り、出来るだけの修理・加工をいたします。		 
			</p>
		 
		 
		 
		</div> <!-- // .rlistf-items__dtl -->
	   </div><!-- // .rlistf-items -->

	   
	   
	   
	   <div class="sd-cont09" style="margin-top:40px; margin-bottom:none; padding-bottom:2px;">
			<h4 class="sd-t2 font-ryumin" style="font-size:20px; font-weight:normal; margin-bottom:10px;">関連リンク</h4>
			<div class="sd-cont09-items" style="width:53%;">
				<div class="sd-cont09-items__img"> <img src="images/after-care/img3.png" alt=""> </div>
				<div class="sd-cont09-items__dtl" style="margin-top:30px; width:215px;">
					<span class="font-ryumin" style="font-size:16px; color:#333333; font-weight:normal; line-height:20px;">
						<a href="#">
							修理について詳しく見る
						</a>
					</span>
				</div>
			</div>
			
		</div>	
	   
	  </div> <!-- # - .rlistf -->
	 </div> <!-- #section-wrapper -->



</div> <!-- // #main-wrapper -->