<div id="main-wrapper">

 <div id="section-wrapper">
  <div id="accent-3" style="margin-bottom: 16px;"></div>
  <h3 id="main-title">見出し1が入ります</h3>
  <div id="accent-3" style="margin-top: 11px;"></div>
  <div id="image-wrapper">
   <img src="images/special-detail/sp_img1.png" alt="" />
  </div>
  <div class="sd-div">
    <ul class="sd-ul">
      <li>2015/00/00</li>
      <li><img src="images/special-detail/sd_folder.png"/><span>商品企画</span></li>
    </ul>
    <ul class="sd-social">
      <li><a href=""><img src="images/special-detail/sd_fb.png" alt="" /></a></li>
      <li><a href=""><img src="images/special-detail/sd_twitter.png" alt="" /></a></li>
      <li><a href=""><img src="images/special-detail/sd_google.png" alt="" /></a></li>
    </ul>
  </div>
  
 </div>

 <div id="section-wrapper" class="sd-cont01">
  <h3 id="main-title">目次</h3>
  <div id="accent-3" style="margin-top: 4px;"></div>
  <ul class="sd-list mt20">
    <li>目次1が入ります。
      <ul class="sd-list2">
        <li>目次1の子要素1</li>
        <li>目次1の子要素2</li>
        <li>目次1の子要素3</li>
      </ul>
    </li>
    <li>目次2が入ります。</li>
    <li>目次3が入ります。</li>
    <li>目次4が入ります。</li>
    <li>目次5が入ります。</li>
  </ul>
 </div> <!-- #section-wrapper -->

<div id="section-wrapper">
  <h3 id="main-title">見出し2</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
  <p class="sd15 mt20">
    ご注文いただいた商品と一緒にペーパークラフトの型紙を送ります。型紙には説明文も記載していますので、その内容に沿って、工作してみて下さい！！定規とカッターと糊があれば完成します！！
  </p>
 </div> <!-- #section-wrapper -->

 <div id="section-wrapper">
  <h3 id="main-title">見出し2</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
  <div id="image-wrapper" class="mb50">
   <img src="images/special-detail/sd_img3.png" alt="" />
  </div>
  <h3 id="main-title" class="">見出し3</h3>
  <p class="sd15 mt20">
    テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
  </p>
  <p class="sd15 mt20">
テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
  </p>
  <div class="sd-cont02 mt50">
  <img src="images/special-detail/sd_img4.png" alt="" />
  <p class="sd15">
 テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
 </p>
 <p class="sd15 mt20">     
 テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
 </p>
 </div>
 </div> <!-- #section-wrapper -->

 <div id="section-wrapper">
  <h3 id="main-title">見出し2</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
  <div class="sd-cont03">
   <div class="sd-cont03-img">
    <img src="images/special-detail/sd_img5.png" alt="" />
    <p class="sd15 ">     
      テキストが入ります。テキストが入ります。テキストが入ります。
    </p>
   </div>
    
   <div class="sd-cont03-img">
    <img src="images/special-detail/sd_img5.png" alt="" />
     <p class="sd15 ">     
      テキストが入ります。テキストが入ります。テキストが入ります。
    </p>
   </div>
  </div>
   <div class="sd-cont04 mt50">
  <h3 id="main-title">見出し2</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
  <div class="mt20">
   <img src="images/special-detail/sd_img6.png" alt="" />
   <p class="sd15 mt20">     
    テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
   </p>
   <p class="sd15 mt20">   
    テキストが入ります。テキストが入ります。テキストが入ります。
   </p>
  </div>
 </div> 
 </div> <!-- #section-wrapper -->

 <div id="section-wrapper">
  <div class="sd-cont05">
    <div class="sd-cont05-item">
      <img src="images/special-detail/sd_img7.png" alt="" />
      <p class="sd15">
        テキストが入ります。テキストが入ります。テキストが入ります。
      </p>
    </div>
    <div class="sd-cont05-item">
      <img src="images/special-detail/sd_img8.png" alt="" />
      <p class="sd15">
        テキストが入ります。テキストが入ります。テキストが入ります。
      </p>
    </div>
    <div class="sd-cont05-item">
      <img src="images/special-detail/sd_img9.png" alt="" />
      <p class="sd15">
        テキストが入ります。テキストが入ります。テキストが入ります。
      </p>
    </div>
  </div>
 
 </div> <!-- #section-wrapper -->

<div id="section-wrapper">
 <div class="sd-cont06">
   <ul>
     <li>
       <div class="sd-cont06-p">
         <span>名前が入ります。</span>
       </div>
       <div class="sd-cont06-c">
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
         </p>
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。</p>
       </div>
     </li>
     <li>
       <div class="sd-cont06-p">
         <span>名前が入ります。</span>
       </div>
       <div class="sd-cont06-c">
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
         </p>
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。</p>
       </div>
     </li>
     <li>
       <div class="sd-cont06-p">
         <span>名前が入ります。</span>
       </div>
       <div class="sd-cont06-c">
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
         </p>
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。</p>
       </div>
     </li>
     <li>
       <div class="sd-cont06-p">
         <span>名前が入ります。</span>
       </div>
       <div class="sd-cont06-c">
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
         </p>
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
         テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。</p>
       </div>
     </li>
     <li>
       <div class="sd-cont06-p">
         <div><img src="images/special-detail/sd_p-img1.png" alt="" /></div>
         <span>名前が入ります。</span>
       </div>
       <div class="sd-cont06-c">
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
         </p>
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。</p>
       </div>
     </li>
     <li>
       <div class="sd-cont06-p">
         <div><img src="images/special-detail/sd_p-img2.png" alt="" /></div>
         <span>名前が入ります。</span>
       </div>
       <div class="sd-cont06-c">
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
         </p>
         <p class="sd15">テキストが入ります。テキストが入ります。テキストが入ります。</p>
       </div>
     </li>
   </ul>
 </div>
 <div class="sd-cont07">
    <h3 id="main-title" >見出し2</h3>
    <ul class="mt35">
      <li>箇条書きリスト1の体裁が入ります。</li>
      <li>箇条書きリスト2の体裁が入ります。</li>
      <li>箇条書きリスト3の体裁が入ります。</li>
      <li>箇条書きリスト4の体裁が入ります。</li>
      <li>箇条書きリスト5の体裁が入ります。</li>
    </ul>
    <ol class="mt50">
      <li>箇条書きリスト1の体裁が入ります。</li>
      <li>箇条書きリスト2の体裁が入ります。</li>
      <li>箇条書きリスト3の体裁が入ります。</li>
      <li>箇条書きリスト4の体裁が入ります。</li>
      <li>箇条書きリスト5の体裁が入ります。</li>
    </ol>
 </div>
 <p class="sd15 mt50 mb50 sdb">太字の体裁はこの体裁。wordpressで「B」を選択した時の体裁</p>
 <div class="sd-cont08">
  <p class="sd15">引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。
引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。
</p>
  <p class="sd15">引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。</p>
 </div>
 <p class="sd15 mb50">ご注文いただいた商品と一緒にペーパークラフトの型紙を送ります。型紙には説明文も記載していますので、その内容に沿って、工作してみて下さい！！<span class="sd-underline">文章中にリンクが貼られるとこんな体裁になります</span>定規とカッターと糊があれば完成します！！</p>

 <p class="sd15 sd-linethru">打ち消し線の場合の表示</p>

 <h4 class="sd-t">見出し4</h4>
 <p class="sd15">ご注文いただいた商品と一緒にペーパークラフトの型紙を送ります。型紙には説明文も記載していますので、その内容に沿って、工作してみて下さい！！定規とカッターと糊があれば完成します！！</p>
 <h4 class="sd-t">見出し5</h4>
 <p class="sd15">ご注文いただいた商品と一緒にペーパークラフトの型紙を送ります。型紙には説明文も記載していますので、その内容に沿って、工作してみて下さい！！定規とカッターと糊があれば完成します！！</p>
 <h4 class="sd-t">見出し6</h4>
 <p class="sd15">ご注文いただいた商品と一緒にペーパークラフトの型紙を送ります。型紙には説明文も記載していますので、その内容に沿って、工作してみて下さい！！定規とカッターと糊があれば完成します！！</p>

 <span class="sd-link">作り手が教えるメンテナンス</span>
</div> <!-- #section-wrapper -->

<div id="section-wrapper">
  <h3 id="main-title">バックナンバー</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
   <div id="sd-rf" class="rlistf mt20"> <!-- #sd-rf - .rlistf -->
    <div class="rlistf-items"><!-- .rlistf-items -->
     <div class="rlistf-items__img"> <img src="images/special-detail/sd_img10.png" alt="" /></div>
     <div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
       <span class="sd-link2">創業者とカバン作ろう第２弾 ～作り手：ナカムラ編 vol.1～</span>
     </div> <!-- // .rlistf-items__dtl -->
    </div><!-- // .rlistf-items -->
   
    <div class="rlistf-items"><!-- .rlistf-items -->
     <div class="rlistf-items__img"> <img src="images/special-detail/sd_img11.png" alt="" /> </div>
     <div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
      <span class="sd-link2">創業者とカバン作ろう第２弾 ～作り手：ナカムラ編 vol.2～</span>
     </div> <!-- // .rlistf-items__dtl -->
    </div><!-- // .rlistf-items -->
   </div> <!-- #sd-rf - .rlistf -->
   <div class="sd-cont09">
     <h4 class="sd-t2">関連リンク</h4>
     <div class="sd-cont09-items">
      <div class="sd-cont09-items__img"> <img src="images/special-detail/sd_img12.png" alt="" /> </div>
        <div class="sd-cont09-items__dtl">
       <span>HERZ仙台店 in 南相馬</span>
     </div>
     </div>
     <div class="sd-cont09-items">
     <div class="sd-cont09-items__img"> <img src="images/special-detail/sd_img13.png" alt="" /> </div>
     <div class="sd-cont09-items__dtl">
       <span>創業者とカバン作ろうttt</span>
     </div>
     </div>
   </div>
   <div class="sd-cont10">
     <ul class="sd-social-l">
      <li><a href=""><img src="images/special-detail/sd_fb.png" alt="" /></a></li>
      <li><a href=""><img src="images/special-detail/sd_twitter.png" alt="" /></a></li>
      <li><a href=""><img src="images/special-detail/sd_google.png" alt="" /></a></li>
    </ul>
   </div>
    

    <div class="sd-cont11">
      <div class="sd-cont11-in">
        <div class="sd-cont11__fb-cont">
          <div>
            <span>FACEBOOK</span>
          <p>日々のあれこれを気ままに投稿中！！<br>
          鞄工房HERZ(ヘルツ)の公式Facebookページ</p>
          </div>
        </div>
        <div class="sd-cont11__fb">
          <img src="images/special-detail/sd_img14.png" alt="" />
        </div>
      </div>
    </div>
 </div> <!-- #section-wrapper -->
</div> <!-- // #main-wrapper -->