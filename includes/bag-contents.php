<div id="main-wrapper">

	<div id="section-wrapper">
		<div id="accent-3" style="margin-bottom: 15px;"></div>
		<h3 id="main-title" class="main-title-attention">HERZの鞄<span class="mini-text-attention" style="padding-left:10px;">HERZ BAG</span></h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<div id="image-wrapper">
			<img src="images/bag/img1.png" alt="" />
		</div>
		<div style="text-align:center;">
			<p class="f15 font-ryumin p-adj" style="margin-top:30px; margin-bottom:50px; font-size:20px; line-height:46px;">
				持ちやすく、使いやすく<br/>
				飽きのこない丈夫なもの。<br/>
				そして使っていくほどに持ち主に馴染んでいく。<br/>
				それがHERZの鞄です。
			</p>
		</div>
	</div>
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin">革の個性と作り手の個性が生み出す二つとない鞄</h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<p class="f15 p-adj" style="margin-top: 13px; margin-bottom: 34px;">
			ナチュラルなタンニン鞣しの革による豊かな革の表情と、作り手一人一人の作りの個性が組み合わさり、文字通り二つとない鞄が日々生まれています。デザインは同じでも、表情は一つ一つ違うのです。<br/>
			その一つ一つの個性に、手作り鞄だからこその魅力があります。みなさんも、どうぞその個性を楽しんで下さい。
		</p>
		<img src="images/bag/img2.png" alt="" style="display: block; margin-top: 45px;">
	</div>
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">革の豊かな表情は、天然革である証</h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<div id="image-wrapper" class="tm-20">
			<img src="images/bag/img3.png" alt="" />
		</div>
		<p class="f15 p-adj" style="margin-top: 13px;">
			HERZの革には、牛が生きていた頃のバラキズ、トラ（シワ）、血筋が多く残っています。<br/>
			それは天然革である証。上から色を沢山乗せたり、表面に加工をしてしまえば消えてしまう多くの表情を隠したりせず敢えてそのままに。革本来の自然な風合いを大切に、革を作っていただいています。<br/>
			HERZの鞄を「雰囲気がある」「味がある」と言って頂けるのもこの天然革だからこそ。<br/>
			天然の革に初めて触れる方は少し驚かれてしまうこともあるそんな個性豊かな面々ですが、牛の息遣いを感じてもらい、革を存分に楽しんで頂ければと思います。
		</p>
		<div id="l-desc" class="rlistf" style="margin-bottom:30px;"> <!-- # - .rlistf -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/bag/img4.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<h3 class="font-ryumin" style="color:#5c2700; font-size:30px; line-height:30px; margin-bottom:30px;">
						バラキズ
					</h3>
					<p class="p-adj">
						革に入っている、引っ掻いたような傷。これは牛が生きていた頃に枝で引っ掛けたり、引っ掻いた時に出来た傷の跡。牛が生きていたことを強く感じる、これはまさに天然革の証です。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/bag/img5.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<h3 class="font-ryumin" style="color:#5c2700; font-size:30px; line-height:30px; margin-bottom:30px;">
						トラ
					</h3>
					<p class="p-adj">
						通称：トラと呼んでいるこちらの筋状の縦模様。首や肩の周りや背中など、シワが寄りやすい部位によく見られます。もちろん一体一体模様の入り方は異なり、またトラの入る部分は限られています。それもあってか革が好きな方はこのトラが好きな方も多く、HERZの鞄の表情を豊かに見せてくれる大切な風合いの一つです。全ての革に入っているわけではないので、これも出会いですね。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/bag/img6.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<h3 class="font-ryumin" style="color:#5c2700; font-size:30px; line-height:30px; margin-bottom:30px;">
						血筋
					</h3>
					<p class="p-adj">
						この葉脈の様な模様は、人間の手の甲に見られる血管と同様牛の皮膚のすぐ下を血管が通っていた跡です。形や残り方も個々で違い、これもまた革らしい表情を演出してくれる重要な要素の１つです。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
			<div class="rlistf-items" style="margin-top: 40px;"><!-- .rlistf-items -->
				<div class="rlistf-items__img"> <img src="images/bag/img7.png" alt=""> </div>
				<div class="rlistf-items__dtl"> <!-- .rlistf-items__dtl -->
					<h3 class="font-ryumin" style="color:#5c2700; font-size:30px; line-height:30px; margin-bottom:30px;">
						色ムラ
					</h3>
					<p class="p-adj">
						人間と同じように牛一体の中でも、繊維の密度は部位によって異なりキメの細かさや風合いも異なります。狭い範囲でもその差がある為、染色した際の色の染まり方も変わってくるのです。<br/>
						それが色の濃淡となって、表面に表れることがあります。また、こうした1枚の革の中の色ムラは革を染色した時にはじめて出てくるものがほとんどです。
					</p>
				</div> <!-- // .rlistf-items__dtl -->
			</div><!-- // .rlistf-items -->
		</div> <!-- # - .rlistf -->
	</div>
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">1枚革仕上げ</h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<div id="image-wrapper" class="tm-20">
			<img src="images/bag/img8.png" alt="" />
		</div>
		<p class="f15 p-adj" style="margin-top: 13px;">
			HERZでは裏地は付けずに一枚革で鞄・小物を仕上げています。革を存分に楽しんでもらいたいという気持ちはもちろん、永く使っているとどうしても革よりも裏地の痛みが早くなり修理に時間や金額が多くかかってしまうからです。<br/>
			永く使う上で必要になってくる修理が、なるべくお客様の負担にならず修理して使おうと思える範囲でお受けできるよう修理を見込んだ鞄作りをしています。
		</p>
	</div>
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin" style="line-height:48px;">商品の深み</h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<div id="image-wrapper" class="tm-20">
			<img src="images/bag/img9.png" alt="" />
		</div>
		<p class="f15 p-adj" style="margin-top: 13px;">
			HERZの鞄は、薄化粧の革故の経年変化も大きな楽しみの一つ。時が経つほどに、使うほどに鞄はどんどん変化していきます。気づけば初めの頃より色が濃くなって、革も馴染んで柔らかくなり、いつの間にかついた傷や気にしていた染みも一つの表情になっていきます。<br/>
			その方の使う頻度や、環境やお手入れによって実に様々な表情を見せてくれるのです。3年、5年、10年と鞄とともに過ごしてみてください。きっと他ではない、自分だけの鞄に育っているはずです。
		</p>
	</div>
	
	<div id="section-wrapper">
		<h3 id="main-title" class="font-ryumin">作り手が生み出す新しい鞄たち</h3>
		<div id="accent-3" style="margin-top: 13px;"></div>
		<p class="f15 p-adj" style="margin-top: 20px; margin-bottom: 34px; width: 705px">
			HERZの新作は、日々鞄作りをしている作り手達によって生みだされています。自分が思いついたことを思いついた時に時間を見つけて、日々の中で作っているのです。<br/>
			だから、アパレルメーカー等で見られるようにシーズン事に新作を出すわけではなく「良いと思う物」「納得のいく物」ができた時に、それは新作として販売されます。<br/>
			新作の考案は、日常的に日々、いろんな時間、いろんなスタッフ達とそこ、ここで行われているのです。
		</p>
		<img src="images/bag/img10.png" alt="" style="display: block; ">
	</div>
	
</div> <!-- // #main-wrapper -->