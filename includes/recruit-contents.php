<div id="main-wrapper">
 
 <div id="section-wrapper">
  <div id="accent-3" style="margin-bottom: 15px;"></div>
  <h3 id="main-title">採用情報<span class="mini-text">-RECRUIT-</span></h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="image-wrapper">
   <img src="images/common/middle/middle-img-13.png" alt="">
  </div>
 </div>

 <div id="section-wrapper">
  <h3 id="main-title">HERZの仕事において大事にしていること</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <p class="content-txt mb50">
   株式会社ヘルツでは、下記の働く上での指針に共感し実行できる人材を求めています。<br>
  <br>
  以下、HERZ服務心得より抜粋
  </p>
  <h3 id="main-title">服務の原則</h3>
  <ul class="content-list mb50">
   <li>つくる喜びを伝え共有する。</li>
  </ul>
  <h3 id="main-title">服務の原則</h3>
  <ul class="content-list">
   <li>仕事の進め方はチームワークを基本とし、全体の中でのチームの役割、チームの中での自己の役割を理解すること。</li>
   <li>それぞれが将来を託して働ける環境となるよう、協力し合うこと。</li>
   <li>つくること、売ること、使っていただくことの繋がりを意識して自己の業務に責任を持つこと。</li>
   <li>新しいコトに挑戦する気持ち、新しいカタチを生み出す気持ちを持ち続けること。</li>
  </ul>
 </div> <!-- // #section-wrapper -->
 
 <div id="section-wrapper">
  <h3 id="main-title">雇用条件・待遇等</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  
  <div id="table-style-1">
   <div id="row">
    <div id="cell" class="left">
     雇用条件
    </div>
    <div id="cell" class="right">
     正社員
    </div>
   </div>
   <div id="row">
    <div id="cell" class="left">
     給与・待遇
    </div>
    <div id="cell" class="right">
     経験、能力を考慮の上、当社既定により優遇（※試用期間3ヶ月有り）　社会保険有り
    </div>
   </div>
   <div id="row">
    <div id="cell" class="left">
     賞与
    </div>
    <div id="cell" class="right">
     年2回支給
    </div>
   </div>
   <div id="row">
    <div id="cell" class="left">
     昇給
    </div>
    <div id="cell" class="right">
     年1回
    </div>
   </div>
   <div id="row">
    <div id="cell" class="left">
     休日・休暇
    </div>
    <div id="cell" class="right">
     週休2日制　年間120日前後　年次有給休暇あり
    </div>
   </div>
   <div id="row">
    <div id="cell" class="left">
     勤務時間
    </div>
    <div id="cell" class="right">
     就業規則に記載（※勤務地によって異なる）
    </div>
   </div>
  </div>
 </div> <!-- // #section-wrapper -->

 <div id="section-wrapper">
  <h3 id="main-title">採用の流れ</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="image-wrapper2">
   <img src="images/common/middle/middle-img-14.png" alt="">
  </div>
 </div> <!-- #section-wrapper -->

 <div id="section-wrapper">
  <h3 id="main-title">勤務地・募集職種</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <ul class="title-list">
   <li><a href="#elem1">東京/HERZ本店・Organ</a></li>
   <li><a href="#elem2">東京/オンラインショップ</a></li>
   <li><a href="#elem3">HERZ仙台店/仙台店スタッフ</a></li>
  </ul>
 </div> <!-- #section-wrapper -->

 <span id="section" name="elem1"></span>
 <div id="section-wrapper">
  <h3 id="main-title">東京/HERZ本店・Organ</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="table-style-3">
   
   <div id="row">
    <div id="cell" class="left">
    </div>
    <div id="cell" class="right">
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    募集職種
    </div>
    <div id="cell" class="right">
    HERZ本店スタッフ・Organスタッフ<br>
    店舗工房業務全般（制作、店舗管理、接客、企画<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    応募資格
    </div>
    <div id="cell" class="right">
    ※中途・新卒問いません。
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    集人員
    </div>
    <div id="cell" class="right">
    2名
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    雇用形態
    </div>
    <div id="cell" class="right">
    正社員
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    雇用条件
    </div>
    <div id="cell" class="right">
    上記 『雇用条件・待遇等』 に記載
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    勤務開始時期
    </div>
    <div id="cell" class="right">
    2016年1月～
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    応募書類送付先
    </div>
    <div id="cell" class="right">
    〒150-0001<br> 
    東京都渋谷区神宮前5-46-16　イルチェントロセレーノB1F<br>
    HERZ本店・Organスタッフ採用担当 宛<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    連絡先
    </div>
    <div id="cell" class="right">
    採用担当：北野　<br>
    Mail:herz.kitano@leatherbag.co.jp<br>
    ※採用に関するお問い合わせは上記宛先のみにお願いします。<br>
    </div>
   </div>
  </div>
 </div> <!-- #section-wrapper -->

 <span id="section" name="elem2"></span>
 <div id="section-wrapper">
  <h3 id="main-title">東京/オンラインショップ</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="table-style-3">

   <div id="row">
    <div id="cell" class="left">
    </div>
    <div id="cell" class="right">
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    募集職種
    </div>
    <div id="cell" class="right">
    オンラインショップスタッフ<br>
    お客様から頂くお問い合わせの対応、受注管理、発送業務がメイン<br>
    (経験を重ねた後、商品写真の撮影やページ作り等の制作業務にも携わって頂きます。)<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    応募資格
    </div>
    <div id="cell" class="right">
    ※中途・新卒問いません。
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    募集人員
    </div>
    <div id="cell" class="right">
    1名
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    雇用形態
    </div>
    <div id="cell" class="right">
    正社員
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    雇用条件
    </div>
    <div id="cell" class="right">
    上記 『雇用条件・待遇等』 に記載
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    勤務開始時期
    </div>
    <div id="cell" class="right">
    2016年1月～
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    応募書類送付先
    </div>
    <div id="cell" class="right">
    〒150-0001 <br>
    東京都渋谷区神宮前5-46-16 イルチェントロセレーノB1F<br>
    HERZオンラインショップ 採用担当 宛<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    連絡先
    </div>
    <div id="cell" class="right">
    採用担当：小野寺<br>
    Mail:herz.onodera@leatherbag.co.jp<br>
    ※採用に関するお問い合わせは上記宛先のみにお願いします。<br>
    </div>
   </div>
  </div>
 </div> <!-- #section-wrapper -->
 
  <span id="section" name="elem3"></span>
  <div id="section-wrapper">
  <h3 id="main-title">HERZ仙台店</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="table-style-3">

   <div id="row">
    <div id="cell" class="left">
    </div>
    <div id="cell" class="right">
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    募集職種
    </div>
    <div id="cell" class="right">
    仙台店スタッフ<br>
    店舗工房業務全般（制作、店舗管理、接客、企画）<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    応募資格
    </div>
    <div id="cell" class="right">
    ※中途・新卒問いません。
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    募集人員
    </div>
    <div id="cell" class="right">
    1名
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    雇用形態
    </div>
    <div id="cell" class="right">
    正社員
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    雇用条件
    </div>
    <div id="cell" class="right">
    上記 『雇用条件・待遇等』 に記載
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    勤務開始時期
    </div>
    <div id="cell" class="right">
    2016年1月～
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    応募書類送付先
    </div>
    <div id="cell" class="right">
    〒980-0014<br>
    宮城県仙台市青葉区本町2-10-33 第二日本オフィスビル1F<br>
    HERZ仙台店　採用担当 宛<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    連絡先
    </div>
    <div id="cell" class="right">
    採用担当：清水<br>
    Mail:herz.sendai@leatherbag.co.jp<br>
    ※採用に関するお問い合わせは上記宛先のみにお願いします。<br>
    </div>
   </div>
  </div>
 </div> <!-- #section-wrapper -->

</div> <!-- // #main-wrapper -->