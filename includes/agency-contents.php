<div id="main-wrapper">
 
 <div id="section-wrapper">
  <div id="accent-3" style="margin-bottom: 15px;"></div>
  <h3 id="main-title">取扱店<span class="mini-text">-AGENCY-</span></h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="image-wrapper">
   <img src="images/common/middle/middle-img-15.png" alt="">
  </div>
  <ul class="shop-list">
   <li><a href="#elem1">直営店</a></li>
   <li><a href="#elem2">北海道 東北地方</a></li>
   <li><a href="#elem3">関東地方</a></li>
   <li><a href="#elem4">中部 東海地方</a></li>
   <li><a href="#elem5">近畿 関西地方</a></li>
   <li><a href="#elem6">中国 四国地方</a></li>
   <li><a href="#elem7">九州地方</a></li>
   <li><a href="#elem8">海外 その他</a></li>
  </ul>
 </div>
 
 <span id="section" name="elem1"></span>
 <div id="section-wrapper">
  <h3 id="main-title">直営店</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="table-style-2">
   
   <div id="row">
    <div id="cell" class="left">
    </div>
    <div id="cell" class="right">
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    HERZ本店
    </div>
    <div id="cell" class="right">
    <span>03-3406-1471</span><br>
    東京都渋谷区神宮前5-46-16　イルチェントロセレーノB1F<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    FACTORY SHOP（渋谷工房）
    </div>
    <div id="cell" class="right">
    <span>03-6427-3250</span><br>
    東京都渋谷区渋谷2-12-8 中村ビル1Ｆ<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    Organ（オルガン）
    </div>
    <div id="cell" class="right">
    <span>03-3406-2010</span><br>
    東京都渋谷区渋谷2-12-6 三田ビル1F<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    大阪店
    </div>
    <div id="cell" class="right">
    <span>06-6539-2525</span><br>
    大阪府大阪市西区南堀江2-4-4<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    仙台店
    </div>
    <div id="cell" class="right">
    <span>022-395-7461</span><br>
    宮城県仙台市青葉区本町2-10-33<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    公式通販サイト
    </div>
    <div id="cell" class="right">
     <span>03-6427-3250</span><br>
     東京都渋谷区神宮前5-46-16　イルチェントロセレーノB1F<br>
    </div>
   </div>
  </div>
 </div> <!-- #section-wrapper -->

 <span id="section" name="elem2"></span>
 <div id="section-wrapper">
  <h3 id="main-title">北海道・東北・上越地方</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="table-style-2">
   
   <div id="row">
    <div id="cell" class="left">
    </div>
    <div id="cell" class="right">
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    ブルータグ
    </div>
    <div id="cell" class="right">
    <span>0166-50-1006</span><br>
    北海道旭川市旭町一条8丁目 MSグランベリー 1F<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    kasi-friendly
    </div>
    <div id="cell" class="right">
    <span>019-606-3810</span><br>
    岩手県盛岡市材木町3-8岩手ビル1階<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    ボーデコール
    </div>
    <div id="cell" class="right">
    <span>0250-22-0195</span><br>
    新潟県新潟市秋葉区新津4462-1<br>
    </div>
   </div>

  </div>
 </div> <!-- #section-wrapper -->

<span id="section" name="elem3"></span>
 <div id="section-wrapper">
  <h3 id="main-title">関東地方</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="table-style-2">
   
   <div id="row">
    <div id="cell" class="left">
    </div>
    <div id="cell" class="right">
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    新盛堂鞄店本店
    </div>
    <div id="cell" class="right">
    <span>03-3352-7033</span><br>
    新宿区新宿3-29-11<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    L.S.TOM
    </div>
    <div id="cell" class="right">
    <span>03-3836-5968</span><br>
    台東区上野6-10-7 アメ横プラザA棟53号永井商店内<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    マルキン商店
    </div>
    <div id="cell" class="right">
    <span>03-3831-8423</span><br>
    台東区上野6-4-4 御徒町センタ－内<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    丸菱
    </div>
    <div id="cell" class="right">
    <span>03-3843-5366</span><br>
    台東区浅草1-19-8<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    ヤンピ
    </div>
    <div id="cell" class="right">
    <span>03-3352-8956</span><br>
    新宿区新宿3-17-7 紀伊国屋書店1F<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    鞄の専門店ヒロセ
    </div>
    <div id="cell" class="right">
    <span>0422-43-6293</span><br>
    三鷹市下連雀3-34-2<br>
    </div>
   </div>

  <div id="row">
   <div id="cell" class="left">
    ヒロキポルタ店
    </div>
    <div id="cell" class="right">
    <span>045-453-6336</span><br>
    横浜市西区高島2-16 横浜駅地下街ポルタ<br>
   </div>
  </div>

  <div id="row">
    <div id="cell" class="left">
    ヒロキ元町店
    </div>
    <div id="cell" class="right">
    <span>045-681-4741</span><br>
    横浜市中区元町2-96<br>
   </div>
  </div>

  <div id="row">
    <div id="cell" class="left">
    サンペイ鞄店　館山店
    </div>
    <div id="cell" class="right">
    <span>0470-22-1086</span><br>
    千葉県館山市北条1823<br>
   </div>
  </div>

  <div id="row">
    <div id="cell" class="left">
    サンペイ鞄店　木更津店
    </div>
    <div id="cell" class="right">
    <span>0438-25-2550</span><br>
    千葉県木更津市大和1-3-16<br>
   </div>
  </div>

  </div>
 </div> <!-- #section-wrapper -->

 <span id="section" name="elem4"></span>
 <div id="section-wrapper">
  <h3 id="main-title">中部・東海地方</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="table-style-2">
   
   <div id="row">
    <div id="cell" class="left">
    </div>
    <div id="cell" class="right">
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    クラフトA
    </div>
    <div id="cell" class="right">
    <span>0762-60-2495</span><br>
    金沢市武蔵町15-1 金沢名鉄丸越百貨店5F家庭用品課<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    BENLLY'S & JOB
    </div>
    <div id="cell" class="right">
    <span>076-234-5383</span><br>
    石川県金沢市新竪町3-34<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    インク
    </div>
    <div id="cell" class="right">
    <span>055-971-4336</span><br>
    静岡県駿東郡清水町卸団地244<br>
    </div>
   </div>

  </div>
 </div> <!-- #section-wrapper -->

 <span id="section" name="elem5"></span>
 <div id="section-wrapper">
  <h3 id="main-title">近畿・関西地方</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="table-style-2">
   
   <div id="row">
    <div id="cell" class="left">
    </div>
    <div id="cell" class="right">
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    note
    </div>
    <div id="cell" class="right">
    <span>078-923-5431</span><br>
    兵庫県加西市北条町北条308-1 イオン加西北条SC1F<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    富士カバン店
    </div>
    <div id="cell" class="right">
    <span>078-331-1042</span><br>
    兵庫県神戸市中央区三宮町1-6-12<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    Osmund Drive
    </div>
    <div id="cell" class="right">
    <span>0792-67-5717</span><br>
    兵庫県姫路市青山西5-7-12 1F<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    鞄ヤ
    </div>
    <div id="cell" class="right">
    <span>06-6454-0038</span><br>
    大阪市北区梅田1-3-1 大阪駅前第1ビルB2F<br>
    </div>
   </div>

  </div>
 </div> <!-- #section-wrapper -->
 
  <span id="section" name="elem6"></span>
  <div id="section-wrapper">
  <h3 id="main-title">中国・四国地方</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="table-style-2">
   
   <div id="row">
    <div id="cell" class="left">
    </div>
    <div id="cell" class="right">
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    CRAFT JOURNAL
    </div>
    <div id="cell" class="right">
    <span>082-555-0206</span><br>
    広島県広島市西区草津新町2-14-21<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    レザーハウス
    </div>
    <div id="cell" class="right">
    <span>0888-25-2095</span><br>
    高知県高知市旭町3丁目61-1<br>
    </div>
   </div>

  </div>
 </div> <!-- #section-wrapper -->
 
 <span id="section" name="elem7"></span>
 <div id="section-wrapper">
  <h3 id="main-title">九州地方</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="table-style-2">
   
   <div id="row">
    <div id="cell" class="left">
    </div>
    <div id="cell" class="right">
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    ポルコロッソ 楽天市場店
    </div>
    <div id="cell" class="right">
    <span>092-434-7307</span><br>
    福岡県福岡市博多区博多駅前3-13-1 林英ビル6階<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    ポルコロッソ　天神店
    </div>
    <div id="cell" class="right">
    <span>092-738-5885</span><br>
    岡県福岡市中央区今泉2-5-30<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    ポルコロッソ キャナルシティ店
    </div>
    <div id="cell" class="right">
    <span>092-263-2170</span><br>
    福岡県福岡市博多区住吉1-2-22キャナルシティーOPA1F<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    カバンのヒグチ
    </div>
    <div id="cell" class="right">
    <span>099-224-3357</span><br>
    鹿児島市東千石町15-5<br>
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    レボルセ
    </div>
    <div id="cell" class="right">
    <span>099-812-6025</span><br>
    鹿児島市中央町1-1　アミュプラザ鹿児島3F<br>
    </div>
   </div>

  </div>
 </div> <!-- #section-wrapper -->


  <span id="section" name="elem8"></span>
  <div id="section-wrapper">
  <h3 id="main-title">海外・その他</h3>
  <div id="accent-3" style="margin-top: 13px;"></div>
  <div id="table-style-2">
   
   <div id="row">
    <div id="cell" class="left">
    </div>
    <div id="cell" class="right">
    </div>
   </div>

   <div id="row">
    <div id="cell" class="left">
    台湾：「鞄工坊(Kaban <br/>Cobo)」
    </div>
    <div id="cell" class="right">
    <span>(04) 2229 3506</span><br>
    138-1, Yi-Chung Street, 1F North Dist., Taichung City<br>
    </div>
   </div>

  </div>
 </div> <!-- #section-wrapper -->


</div> <!-- // #main-wrapper -->