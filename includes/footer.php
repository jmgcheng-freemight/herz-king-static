<div id="accent"></div> <!-- #accent -->
<div id="p-footer-wrapper"> <!-- #p-footer-wrapper -->
 <div id="p-footer"> <!-- #p-footer -->
  <div id="footer-logo">
   <img src="images/common/bot-logo.png" alt="">
   <p class="slogan">
   時を経てこそ解る味わいがある。使い込んでこそ伝わる温もりがある。<br>
   デザインから製作まで１人の作り手が心を込めて最後まで仕上げるもの作り。<br>
   それがヘルツのスピリット。</p>
  </div>
  <div id="footer-nav">
   <ul class="menu">
    <li><a href="https://twitter.com/herz_bag" target="_blank"><img class="icon" src="images/common/icons/tw-bot.png" alt=""/>TWITTER</a></li>
    <li><a href="https://www.facebook.com/herzbag" target="_blank"><img class="icon" src="images/common/icons/fb-bot.png" alt=""/>FACEBOOK</a></li>
    <li><a href="https://www.herz-bag.jp/webshop/mailmagazine.html" target="_blank"><img class="icon" src="images/common/icons/th-bot.png" alt=""/>TUMBLER</a></li>
    <li><a href="https://www.herz-bag.jp/webshop/mailmagazine.html" target="_blank"><img class="icon" src="images/common/icons/msg-bot.png" alt=""/>MAIL MAGAZINE</a></li>
    <li><a href=""><img class="icon" src="images/common/icons/map-bot.png" alt=""/>SITE MAP</a></li>
    <li><a href="https://www.herz-bag.jp/webshop/" target="_blank"><img class="icon" src="images/common/icons/cart-bot.png" alt=""/>ONLINE SHOP</a></li>
   </ul>
   <p class="copyright">Copyright (c) 2000-2015 HERZ Co.,Ltd. All Rights Reserved.</p>
  </div>
 </div> <!-- // #p-footer -->
</div> <!-- // #p-footer-wrapper -->
<a href="#" class="page-top-img"><img src="images/common/page-top.png" alt=""/></a>
</div> <!-- // #p-wrap -->

<!-- JS -->
<script src="js/jquery.bxslider.js"></script>
<script src="js/jquery.bxslider2.js"></script>

<script type="text/javascript">
 $(document).ready(function(){
 
 //Check to see if the window is top if not then display button
 $(window).scroll(function(){
  if ($(this).scrollTop() > 100) {
   $('.page-top-img').fadeIn();
  } else {
   $('.page-top-img').fadeOut();
  }
 });
 
 //Click event to scroll to top
 $('.page-top-img').click(function(){
  $('html, body').animate({scrollTop : 0},800);
  return false;
 });

 $(window).scroll(function(){
  if ($(this).scrollTop() > 100) {
   $('.page-top-img').fadeIn();
  } else {
   $('.page-top-img').fadeOut();
  }
 });

 //Sticky Header
 $(function(){
  var sticky = $('#p-header-wrapper').offset().top;
          
 $(window).scroll(function(){
  if( $(window).scrollTop() > sticky ) {
  $('#p-header-wrapper').css({position: 'fixed', top: '0px'});
  } else {
  $('#p-header-wrapper').css({position: 'static', top: '0px'});
  }
 });
});


 
});
</script>

<script>
    $(document).ready(function(){
        $('.bxslider2').bxSlider();
    });
</script>




<script>
    $(document).ready(function(){
        $('.slider1').bxSlider1({
            slideWidth: 206,
            minSlides: 2,
            maxSlides: 4,
            moveSlides: 1,
            slideMargin: 45
        });
    });

</script>


<script>
    $(document).ready(function(){
        $('.slider2').bxSlider1({
            slideWidth: 205,
            minSlides: 2,
            maxSlides: 4,
            moveSlides: 1,
            slideMargin: 45
        });
    });



</script>
</body>
</html>