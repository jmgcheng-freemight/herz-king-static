<div id="p-banner">
    <p>
        鞄は道具です。<br/>
        持ちやすく、使いやすく <br/>
        飽きのこない丈夫なものがいい。<br/>
        それが「ヘルツの鞄」です。
    </p>
</div>


<div id="top-contents" class="mt80"> <!-- #top-contents -->

    <div id="accent-4" style="margin-bottom: 9px;"></div>
    <h3 id="main-title">お知らせ<span class="mini-text">-NEWS-</span></h3>
    <a href="http://www.herz-bag.jp/blog/" target="_blank"><h3 class="title-text-right"><img src="images/common/arrow.png" alt="">お知らせ一覧</h3></a>
    <div id="accent-4" style="margin-top: 6px;"></div>

    <div class="mt30">
        <div class="slider1">
            <div class="slide">
                <a href=""><img class="slider-text" src="images/common/thumb-mid-1.png" alt=""/></a>
                <div class="tag"><a href=""><img class="mid-slider" src="images/common/mid/hashtag-01.png" alt=""/>2015/09/17</a></div>
                <p class="title">Early Autumn un Nagoya</p>
            </div>
            <div class="slide">
                <a href=""><img class="slider-text" src="images/common/thumb-mid-2.png" alt=""/></a>
                <div class="tag"><a href=""><img class="mid-slider" src="images/common/mid/hashtag-02.png" alt=""/>2015/09/14</a></div>
                <p class="title">A’-3ダイアログ ブリーフケース</p>
            </div>

            <div class="slide">
                <a href=""><img class="slider-text" src="images/common/thumb-mid-3.png" alt=""/></a>
                <div class="tag"><a href=""><img class="mid-slider" src="images/common/mid/hashtag-03.png" alt=""/>2015/09/17</a></div>
                <p class="title">創業者の近藤、水かきマチの3wayバッグを試作中</p>
            </div>

            <div class="slide">
                <a href=""><img class="slider-text" src="images/common/thumb-mid-4.png" alt=""/></a>
                <div class="tag"><a href=""><img class="mid-slider" src="images/common/mid/hashtag-04.png" alt=""/>2015/09/17</a></div>
                <p class="title">サンプル鞄が続々と byFACTORY SHOP</p>
            </div>
        </div>
    </div>

    <div id="accent-4" style="margin-bottom: 9px; margin-top:39px;"></div>
    <h3 id="main-title">新作アイテム<span class="mini-text">-NEW ITEM-</span></h3>
    <a href="http://www.herz-bag.jp/webshop/products/list2.html" target="_blank"><h3 class="title-text-right"><img src="images/common/arrow.png" alt="">新作アイテム一覧</h3></a>
    <div id="accent-4" style="margin-top: 6px;"></div>

    <div id="new-item-wrapper" class="mt20"> <!-- #new-item-wrapper -->
        <div class="left" style="margin-right: 3px;">
            <p class="border-line">
                新作は日々鞄を制作している <br/>
                作り手自身がデザインしています。<br/><br/>
                枠にとらわれない自由な発想で作られる <br/>
                カバンや小物たちはどれも個性的でありなが <br/>
                らも、HERZらしい雰囲気をもっています。<br/>
            </p>
            <img class="" style="opacity: 0.7; margin-top: 25px;" src="images/common/mid/img-mid2.png" alt=""/>
        </div>
        <div class="right mr30">
            <a href=""><img src="images/common/mid/mid2-img1.png" alt="Thumb" /></a>
            <a href=""><p class="font-bold" style="margin-top: 5px;margin-bottom: 31px;"><u>Organ新作：肩掛けファスナートート</u></p></a>

            <a href=""><img src="images/common/mid/mid2-img2.png" alt="Thumb" /></a>
            <a href=""><p  class="font-bold" style="margin-top: 5px;"><u>Organ新作：ブリーフケース</u></p></a>
        </div>
        <div class="right">
            <a href=""><img src="images/common/mid/mid2-img3.png" alt="Thumb" /></a>
            <a href=""><p class="font-bold" style="margin-top: 5px;margin-bottom: 31px;"><u>縦型ペンケース</u></p></a>

            <a href=""><img src="images/common/mid/mid2-img4.png" alt="Thumb" /></a>
            <a href=""><p class="font-bold" style="margin-top: 5px;"><u>2本手ファスナービジネスバッグ</u></p></a>
        </div>
    </div> <!-- // #new-item-wrapper -->

    <div id="accent-4" style="margin-bottom: 9px; margin-top: 63px;"></div>
    <h3 id="main-title">定番アイテム<span class="mini-text">-STANDARD ITEM-</span></h3>
    <div id="accent-4" style="margin-top: 6px;"></div>

    <div id="standard-item-wrapper" style="margin-top: 27px;"> <!-- #standard-item-wrapper -->
        <div id="box-wrapper" class="mb20">
            <div class="box">
                <img src="images/common/standardItem/std-logo.png" alt="logo" style="margin-top: -6px;">
                <p style="margin-top: 13px;">1973年の創業当時から 「丈夫で長く使える鞄」をテーマに武骨な革製品を400型以上、変わらずに作り続けています。</p>
                <div class="mt20"><a href=""><img src="images/common/standardItem/std-arrow.png" alt="" />オンラインショップ</a></div>
            </div>
            <div class="box mr30 ml30">
                <a href=""><img src="images/common/standardItem/std-item1.png" alt="Thumb" /></a>
                <a href=""><p class="title">総かぶせ・ヨコ型2wayビジネスバッグ(BC-13)</p></a>
            </div>
            <div class="box">
                <a href=""><img src="images/common/standardItem/std-item2.png" alt="Thumb" /></a>
                <a href=""><p class="title">ファスナー長財布(WL-58)</p></a>
            </div>
        </div>

        <div id="accent-5"></div>

        <div id="box-wrapper" class="mt25">
            <div class="box">
                <img src="images/common/standardItem/std-organ-logo.png" alt="logo">
                <p style="margin-top: 17px;">イタリアンレザーを使用し「道具としての鞄」をよりシンプルに、女性にも気兼ねなく使える革製品を作り出しています。</p>
                <div class="mt20"><a href=""><img src="images/common/standardItem/std-arrow.png" alt="" />Organモデル販売ページ </a></div>
                <div style="margin-top: 8px;"><a href=""><img src="images/common/standardItem/std-arrow.png" alt="" />Organブランドサイト<img src="images/common/standardItem/sml-icon.png" alt="icon" /></a></div>
            </div>
            <div class="box mr30 ml30">
                <a href=""><img src="images/common/standardItem/std-item3.png" alt="Thumb" /></a>
                <a href=""><p class="title">ショルダーバッグ(G-19)</p></a>
            </div>
            <div class="box">
                <a href=""><img src="images/common/standardItem/std-item4.png" alt="Thumb" /></a>
                <a href=""><p class="title">マチ付き二つ折り財布(GS-16)</p></a>
            </div>
        </div>
    </div> <!-- // #standard-item-wrapper -->

    <div id="accent-4" class="mt70" style="margin-bottom: 9px;"></div>
    <h3 id="main-title">革鞄と革小物<span class="mini-text">-CATEGORY-</span></h3>
    <a href="https://www.herz-bag.jp/webshop/" target="_blank"><h3 class="title-text-right"><img src="images/common/arrow.png" alt=""/>オンラインショップ</h3></a>
    <div id="accent-4" style="margin-top: 6px;"></div>

    <ul id="category-wrapper">
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list9.html" target="_blank"><img src="images/common/categories/item1.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list9.html" target="_blank"><p class="mt5">クラシックバッグ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list10.html" target="_blank"><img src="images/common/categories/item2.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list10.html" target="_blank"><p class="mt5">ビジネスバッグ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list11.html" target="_blank"><img src="images/common/categories/item3.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list11.html" target="_blank"><p class="mt5">ショルダーバッグ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list12.html" target="_blank"><img src="images/common/categories/item4.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list12.html" target="_blank"><p class="mt5">リュック</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list13.html" target="_blank"><img src="images/common/categories/item5.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list13.html" target="_blank"><p class="mt5">3wayバッグ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list14.html" target="_blank"><img src="images/common/categories/item6.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list14.html" target="_blank"><p class="mt5">トートバッグ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list15.html" target="_blank"><img src="images/common/categories/item7.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list15.html" target="_blank"><p class="mt5">ボストンバッグ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list16.html" target="_blank"><img src="images/common/categories/item8.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list16.html" target="_blank"><p class="mt5">ベルトポーチ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list17.html" target="_blank"><img src="images/common/categories/item9.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list17.html" target="_blank"><p class="mt5">レディースバッグ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list18.html" target="_blank"><img src="images/common/categories/item10.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list18.html" target="_blank"><p class="mt5">ボディバッグ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list19.html" target="_blank"><img src="images/common/categories/item11.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list19.html" target="_blank"><p class="mt5">セカンドバッグ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list20.html" target="_blank"><img src="images/common/categories/item12.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list20.html" target="_blank"><p class="mt5">カメラバッグ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list21.html" target="_blank"><img src="images/common/categories/item13.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list21.html" target="_blank"><p class="mt5">ランドセル</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list22.html" target="_blank"><img src="images/common/categories/item14.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list22.html" target="_blank"><p class="mt5">革財布</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list23.html" target="_blank"><img src="images/common/categories/item15.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list23.html" target="_blank"><p class="mt5">ステーショナリー</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list24.html" target="_blank"><img src="images/common/categories/item16.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list24.html" target="_blank"><p class="mt5">名刺・カード入れ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list25.html" target="_blank"><img src="images/common/categories/item17.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list25.html" target="_blank"><p class="mt5">ポーチ</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list26.html" target="_blank"><img src="images/common/categories/item18.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list26.html" target="_blank"><p class="mt5">パスケース</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list27.html" target="_blank"><img src="images/common/categories/item19.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list27.html" target="_blank"><p class="mt5">革小物</p></a>
        </li>
        <li>
            <a href="http://www.herz-bag.jp/webshop/products/list28.html" target="_blank"><img src="images/common/categories/item20.png" alt="Thumb" /></a>
            <a href="http://www.herz-bag.jp/webshop/products/list28.html" target="_blank"><p class="mt5">ストラップ・肩当て</p></a>
        </li>
    </ul>

    <div id="accent-4" style="margin-bottom: 9px; margin-top: 53px;"></div>
    <h3 id="main-title">特集<span class="mini-text">-SPECIAL CONTENT-</span></h3>
    <a href="http://www.herz-bag.jp/◯◯◯" target="_blank"><h3 class="title-text-right"><img src="images/common/arrow.png" alt="" />特集一覧</h3></a>
    <div id="accent-4" style="margin-top: 6px;"></div>

    <div id="box-wrapper" style="margin-bottom: 67px;">
        <div class="ml10 mr30 box2" style="margin-top: 42px;">
            <div class="image-box">
                <img class="spcl-tape-left" src="images/common/special-content/sp-tape-left.png" alt="" />
                <img class="spcl-tape-right" src="images/common/special-content/sp-tape-right.png" alt="" />
                <img src="images/common/special-content/spcl-01.png" alt=""/>
            </div>
            <a href=""><p>リュックの試作 ～定番化を目指して 作り手：村松～</p></a>
        </div>


        <div class="box2 mr30" style="margin-top: 42px;">
            <div class="image-box">
                <img class="spcl-tape-left" src="images/common/special-content/sp-tape-left.png" alt="" />
                <img class="spcl-tape-right" src="images/common/special-content/sp-tape-right.png" alt="" />
                <img src="images/common/special-content/spcl-02.png" alt=""/>
            </div>
            <a href="">HERZなんでも実験室:雨に濡れてしまった！気になる雨染みどうなるの？</a>
        </div>

        <div class="box2" style="margin-top: 42px;">
            <div class="image-box">
                <img class="spcl-tape-left" src="images/common/special-content/sp-tape-left.png" alt="" />
                <img class="spcl-tape-right" src="images/common/special-content/sp-tape-right.png" alt="" />
                <img src="images/common/special-content/spcl-03.png" alt=""/>
            </div>
            <a href="">創業者とカバンを作ろう：第2弾試作編</a>
        </div>
    </div>

</div> <!-- // #top-contents -->

<div id="accent-5"></div>
<div id="banner2">
    <h4 class="title" style="padding-top: 55px;">HERZとはドイツ語でハート(心)</h4>
    <p class="sub-title" style="margin-top: 25px; margin-bottom: 33px;">HERZ THE HEART</p>
    <p class="text">
        HERZ(ヘルツ)では「一生使える」をテーマに、<br/>
        流行に流されない味わいのある鞄を作り続けています。<br/>
        <br/>
        革の裁断から縫製まで全て自分たちの手で作り上げる <br/>
        本当のMADE IN JAPANにこだわり続けて40年以上。<br/>
        革鞄と共に過ごす、楽しさ。喜び。<br/>
        それをお伝えすることが、HERZの願いです。<br/>
    </p>
    <p class="text" style="margin-top: 41px;"><a href="http://www.herz-bag.jp/aboutus/" target="_blank" style="text-decoration: underline"><img src="images/common/banner-arrow.png"
                                                                                         alt="">HERZについて詳しく</a></p>
</div>
<div id="accent-5"></div>

<div id="top-contents" style="margin-bottom: 63px; margin-top: 47px;"> <!-- #top-contents -->

    <div id="box-wrapper"> <!-- #box-wrapper -->
        <div class="box heart">
            <div id="accent-6" style="margin-bottom: 11px;"></div>
            <h4 class="heart-title align-center">HERZの鞄</h4>
            <div id="accent-6" style="margin-top: 7px;"></div>
            <a href="http://www.herz-bag.jp/aboutus/products/" target="_blank"><img  src="images/common/herzhaert/hrt-img1.png" alt="" class="mt30" /></a>
            <p style="margin-top: 8px;">選んでくださった皆さんに出来る限り永く使って欲しいからHERZの鞄はとにかく「丈夫であること」を大切に作っています。</p>
            <p class="text-center" style="margin-top: 17px;"><a href="http://www.herz-bag.jp/aboutus/products/" target="_blank"><img src="images/common/herzhaert/std-arrow.png" alt="" />HERZの鞄</a></p>

        </div>
        <div class="box mr30 ml30">
            <div class="box heart">
                <div id="accent-6" style="margin-bottom: 11px;"></div>
                <h4 class="heart-title align-center">HERZの歴史</h4>
                <div id="accent-6" style="margin-top: 7px;"></div>
                <a href="http://www.herz-bag.jp/aboutus/history/" target="_blank"><img  src="images/common/herzhaert/hrt-img2.png" alt="" class="mt30" /></a>
                <p style="margin-top: 8px;">1973年創業のHERZ。そもそものはじまりは、創業者でもある近藤 晃理の、「作りたい！ 楽しい！」でした。</p>
                <p class="text-center" style="margin-top: 17px;"><a href="http://www.herz-bag.jp/aboutus/history/" target="_blank"><img src="images/common/herzhaert/std-arrow.png" alt="" />HERZの歴史</a></p>

            </div>
        </div>
        <div class="box">
            <div class="box heart">
                <div id="accent-6" style="margin-bottom: 11px;"></div>
                <h4 class="heart-title align-center">工房について</h4>
                <div id="accent-6" style="margin-top: 7px;"></div>
                <a href="http://www.herz-bag.jp/factory/" target="_blank"><img  src="images/common/herzhaert/hrt-img3.png" alt="" class="mt30" /></a>
                <p style="margin-top: 8px;">HERZでは、分業制ではなく、革の裁断を除く、工程の最初から最後までを一人の作り手が一つの鞄を作っています。</p>
                <p class="text-center" style="margin-top: 17px;"><a href="http://www.herz-bag.jp/factory/" target="_blank"><img src="images/common/herzhaert/std-arrow.png" alt="" />工房について</a></p>
            </div>
        </div>
    </div> <!-- // #box-wrapper -->

</div> <!-- // #top-contents -->


<div id="accent"></div>
<div id="banner3">
    <h4 class="title">作る場所と売る場所が一緒の空間</h4>
    <p class="text" style="margin-top: 28px;">
        HERZの直営店舗は工房兼お店。<br/>
        自分たちの手でお届けすることを大切にしているためです。<br/>
        作り手一同、ご来店をお待ちしています。<br/>
    </p>
    <p class="text" style="margin-top: 31px;"><a href="http://www.herz-bag.jp/aboutus/" target="_blank" style="text-decoration: underline;" ><img src="images/common/banner-arrow.png"
                                                                                         alt="">直営店一覧</a></p>
</div>
<div id="accent"></div>




<div id="top-contents"> <!-- #top-contents -->
    <div style="margin-top: 47px;">
        <div class="slider2">
            <div class="slide">
                <a href=""><img class="slider-text" src="images/common/organ/oran-img-02.png" alt=""/></a>
                <a href=""><p class="title">本店</p></a>
                <p class="description">東京都渋谷区神宮前5-46-16 イルチェン トロセレーノB1F</p>
                <a href=""><div class="phone">03-6427-2412</div></a>
                <a href=""><div class="schedule">11:00-19:00</div></a>
                <a href=""><div class="open">水曜日</div></a>
            </div>

            <div class="slide">
                <a href=""><img class="slider-text" src="images/common/organ/oran-img-02.png" alt=""/></a>
                <a href=""><p class="title">FACTORY SHOP 2</p></a>
                <p class="description">東京都渋谷区渋谷2-12-8 中村ビル1F</p>
                <a href=""><div class="phone">03-6427-2412</div></a>
                <a href=""><div class="schedule">11:00-19:00</div></a>
                <a href=""><div class="open">水曜日</div></a>
            </div>

            <div class="slide">
                <a href=""><img class="slider-text" src="images/common/organ/oran-img-03.png" alt=""/></a>
                <a href=""><p class="title">Organ</p></a>
                <p class="description">東京都渋谷区渋谷2-12-6 三田ビル1F</p>
                <a href=""><div class="phone">03-6427-2412</div></a>
                <a href=""><div class="schedule">11:00-19:00</div></a>
                <a href=""><div class="open">水曜日</div></a>
            </div>

            <div class="slide">
                <a href=""><img class="slider-text" src="images/common/organ/oran-img-04.png" alt=""/></a>
                <a href=""><p class="title mt10">RESO.</p></a>
                <p class="description">東京都渋谷区渋谷2-7-12</p>
                <a href=""><div class="phone">03-6427-2412</div></a>
                <a href=""><div class="schedule">11:00-19:00</div></a>
                <a href=""><div class="open">水曜日</div></a>
            </div>

        </div>
    </div>

    <div class="organ-brand" style="margin-bottom: 74px; margin-top: 67px;">
        <a href="http://www.organ-leather.com/" target="_blank"><img src="images/common/organ/organ-brand-site2.png" alt=""></a>
    </div>
</div> <!-- // #top-contents -->

<div id="accent-7"></div>
<div id="banner4">
    <h4 class="title">質の良い革をそのまま贅沢に使う</h4>
    <p class="text" style="margin-top: 42px;">
        HERZでいう質の良い革とは、見た目が均一で綺麗な革ではありません。<br/>
        むしろその逆です。革本来のキズやシワを隠す表面加工を最小限にとどめ、<br/>
        植物タンニンで時間をかけて丹精になめしたオイルレザーを使っています。<br/>
        使い込み、時を経るごとに色に深みと艶が増し、<br/>
        使う人ならではの豊かで味わい深い表情を見せてくれます。<br/>
    </p>
    <p class="text" style="margin-top: 43px;"><a href="http://www.herz-bag.jp/aboutus/" target="_blank" style="text-decoration: underline"><img src="images/common/banner-arrow.png"
                                                                                         alt="">素材について詳しく見る</a></p>
</div>
<div id="accent-7"></div>

<div id="top-contents"> <!-- #top-contents -->

    <div id="final-wrapper" class="box-fix" style="margin-top: 89px;"> <!-- #final-wrapper -->
        <div class="left">
            <a href=""><img  src="images/common/last/last-img1.png" alt="" /></a>
        </div>
        <div class="right">
            <div id="accent-8" style="margin-bottom:18px;"></div>
            <h2 id="main-title">経年変化を楽しむ<span class="mini-text">-ENJOY THE AGING-</span></h2>
            <div id="accent-8" style="margin-top: 13px;"></div>
            <p style="margin-top: 16px;">お届けしたカバンは未完成です、と聞いたら驚かれるでしょうか。私達がお作りするのは80%まで、残りの20％はお客様に使っていただく事によって仕上げられるのです。</p>
            <div style="margin-top: 17px;"><a href="" style="text-decoration: underline;"><img src="images/common/last/last-arrow.png" alt="">経年変化を楽しむ</a></div>
        </div>
    </div> <!-- // #final-wrapper -->

    <div id="final-wrapper" class="box-fix" style="margin-top: 78px;"> <!-- #final-wrapper -->
        <div class="left">
            <div id="accent-8" style="margin-bottom:18px;"></div>
            <h2 id="main-title">お手入れ方法<span class="mini-text">-CARE-</span></h2>
            <div id="accent-8" style="margin-top: 13px;"></div>
            <p style="margin-top: 16px;">皮革製品のお手入れは少し面倒です。でもほんの少し手を掛けてあげる事によって、愛着が湧き、カバンの寿命はぐっと延びます。日常でのお手入れが、味のある革を育てる第一歩となるのです。お手入れしながら長く大切に使い続けていくこと。これこそが本当の贅沢だとHERZは考えています。</p>
         <div style="margin-top: 17px;"><a href="" style="text-decoration: underline;"><img src="images/common/last/last-arrow.png" alt="">経年変化を楽しむ</a></div>
        </div>
        <div class="right">
            <a href="http://www.herz-bag.jp/webshop/products/list4.html" target="_blank"><img  src="images/common/last/last-img2.png" alt="" /></a>
        </div>
    </div> <!-- // #final-wrapper -->

    <div id="final-wrapper" class="box-fix mb100" style="margin-top: 78px;"> <!-- #final-wrapper -->
        <div class="left">
            <a href=""><img  src="images/common/last/last-img3.png" alt="" /></a>
        </div>
        <div class="right">
            <div id="accent-8" style="margin-bottom:18px;"></div>
            <h2 id="main-title">修理について<span class="mini-text">-REPAIR-</span></h2>
            <div id="accent-8" style="margin-top: 13px;"></div>
            <p style="margin-top: 16px;">丈夫で長く使える＝壊れないわけではありません。永くお使い頂中で、どうしても修理は必要になります。その時、お使いの鞄を拝見し、その状態にふさわしい方法で作り手が丁寧に修理いたします。HERZの鞄はお使いいただく限り、出来るだけの修理・加工をいたします。</p>
         <div style="margin-top: 17px;"><a href="" style="text-decoration: underline;"><img src="images/common/last/last-arrow.png" alt="">経年変化を楽しむ</a></div>
        </div>
    </div> <!-- // #final-wrapper -->

</div> <!-- // #top-contents -->