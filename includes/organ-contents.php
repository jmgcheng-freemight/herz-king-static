<div id="organ-wrapper">
 
 <div id="section-wrapper">
  <div id="accent-3" style="margin-bottom: 16px;"></div>
  <h3 id="main-title">Organ イタリアンレザー<span class="mini-text">-ITARIAN LEATHER-</span></h3>
  <div id="accent-3" style="margin-top: 11px;"></div>
  <div id="image-wrapper">
   <img src="images/organ/organ-cover.png" alt="">
  </div>
  <div class="organ-txt-wrapper" style="margin-top: 24px;">
   <p class="organ-txt">
   Organモデルで使用されている革は植物タンニンなめしのイタリアンレザーを使用しています。トラ、色ムラなどの個体差をあえて隠さず、革素材本来の魅力を活かした仕上げです。多くのタンナーが存在する革の街､トスカーナ州サンタクローチェで培われた伝統的な製法で作られており、加工に時間がかかり脂が浸透しにくい反面、使い込んだ時に独特の色艶がでることと、一旦加脂したオイルが抜けにくいという特徴があります。<br>
   <br>
   Organでは、鞄のデザインによって、数種類のイタリア革を使い分けています。
   </p>
  </div>
 </div>

 <div id="section-wrapper">
  <h3 id="main-title">ミネルバボックス</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
  <div id="image-wrapper3">
   <img src="images/organ/organ-image00.png" alt="">
  </div>
  <div class="organ-txt-wrapper" style="margin-top: 24px;">
   <p class="organ-txt">
   ドライドラムで革をほぐすことで出来るシボと呼ばれる皺と柔らかさが特徴。<br>
   部位による革質差により、それぞれシボの入り具合が異なり、豊かな表情が生まれます。
  </p>
  </div>

  <div class="organ-related box-fix"> <!-- .organ-related -->
   <p class="related-title">関連リンク</p>
   <div class="thumb-wrapper">
    <img src="images/organ/organ-thumb00.png" alt="">
   </div>
    <div class="link-wrapper"><img src="images/common/arrow.png" alt=""><a href="">ミネルバボックスについて詳しく見る</a></div>
  </div> <!-- // .organ-related -->
 </div> <!-- // #section-wrapper -->

 <div id="section-wrapper">
  <h3 id="main-title">ミネルバリスシオ</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
  <div id="image-wrapper3">
   <img src="images/organ/organ-image01.png" alt="">
  </div>
  <div class="organ-txt-wrapper" style="margin-top: 24px;">
   <p class="organ-txt">
   リスシオとは、イタリア語で滑らかという意味があり、革の表情は植物タンニンなめし特有の温かみ、透明感を感じさせるスムースな仕上がりになっています。
  </p>
  </div>

  <div class="organ-related box-fix"> <!-- .organ-related -->
   <p class="related-title">関連リンク</p>
   <div class="thumb-wrapper">
    <img src="images/organ/organ-thumb01.png" alt="">
   </div>
    <div class="link-wrapper"><img src="images/common/arrow.png" alt=""><a href="">ミネルバリスシオについて詳しく見る</a></div>
  </div> <!-- // .organ-related -->
 </div> <!-- // #section-wrapper -->

 <div id="section-wrapper">
  <h3 id="main-title">リバース</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
  <div id="image-wrapper3">
   <img src="images/organ/organ-image02.png" alt="">
  </div>
  <div class="organ-txt-wrapper" style="margin-top: 24px;">
   <p class="organ-txt">
   床面に加工処理を施している革で、通常は吟面を裏側にして使います。吟面は表面加工を一切していないため、キズやムラが多く入っていますが、Organではそれらを味として捉え、そのままの風合いを活かして使用しています。
  </p>
  </div>

  <div class="organ-related box-fix"> <!-- .organ-related -->
   <p class="related-title">関連リンク</p>
   <div class="thumb-wrapper">
    <img src="images/organ/organ-thumb02.png" alt="">
   </div>
    <div class="link-wrapper"><img src="images/common/arrow.png" alt=""><a href="">リバースについて詳しく見る</a></div>
  </div> <!-- // .organ-related -->
 </div> <!-- // #section-wrapper -->

 <div id="section-wrapper">
  <h3 id="main-title">ユーフラテ</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
  <div id="image-wrapper3">
   <img src="images/organ/organ-image03.png" alt="">
  </div>
  <div class="organ-txt-wrapper" style="margin-top: 24px;">
   <p class="organ-txt">
   使い込まれた革のような独特の表情が特徴。<br>
「ウェットバック」と呼ばれる水を入れたドラムの中で繊維をほぐしながらシボを出し、乾燥後に銀面をガラス玉で擦る「グレージング加工」を施すことで艶を出しています。
  </p>
  </div>

  <div class="organ-related box-fix"> <!-- .organ-related -->
   <p class="related-title">関連リンク</p>
   <div class="thumb-wrapper">
    <img src="images/organ/organ-thumb03.png" alt="">
   </div>
    <div class="link-wrapper"><img src="images/common/arrow.png" alt=""><a href="">ユーフラテについて詳しく見る</a></div>
  </div> <!-- // .organ-related -->
 </div> <!-- // #section-wrapper -->
 
 <div id="section-wrapper">
  <h3 id="main-title">ホリデイ</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
  <div id="image-wrapper3">
   <img src="images/organ/organ-image04.png" alt="">
  </div>
  <div class="organ-txt-wrapper" style="margin-top: 24px;">
   <p class="organ-txt">
  柔らかく揉んで、自然に生まれるシボやシワの不規則さが革本来の個性を感じる革。<br>
エイジングのスパンが長く、ゆっくりと変化を楽しめるのが特徴です。
  </p>
  </div>

  <div class="organ-related box-fix"> <!-- .organ-related -->
   <p class="related-title">関連リンク</p>
   <div class="thumb-wrapper">
    <img src="images/organ/organ-thumb04.png" alt="">
   </div>
    <div class="link-wrapper"><img src="images/common/arrow.png" alt=""><a href="">ホリデイについて詳しく見る</a></div>
  </div> <!-- // .organ-related -->
 </div> <!-- // #section-wrapper -->

 <div id="section-wrapper">
  <h3 id="main-title">ダコタ</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
  <div id="image-wrapper3">
   <img src="images/organ/organ-image05.png" alt="">
  </div>
  <div class="organ-txt-wrapper" style="margin-top: 24px;">
   <p class="organ-txt">
  強度が高く厚みのある力強さ、平滑で張りのあるテクスチャーが特徴。<br>
こちらもエイジングのスパンが長く、上品な風合いをゆっくり味わえます。
  </p>
  </div>

  <div class="organ-related box-fix"> <!-- .organ-related -->
   <p class="related-title">関連リンク</p>
   <div class="thumb-wrapper">
    <img src="images/organ/organ-thumb05.png" alt="">
   </div>
    <div class="link-wrapper"><img src="images/common/arrow.png" alt=""><a href="">ダコタについて詳しく見る</a></div>
  </div> <!-- // .organ-related -->
 </div> <!-- // #section-wrapper -->

 <div id="section-wrapper">
  <h3 id="main-title">イタリアンレザーのお手入れ・色移りについて</h3>
  <div id="accent-3" style="margin-top: 9px;"></div>
  <div class="organ-txt-wrapper" style="margin-top: 21px;">
   <p class="organ-txt">
  お手入れについては、革に上質な油分が十分に含まれておりますので、空拭きのみで問題ありません。表面に大きな加工をしていないので、小さなキズやシワが隠れにくく、爪傷が付きやすいという特徴がありますが、指で軽く揉み込んであげるとキズが目立ちにくくなります。<br>
  <br>
天然の染料を使用し、革の表面加工を抑えていますので、革の表面や内側の摩擦や水ぬれによる衣類等への色移りがあります。特に薄い色の衣服でのご使用はご注意ください。
  </p>
  </div>
  <a href="" style="margin-top: 41px;">Organモデル販売ページ</a>
  <img src="images/organ/organ-btm-image.png" alt="" style="display: block; margin-top: 45px;">
 </div> <!-- // #section-wrapper -->
 

</div> <!-- // #organ-wrapper -->