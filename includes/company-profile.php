<div class="middle-content-container align-left" >
	<div class="mid-row-1">
		<img src="images/sidebar-slice/mid-line-hr.png" alt=""/>
		<h3 class="comp-profile-text-title">会社概要<span class="mini-text">-PROFILE-</span></h3>
		<!-- <a href="" target="_blank" ><h3 class="float-right"><img src="images/arrow.png" alt=""></h3></a> -->
		<img src="images/sidebar-slice/mid-line-hr.png" alt="" />
	
	<div class="comp-profile-img">
	<img src="images/company-profile/comp-profile-img1.png" alt="">		
	</div>

	</div>

	<div class="comp-profile-row2">
		<h3 class="comp-profile-text-title2">ヘルツの製品<span class="mini-text"></span></h3>
		<img src="images/sidebar-slice/mid-line-hr.png" alt="" />
		
		<div class="comp-text-1"><h3 class="comp-profile-text-title">オリジナルであること 丈夫であること<br>
														  飽きのこないオーソドックスなデザインであること</h3></div>

		<div class="comp-text-2">
			<p>ヘルツ(HERZ)は、渋谷と博多、大阪、仙台に工房、直営店を構える鞄職人による、日本の手作り革鞄(<br>
				かばん)工房です。革の裁断から縫製まで、一点一点全て自分達の手で作り上げています。本当のmade<br>
				 in japan(日本製)にこだわり続けて40年以上。革鞄(カバン)と共に過ごす、楽しさ。喜び。それをお伝<br>
				 えすることが、HERZの願いです。</p>
		</div>												  

	
	</div>














			<div class="comp-profile-row3">

			<!-- 	<table>
			<tr>
			    <th>会社名</th>
			    <td>株式会社　ヘルツ</td>
			  </tr>

			  <tr>
			    <th>代表者</th>
			    <td>代表取締役　野口 裕明</td>
			  </tr>
			  <tr>
			    <th>所在地本社</th>
			    <td>東京都渋谷区神宮前5-46-16</td>
			  </tr>
			  <tr>
			    <th>電話</th>
			    <td>03-3406-1510（代）</td>
			  </tr>
			  <tr>
			    <th>FAX</th>
			    <td>03-3406-1520</td>
			  </tr>

			  <tr>
			    <th>E-mail</th>
			    <td>info@herz-bag.jp</td>
			  </tr>
			  
			  <tr>
			    <th>直営店および工房</th>
			    <td>本店 / 東京都渋谷区神宮前5-46-16<br>
			    	FACTORY SHOP(渋谷工房) / 東京都渋谷区渋谷2-12-8<br>
					Organ / 東京都渋谷区渋谷2-12-6<br>
					RESO. / 東京都渋谷区渋谷2-7-12<br>
					大阪店 / 大阪府大阪市西区南堀江2-4-4<br>
					仙台店 / 宮城県仙台市青葉区本町2-10-33<br>
					博多工房 / 福岡県福岡市博多区博多駅前3-16-10<br>
			    </td>
			  </tr>

			  <tr>
			    <th>資本金</th>
			    <td>1,000万円</td>
			  </tr>

			   <tr>
			    <th>創業</th>
			    <td>1973年10月</td>
			  </tr>

			   <tr>
			    <th>設立</th>
			    <td>1987年12月</td>
			  </tr>

			   <tr>
			    <th>業種</th>
			    <td>企画製造販売業</td>
			  </tr>

			    <tr>
			    <th>取扱品目</th>
			    <td>皮革製品（鞄、バッグ、生活雑貨）</td>
			  </tr>

			</table> -->



				<!--<div class="comp-info">
				<div class="comp-info1"><p class="comp-info-right float-left">会社名</p><p class="comp-info-left">株式会社　ヘルツ</p></div><br><br>
				<div class="comp-info1" style="margin-top: -69px;"><p class="comp-info-right float-left">代表者</p><p class="comp-info-left">代表取締役　野口 裕明</p></div><br><br>
				<div class="comp-info1" style="margin-top: -69px;"><p class="comp-info-right float-left">所在地本社</p><p class="comp-info-left">東京都渋谷区神宮前5-46-16</p></div><br><br>
				<div class="comp-info1" style="margin-top: -69px;"><p class="comp-info-right float-left">電話</p><p class="comp-info-left">03-3406-1510（代）</p></div><br><br>
				<div class="comp-info1" style="margin-top: -71px;"><p class="comp-info-right float-left">FAX</p><p class="comp-info-left">03-3406-1520</p></div><br><br>
				<div class="comp-info1" style="margin-top: -71px;"><p class="comp-info-right float-left">E-mail</p><p class="comp-info-left">info@herz-bag.jp</p></div><br><br>
				<div class="comp-info1" style="margin-top: -71px;"><p class="comp-info-right float-left">直営店および工房</p><p class="comp-info-left" style="line-height: 46px;">本店 / 東京都渋谷区神宮前5-46-16<br>
																						    	FACTORY SHOP(渋谷工房) / 東京都渋谷区渋谷2-12-8<br>
																								Organ / 東京都渋谷区渋谷2-12-6<br>
																								RESO. / 東京都渋谷区渋谷2-7-12<br>
																								大阪店 / 大阪府大阪市西区南堀江2-4-4<br>
																								仙台店 / 宮城県仙台市青葉区本町2-10-33<br>
																								博多工房 / 福岡県福岡市博多区博多駅前3-16-10<br></p></div><br><br>


				<div class="comp-info1" style="margin-top: -69px;"><p class="comp-info-right float-left">資本金</p><p class="comp-info-left">1,000万円</p></div><br><br>
				<div class="comp-info1" style="margin-top: -69px;"><p class="comp-info-right float-left">創業</p><p class="comp-info-left">1973年10月</p></div><br><br>
				<div class="comp-info1" style="margin-top: -69px;"><p class="comp-info-right float-left">設立</p><p class="comp-info-left">1987年12月</p></div><br><br>
				<div class="comp-info1" style="margin-top: -69px;"><p class="comp-info-right float-left">業種</p><p class="comp-info-left">企画製造販売業</p></div><br><br>
				<div class="comp-info1" style="margin-top: -69px;border-bottom: 1px dotted #5C2700;height: 46px;"><p class="comp-info-right float-left">取扱品目</p><p class="comp-info-left">皮革製品（鞄、バッグ、生活雑貨）</p></div><br><br>
				</div>





	 

			</div>-->
			<div id="comp-table">
				<div class="table-1">
					<div class="table-row-1">
						<div class="table-cell table-cell-left">会社名</div>
						<div class="table-cell table-cell-right">株式会社　ヘルツ</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">代表者</div>
						<div class="table-cell table-cell-right">代表取締役　野口 裕明</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">所在地本社</div>
						<div class="table-cell table-cell-right">東京都渋谷区神宮前5-46-16</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">電話</div>
						<div class="table-cell table-cell-right">03-3406-1510（代）</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">FAX</div>
						<div class="table-cell table-cell-right">03-3406-1520</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">E-mail</div>
						<div class="table-cell table-cell-right">info@herz-bag.jp</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">直営店および工房</div>
						<div class="table-cell table-cell-right">本店 / 東京都渋谷区神宮前5-46-16<br>
				    	FACTORY SHOP(渋谷工房) / 東京都渋谷区渋谷2-12-8<br>
						Organ / 東京都渋谷区渋谷2-12-6<br>
						RESO. / 東京都渋谷区渋谷2-7-12<br>
						大阪店 / 大阪府大阪市西区南堀江2-4-4<br>
						仙台店 / 宮城県仙台市青葉区本町2-10-33<br>
						博多工房 / 福岡県福岡市博多区博多駅前3-16-10<br></div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">資本金</div>
						<div class="table-cell table-cell-right">1,000万円</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">創業</div>
						<div class="table-cell table-cell-right">1973年10月</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">設立</div>
						<div class="table-cell table-cell-right">1987年12月</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">業種</div>
						<div class="table-cell table-cell-right">企画製造販売業</div>
					</div>
					<div class="table-row-1">
						<div class="table-cell table-cell-left">取扱品目</div>
						<div class="table-cell table-cell-right">皮革製品（鞄、バッグ、生活雑貨）</div>
					</div>
				</div>
			</div>




</div>

