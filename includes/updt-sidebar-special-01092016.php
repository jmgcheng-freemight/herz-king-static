			<aside class="site-aside site-left-aside">
				
				
				<nav class="site-menu site-aside-menu-1 special-category">
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2>
							特集カテゴリー
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					<ul>
						<li class="li-item">
							<a class="anc link-3" href="#"><img src="images/updt-common/ico-folder-brown-2.png" />お店のこと</a>
						</li>
						<li class="li-item">
							<a class="anc link-3" href="#"><img src="images/updt-common/ico-folder-brown-2.png" />工房/ 作り手のこと</a>
						</li>
						<li class="li-item">
							<a class="anc link-3" href="#"><img src="images/updt-common/ico-folder-brown-2.png" />素材/ アフターケアのこと</a>
						</li>
						<li class="li-item">
							<a class="anc link-3" href="#"><img src="images/updt-common/ico-folder-brown-2.png" />商品/ イベントのこと</a>
						</li>
						<li class="li-item">
							<a class="anc link-3" href="#"><img src="images/updt-common/ico-folder-brown-2.png" />スタッフ愛用品</a>
						</li>
						<li class="li-item">
							<a class="anc link-3" href="#"><img src="images/updt-common/ico-folder-brown-2.png" />プレゼント/ 贈り物情報</a>
						</li>
						<li class="li-item">
							<a class="anc link-3" href="#"><img src="images/updt-common/ico-folder-brown-2.png" />その他</a>
						</li>
					</ul>
				</nav>
				

				<nav class="site-menu site-aside-menu-1 news-list">
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2>
							新着情報
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					<ul>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-1.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-menu-blog.png" />2015/09/17</h3>
							<a class="anc link-3" href="#">Early Autumn un Nagoya</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-2.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-menu-feature.png" />2015/09/14</h3>
							<a class="anc link-3" href="#">A’-3ダイアログ ブリーフケース</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-3.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-menu-press.png" />2015/09/17</h3>
							<a class="anc link-3" href="#">創業者の近藤、水かきマチの3wayバッグを試作中</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-4.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-menu-shopping.png" />2015/09/17</h3>
							<a class="anc link-3" href="#">サンプル鞄が続々と by FACTORY SHOP</a>
							<div class="clear-both"></div>
						</li>
					</ul>
				</nav>
				
				<nav class="site-menu site-aside-menu-1 category-list">
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2>
							革鞄と革小物
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					<ul>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_01.png" /></a>
							<a class="anc link-3" href="#">クラシックバッグ)</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_02.png" /></a>
							<a class="anc link-3" href="#">ビジネスバッグ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_03.png" /></a>
							<a class="anc link-3" href="#">ショルダーバッグ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_04.png" /></a>
							<a class="anc link-3" href="#">リュック</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_05.png" /></a>
							<a class="anc link-3" href="#">3wayバッグ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_06.png" /></a>
							<a class="anc link-3" href="#">トートバッグ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_07.png" /></a>
							<a class="anc link-3" href="#">ボストンバッグ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_08.png" /></a>
							<a class="anc link-3" href="#">ベルトポーチ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_09.png" /></a>
							<a class="anc link-3" href="#">レディースバッグ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_10.png" /></a>
							<a class="anc link-3" href="#">ボディバッグ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_11.png" /></a>
							<a class="anc link-3" href="#">セカンドバッグ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_12.png" /></a>
							<a class="anc link-3" href="#">カメラバッグ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_13.png" /></a>
							<a class="anc link-3" href="#">ランドセル</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_14.png" /></a>
							<a class="anc link-3" href="#">革財布</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_15.png" /></a>
							<a class="anc link-3" href="#">ステーショナリー</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_16.png" /></a>
							<a class="anc link-3" href="#">名刺・カード入れ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_17.png" /></a>
							<a class="anc link-3" href="#">ポーチ</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_18.png" /></a>
							<a class="anc link-3" href="#">パスケース</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_19.png" /></a>
							<a class="anc link-3" href="#">革小物</a>
							<div class="clear-both"></div>
						</li>
						<li class="li-item">
							<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_20.png" /></a>
							<a class="anc link-3" href="#">ストラップ・肩当て</a>
							<div class="clear-both"></div>
						</li>
					</ul>
				</nav>
				
				<nav class="site-menu site-aside-menu-1 special-content">
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2>
							特集
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					<ul>
						<li class="li-item">
							<div class="col col-1">
								<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png" />
								<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png" />
								<a class="anc-img" href="#">
									<img class="" src="images/updt-common/special-content-thumb-1.jpg" />
								</a>
							</div>
							<div class="col col-2">
								<a class="anc link-3" href="#">リュックの試作 ～定番化を目指して 作り手：村松～</a>
							</div>
							<div class="clear-both"></div>
						</li>
					</ul>
					<a class="anc link-2" href="#"><img src="images/updt-common/ico-arrow-right-black.png">特集一覧</a>
				</nav>
				
				<nav class="site-menu site-aside-menu-1 staff-favorite">
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2>
							スタッフ愛用品
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					<ul>
						<li class="li-item">
							<a class="anc-img" href="#">
								<img src="images/updt-common/staff-favorites-thumb-1.jpg" />
							</a>
							<a class="anc link-3" href="#">Lax Ruck（ラックスリュック）</a>
						</li>
					</ul>
					<a class="anc link-2" href="#"><img src="images/updt-common/ico-arrow-right-black.png">スタッフ愛用品一覧</a>
				</nav>
				
				<nav class="site-menu site-aside-menu-1 press-release">
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2>
							プレスリリース
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					<ul>
						<li class="li-item">
							<a class="anc-img" href="#">
								<img src="images/updt-common/press-release-thumb-1.jpg" />
							</a>
							<a class="anc link-3" href="#">雑誌掲載のお知らせ「nice things. 2015年11月号」</a>
						</li>
					</ul>
					<a class="anc link-2" href="#"><img src="images/updt-common/ico-arrow-right-black.png">プレスリリース一覧</a>
				</nav>
				
			</aside>