   <!--  -->
   <div id="sidebar-container" class="mb45">
   <div id="accent-2"></div>
    <h3 class="sidebar-title font-ryumin" style="margin-top:0px; margin-bottom:2px;">
		特集カテゴリー
	</h3>
    <div id="accent-2"></div>
     <ul class="sidebar-cont">
       <li style="margin-bottom:8px;">
		<a href=""><img src="images/common/brown-folder.png"/><span>お店のこと</span></a></li>
       <li style="margin-bottom:8px;">
		<a href=""><img src="images/common/brown-folder.png"/><span>工房/ 作り手のこと</span></a></li>
       <li style="margin-bottom:8px;">
		<a href=""><img src="images/common/brown-folder.png"/><span>素材/ アフターケアのこと</span></a></li>
       <li style="margin-bottom:8px;">
		<a href=""><img src="images/common/brown-folder.png"/><span>商品/ イベントのこと</span></a></li>
	   <li style="margin-bottom:8px;">
		<a href=""><img src="images/common/brown-folder.png"/><span>スタッフ愛用品</span></a></li>
	   <li style="margin-bottom:8px;">
		<a href=""><img src="images/common/brown-folder.png"/><span>プレゼント/ 贈り物情報</span></a></li>
	   <li style="margin-bottom:8px;">
		<a href=""><img src="images/common/brown-folder.png"/><span>その他</span></a></li>
     </ul>
   </div>
   <!--  -->

   <!-- What's New [新着情報] Section -->
   <div id="sidebar-container" class="mb45"> <!-- #sidebar-container -->
   <div id="accent-2"></div>
    <h3 class="sidebar-title font-ryumin" style="margin-top:0px; margin-bottom:2px;">
		新着情報</h3>
    <div id="accent-2"></div>
    
    
    <div id="sec1-wrapper">
     <div class="left">
      <a href=""><img src="images/common/sidebar-slice/s1.png" alt="" /></a>
     </div>
     <div class="right">
      <div class="tag-01" /><span class="date">2015/09/17</span></div>
      
      <a href="" target="_blank" class="title">Early Autumn in Nagoya</a>
     </div> 
    </div>

    <div id="sec1-wrapper">
     <div class="left">
      <a href=""><img src="images/common/sidebar-slice/s2.png" alt="" /></a>
     </div>
     <div class="right">
      <div class="tag-02" /><span class="date">2015/09/14</span></div>
      
      <a href="" target="_blank" class="title">A’-3ダイアログ ブリーフケース</a>
     </div> 
    </div>

    <div id="sec1-wrapper">
     <div class="left">
      <a href=""><img src="images/common/sidebar-slice/s3.png" alt="" /></a>
     </div>
     <div class="right">
      <div class="tag-03" /><span class="date">2015/09/17</span></div>
      <a href="" target="_blank" class="title">創業者の近藤、水かきマチの3wayバッグを試作中</a>
     </div> 
    </div>

    <div id="sec1-wrapper">
     <div class="left">
      <a href=""><img src="images/common/sidebar-slice/s4.png" alt="" /></a>
     </div>
     <div class="right">
      <div class="tag-04" /><span class="date">2015/09/17</span></div>
      <a href="" target="_blank" class="title">サンプル鞄が続々と by FACT ORY SHOP サンプル鞄が続...</a>
     </div> 
    </div>
   </div> <!-- // #sidebar-container -->
   <!-- // What's New [新着情報] Section -->

   <!-- Accessories [革鞄と革小物] Section -->
   <div id="sidebar-container" class="mb45"> <!-- #sidebar-container -->
   <div id="accent-2"></div>
    <h3 class="sidebar-title font-ryumin" style="margin-top:0px; margin-bottom:2px;">
		革鞄と革小物</h3>
    <div id="accent-2"></div>
    
    <ul id="acc-list">
     <li><a href=""><img src="images/common/sidebar-slice/cat1.png"><p>クラシックバッグ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat2.png"><p>ビジネスバッグ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat3.png"><p>ショルダーバッグ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat4.png"><p>リュック</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat5.png"><p>3wayバッグ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat6.png"><p>トートバッグ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat7.png"><p>ボストンバッグ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat8.png"><p>ベルトポーチ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat9.png"><p>レディースバッグ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat10.png"><p>ボディバッグ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat11.png"><p>セカンドバッグ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat12.png"><p>カメラバッグ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat13.png"><p>ランドセル</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat14.png"><p>革財布</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat15.png"><p>ステーショナリー</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat16.png"><p>名刺・カード入れ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat17.png"><p>パスケース</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat18.png"><p>ポーチ</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat19.png"><p>革小物</p></a></li>
     <li><a href=""><img src="images/common/sidebar-slice/cat20.png"><p>ストラップ・肩当て</p></a></li>
    </ul>
   </div> <!-- // #sidebar-container -->
   <!-- // Accessories [革鞄と革小物] Section -->

   <!-- Features [特集] Section -->
   <div id="sidebar-container" class="mb45"> <!-- #sidebar-container -->
   <div id="accent-2"></div>
    <h3 class="sidebar-title font-ryumin" style="margin-top:0px; margin-bottom:2px;">
		特集</h3>
    <div id="accent-2"></div>
    
    <div id="sidebar-single">
     <div id="img-box">
      <img class="side-tape-left" src="images/common/sidebar-slice/tape-left.png" alt="" />
      <img class="side-tape-right" src="images/common/sidebar-slice/tape-right.png" alt="" />
      <div class="img1"><img src="images/common/sidebar-slice/sidebar-img1.png" alt="" ></div>
     </div>
     <p class="title">リュックの試作 ～定番化を目指して作り手：村松～</p>
     <a href=""><img src="images/common/sidebar-slice/sidebar-arrow.png" alt="" style="margin-right: 3px;" />特集一覧</a>
    </div>
   </div> <!-- // #sidebar-container -->
   <!-- // Features [特集] Section -->

   <!-- Staff's Favorites [スタッフ愛用品] Section -->
   <div id="sidebar-container" class="mb45"> <!-- #sidebar-container -->
   <div id="accent-2"></div>
    <h3 class="sidebar-title font-ryumin" style="margin-top:0px; margin-bottom:2px;">
		スタッフ愛用品</h3>
    <div id="accent-2"></div>
    
    <div id="sidebar-single">
     <div id="img-box">
      <img src="images/common/sidebar-slice/sidebar-img2.png" alt="" >
     </div>
     <p class="title">リュックの試作 ～定番化を目指して作り手：村松～</p>
     <a href=""><img src="images/common/sidebar-slice/sidebar-arrow.png" alt="" style="margin-right: 3px;" />スタッフ愛用品一覧</a>
    </div>
   </div> <!-- // #sidebar-container -->
   <!-- // Staff's Favorites [スタッフ愛用品] Section -->

   <!-- Press Release [プレスリリース] Section -->
   <div id="sidebar-container" class=""> <!-- #sidebar-container -->
   <div id="accent-2"></div>
    <h3 class="sidebar-title font-ryumin" style="margin-top:0px; margin-bottom:2px;">
		プレスリリース</h3>
    <div id="accent-2"></div>
    
    <div id="sidebar-single">
     <div id="img-box">
      <img src="images/common/sidebar-slice/sidebar-img3.png" alt="" >
     </div>
     <p class="title">リュックの試作 ～定番化を目指して作り手：村松～</p>
     <a href=""><img src="images/common/sidebar-slice/sidebar-arrow.png" alt="" style="margin-right: 3px;" />プレスリリース一覧</a>
    </div>
   </div> <!-- // #sidebar-container -->
   <!-- // Press Release [プレスリリース] Section -->

  </div> <!-- // #p-sidebar -->