<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-{page-name} has-aside">
			
			<div class="ablk-1 header-breadcrumb">
				<p class="breadcrumb">
					<a class="anc link-3" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				<div class="ablk-1 site-content-breadcrumb">
					<p class="breadcrumb">
						<a class="anc link-3" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
					</p>
				</div>
				
				<div class="ablk-{n} {modifier-name}">
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2>
							Japanese Header
							<span class="header-eng">-English-</span>
							<a class="anc link-2" href="#"><img src="images/updt-common/ico-arrow-right-black.png" />特集一覧</a>
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					<h1>
						H1
					</h1>
					<h2>
						H2
					</h2>
					<h3>
						H3
					</h3>
					<h4>
						H4
					</h4>
					<h5>
						H5
					</h5>
					<h6>
						H6
					</h6>
					<p>
						 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eget felis facilisis, placerat ipsum ut, posuere dolor. In malesuada feugiat dolor at suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tempor urna sed arcu viverra, nec aliquet lectus rhoncus. Donec venenatis erat mauris. Sed varius efficitur aliquet. Nullam ut rutrum orci. In hac habitasse platea dictumst. Maecenas non condimentum lacus. Nullam nunc dui, porta quis aliquam quis, euismod vitae erat.
					</p>
					<p>
						Suspendisse luctus, ipsum nec convallis hendrerit, dui lacus ornare purus, nec maximus lorem diam eu sapien. Nam quis mi quis nunc varius volutpat. Nam lorem purus, pellentesque nec risus nec, feugiat convallis erat. In dictum velit tincidunt erat maximus, vitae fermentum lorem fermentum. Cras vel tincidunt turpis, at tincidunt felis. Quisque dui velit, faucibus tincidunt malesuada eu, vulputate in augue. Aenean nunc risus, lobortis consectetur ante et, efficitur finibus diam. Aenean dui mi, interdum id diam nec, suscipit iaculis leo. Quisque suscipit nec nulla vel cursus. Ut vel imperdiet nisi, ac facilisis libero. 
					</p>
					<ul>
						<li>箇条書きリスト1の体裁が入ります。</li>
						<li>箇条書きリスト1の体裁が入ります。</li>
						<li>箇条書きリスト1の体裁が入ります。</li>
						<li>箇条書きリスト1の体裁が入ります。</li>
						<li>箇条書きリスト1の体裁が入ります。</li>
						<li>箇条書きリスト1の体裁が入ります。</li>
						<li>箇条書きリスト1の体裁が入ります。</li>
					</ul>
					<p>
						<strong>太字の体裁はこの体裁。wordpressで「B」を選択した時の体裁</strong>
					</p>
					<blockquote>
						<p>
							引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。
							引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。
						</p>
						<p>
							引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。
						</p>						
					</blockquote>
					<p>
						<del>
							打ち消し線の場合の表示
						</del>
					</p>
					
					<h4>
						見出し4
					</h4>
					<br/><br/>
					<h5>
						見出し5
					</h5>
					<br/><br/>
					<h6>
						見出し6
					</h6>
					<br/><br/>
					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
