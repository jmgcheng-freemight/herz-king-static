<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						経年変化を楽しむ
						<span class="header-eng">ENJOY THE AGING</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br /><br />
				
				<div class="cblk-1">
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/aging/img1.png" />
						
						
					</div>
					
					<br/><br/><br/><br/>
					

					<header class="header-content">
						<h2 class="h">
							共に時を刻むカバン
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>お届けしたカバンは未完成です、と聞いたら驚かれるでしょうか。 <br />
   私達がお作りするのは80％まで、残りの20％はお客様に使っていただく事によって仕上げられるのです。 <br />
   カバンは使い手がいなければ、単なる物に過ぎません。手で触れ、使われて初めて生きてきます。
						</p>

					</div>
					
					<br /><br /><br />
					
					
					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/aging/img2.png">
						<p>
							タンニンなめしの革は、使い込むと色艶を増し、経年変化と共に自分だけの味が出てきます。使い始めは少し固いと感じるかもしれませんが、使う度にしなやかに手に馴染んでくるのが本革の良い所です。 通常、物が古びるという事は、否定的な表現方法として使われがちですが、本革のカバンは全く逆の意味合いになります。 革鞄は使えば使うほど、経年変化と共にその価値を増し、深みのある雰囲気を醸し出してくるのです。
						</p>
						<div class="clear-both"></div>
					</div>
					
					
					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/aging/img3.png">
						<p>
							2年、3年とカバンと共に過ごしてみて下さい。きっとあなただけのカバンが完成しているはずです。<br/>
      そのうちについたキズや汚れも、その鞄が刻んだ「時の年輪」となることでしょう。<br/>
      愛着の湧く、長く付き合えるパートナー。それがHERZのカバンです。
						</p>
						<div class="clear-both"></div>
					</div>
					
					<br/><br/><br/><br/>
					
					

					<header class="header-content">
						<h2 class="h">
							HERZで使用している革は大きく分けて2種類
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					<div class="iblk">
					  <div class="comparison">
					   <div class="col2">
						<div class="col2-items">
						 <div class="comparison-thumb"><img src="images/aging/img4.png" alt=""/></div>
						 <h4 class="comparison-title">HERZの定番カラー</h4>
						 <h4 class="comparison-color">革色：キャメル</h4>
						 <p class="comparison-dtl">使い込むほどに味わいが増し、飴色に変化します。</p>
						</div>

						<div class="col2-items mleft20">
						 <div class="comparison-thumb"><img src="images/aging/img5.png" alt=""/></div>
						 <h4 class="comparison-title">男女ともに使える落ち着いた色</h4>
						 <h4 class="comparison-color">革色：チョコ</h4>
						 <p class="comparison-dtl">キャメルほど色の変化はありませんが、艶が増し、色が深くなっていきます。</p>
						</div>

						<div class="col2-items">
						 <div class="comparison-thumb"><img src="images/aging/img6.png" alt=""/></div>
						 <h4 class="comparison-title">ビジネスシーンの定番色</h4>
						 <h4 class="comparison-color">革色：黒（ブラック）</h4>
						 <p class="comparison-dtl">色の変化はないものの、使い込むと艶は増していき、風格が出てきます。ビジネス使いにピッタリです。</p>
						</div>

						<div class="col2-items mleft20"> 
						 <div class="comparison-thumb"><img src="images/aging/img7.png" alt=""/></div>
						 <h4 class="comparison-title">落ち着いた雰囲気で深みのある緑</h4>
						 <h4 class="comparison-color">革色：グリーン</h4>
						 <p class="comparison-dtl">キャメルの様に、経年変化と共に色味が変わっていきます。使用状況により、色の具合もそれぞれ異なります。</p>
						</div>

						<div class="col2-items"> 
						 <div class="comparison-thumb"><img src="images/aging/img8.png" alt=""/></div>
						 <h4 class="comparison-title">女性の方が好んでお使いになる赤</h4>
						 <h4 class="comparison-color">赤（レッド）</h4>
						 <p class="comparison-dtl">キャメルほど色の変化はありませんが、艶が増し、色が深くなっていきます。</p>
						</div> 
						<div class="clear-both"></div>
					   </div> 
					  </div> 
					</div> 
					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/aging/img9.png">
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">HERZオリジナルレザーについて詳しく見る</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>	

					
					
					<br/><br/><br/><br/>
					
					

					<header class="header-content">
						<h2 class="h">
							Organイタリアンレザーのエイジング
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">

					<p>Organで使用しているイタリアンレザーはHERZの革よりも油分を多く含んでいるので、使っていく中でのエイジングも比較的早いです。HERZの革と同様、色艶が増して、革本来の味の変化をお楽しみ頂けます。</p>
					</div>
					
					<div class="iblk">
					
					  <div class="comparison">
					   <div class="col2">
						<div class="col2-items">
						 <div class="comparison-thumb"><img src="images/aging/img10.png" alt=""/></div>

						</div>

						<div class="col2-items mleft20">
						 <div class="comparison-thumb"><img src="images/aging/img11.png" alt=""/></div>

						</div>

						<div class="col2-items">
						 <div class="comparison-thumb"><img src="images/aging/img12.png" alt=""/></div>

						</div>

						<div class="col2-items mleft20"> 
						 <div class="comparison-thumb"><img src="images/aging/img13.png" alt=""/></div>

						</div>


						<div class="clear-both"></div>
					   </div> 
					  </div> 
					
					
					</div>
					
					<div class="iblk"><a href="" class="link"> <img src="images/aging/arrow-right.png" alt="">HERZオリジナルレザーについて詳しく見る</a></div>
					<br /><br /><br />
					<div class="iblk">
					
						<div class="related-links"> 
						   <h3>関連リンク</h3>

						   <div class="related-sec"> 
							<div class="related-sec__item"><a href="">
							 <div class="related-sec__thumb"><img src="images/aging/img14.png" alt=""></div>
							 <p class="related-sec__dtl">革の経年変化（エイジング）も分かりますので、ご参考下さい。</p>
							</a></div>

							<div class="related-sec__item"><a href="">
							 <div class="related-sec__thumb"><img src="images/aging/img15.png" alt=""></div>
							 <p class="related-sec__dtl">HERZ革製品と共に暮らす使い手さんたちの写真投稿ブログ</p>
							</a></div>

							<div class="related-sec__item"><a href="">
							 <div class="related-sec__thumb"><img src="images/aging/img16.png" alt=""></div>
							 <p class="related-sec__dtl">お使いのHERZバッグや小物たちを公式サイトに投稿してみませんか</p>
							</a></div>
						   </div>
						  </div>					
					
					</div>
					<br /><br /><br />
					
					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
