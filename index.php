<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-index">
		
			<div class="site-content">
				
				<header class="header-shoutout header-hertz" >
					<div class="b-content">
						<h2>
							鞄は道具です。<br/>
							持ちやすく、使いやすく<br/>
							飽きのこない丈夫なものがいい。<br/>
							それが「ヘルツの鞄」です。
						</h2>
					</div>
				</header>
				<br/><br/><br/><br/><br/>
				<!-- 
				-->
				
				<!-- 
				<div class="banner-bgimg" >
					<div class="banner-bgimg-images banner-bgimg-images-carousel">
						<div>
							<img src="images/updt-index/img-1-643.jpg" />
						</div>
						<div>
							<img src="images/updt-index/img-2-643.jpg" />
						</div>
						<div>
							<img src="images/updt-index/img-3-643.jpg" />
						</div>
						<div>
							<img src="images/updt-index/img-4-643.jpg" />
						</div>
						<div>
							<img src="images/updt-index/img-5-643.jpg" />
						</div>
						<div>
							<img src="images/updt-index/img-6-643.jpg" />
						</div>
						<div>
							<img src="images/updt-index/img-7-643.jpg" />
						</div>
						<div>
							<img src="images/updt-index/img-8-643.jpg" />
						</div>
						<div>
							<img src="images/updt-index/img-9-643.jpg" />
						</div>
						<div>
							<img src="images/updt-index/img-10-643.jpg" />
						</div>
						<div>
							<img src="images/updt-index/img-11-643.jpg" />
						</div>
					</div>
					<div class="banner-bgimg-shoutout " >
						<div class="l-container">
							<h2>
								鞄は道具です。<br/>
								持ちやすく、使いやすく<br/>
								飽きのこない丈夫なものがいい。<br/>
								それが「ヘルツの鞄」です。
							</h2>
						</div>
					</div>
				</div>
				-->
				
				<br/><br/><br/><br/><br/>
				
				<div class="cblk-1">
					
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2 class="h">
							お知らせ
							<span class="header-eng">-NEWS-</span>
							<a class="anc anc-header-content" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />お知らせ一覧</a>
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk iblk-1 carousel news-list-carousel">
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-1.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-blog.png" />2015/09/17</h3>
							<a class="anc link-1" href="#">Early Autumn un Nagoya</a>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-2.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-feature.png" />2015/09/14</h3>
							<a class="anc link-1" href="#">A’-3ダイアログ ブリーフケース</a>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-3.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-press.png" />2015/09/17</h3>
							<a class="anc link-1" href="#">創業者の近藤、水かきマチの3wayバッグを試作中</a>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-4.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-shopping.png" />2015/09/17</h3>
							<a class="anc link-1" href="#">サンプル鞄が続々と byFACTORY SHOP</a>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-1.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-blog.png" />2015/09/17</h3>
							<a class="anc link-1" href="#">Early Autumn un Nagoya</a>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-2.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-feature.png" />2015/09/14</h3>
							<a class="anc link-1" href="#">A’-3ダイアログ ブリーフケース</a>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-3.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-press.png" />2015/09/17</h3>
							<a class="anc link-1" href="#">創業者の近藤、水かきマチの3wayバッグを試作中</a>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/news-list-thumb-4.jpg" /></a>
							<h3><img src="images/updt-common/ico-news-list-tag-shopping.png" />2015/09/17</h3>
							<a class="anc link-1" href="#">サンプル鞄が続々と byFACTORY SHOP</a>
						</div>
					</div>
					
					<br/><br/><br/>
					
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2 class="h">
							新作アイテム
							<span class="header-eng">-NEW ITEM-</span>
							<a class="anc anc-header-content" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />新作アイテム一覧</a>
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk iblk-2">
						<div class="col col-1">
							<div class="prg-box-1">
								<p>
									新作は日々鞄を制作している<br/>
									作り手自身がデザインしています。
								</p>
								<p>
									枠にとらわれない自由な発想で作られる<br/>
									カバンや小物たちはどれも個性的でありなが<br/>
									らも、HERZらしい雰囲気をもっています。
								</p>
							</div>
							<div class="mb30"></div>
							<img class="img-bg-kuno" src="images/updt-index/bg-new-item.jpg" />
							<div class="clear-both"></div>
						</div>
						<div class="col col-2">
							<ul>
								<li class="li-item odd">
									<a class="anc-img" href="#"><img src="images/updt-common/new-item-thumb-1.jpg" /></a>
									<a class="anc link-1" href="#">Organ新作：肩掛けファスナートート</a>
								</li>
								<li class="li-item even">
									<a class="anc-img" href="#"><img src="images/updt-common/new-item-thumb-2.jpg" /></a>
									<a class="anc link-1" href="#">縦型ペンケース</a>
								</li>
								<li class="li-item odd">
									<a class="anc-img" href="#"><img src="images/updt-common/new-item-thumb-3.jpg" /></a>
									<a class="anc link-1" href="#">Organ新作：ブリーフケース</a>
								</li>
								<li class="li-item even">
									<a class="anc-img" href="#"><img src="images/updt-common/new-item-thumb-4.jpg" /></a>
									<a class="anc link-1" href="#">2本手ファスナービジネスバッグ</a>
								</li>
								<div class="clear-both"></div>
							</ul>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/><br/><br/>
					
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2 class="h">
							定番アイテム
							<span class="header-eng">-STANDARD ITEM-</span>
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk iblk-2">
						<div class="col col-1">
							<h2>
								<img src="images/updt-common/site-logo-2.png" />
							</h2>
							<div class="mb20"></div>
							<p>
								1973年の創業当時から 「丈夫で長く使える鞄」をテーマに武骨な革製品を400型以上、変わらずに作り続けています。
							</p>
							<div class="mb20"></div>
							<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />オンラインショップ</a>
							<div class="mb20"></div>
						</div>
						<div class="col col-2">
							<ul>
								<li class="li-item odd">
									<a class="anc-img" href="#"><img src="images/updt-common/standard-item-thumb-1.jpg" /></a>
									<a class="anc link-1" href="#">総かぶせ・ヨコ型2wayビジネスバッグ(BC-13)</a>
								</li>
								<li class="li-item even">
									<a class="anc-img" href="#"><img src="images/updt-common/standard-item-thumb-2.jpg" /></a>
									<a class="anc link-1" href="#">ファスナー長財布(WL-58)</a>
								</li>
								<div class="clear-both"></div>
							</ul>
						</div>
						<div class="clear-both"></div>
					</div>
					<div class="accent-2 mb20"></div>
					<div class="iblk iblk-2">
						<div class="col col-1">
							<h2>
								<img src="images/updt-common/organ-logo.png" />
							</h2>
							<div class="mb20"></div>
							<p>
								イタリアンレザーを使用し「道具としての鞄」をよりシンプルに、女性にも気兼ねなく使える革製品を作り出しています。
							</p>
							<div class="mb20"></div>
							<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />Organモデル販売ページ</a><br/>
							<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />Organブランドサイト</a>
							<div class="mb40"></div>
						</div>
						<div class="col col-2">
							<ul>
								<li class="li-item odd">
									<a class="anc-img" href="#"><img src="images/updt-common/standard-item-thumb-3.jpg" /></a>
									<a class="anc link-1" href="#">ショルダーバッグ(G-19)</a>
								</li>
								<li class="li-item even">
									<a class="anc-img" href="#"><img src="images/updt-common/standard-item-thumb-4.jpg" /></a>
									<a class="anc link-1" href="#">マチ付き二つ折り財布(GS-16)</a>
								</li>
								<div class="clear-both"></div>
							</ul>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/><br/><br/>
					
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2 class="h">
							革鞄と革小物
							<span class="header-eng">-CATEGORY-</span>
							<a class="anc anc-header-content" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />オンラインショップ</a>
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk iblk-3">
						<ul>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_01.png" /></a>
								<a class="anc link-1" href="#">クラシックバッグ)</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_02.png" /></a>
								<a class="anc link-1" href="#">ビジネスバッグ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_03.png" /></a>
								<a class="anc link-1" href="#">ショルダーバッグ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_04.png" /></a>
								<a class="anc link-1" href="#">リュック</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_05.png" /></a>
								<a class="anc link-1" href="#">3wayバッグ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_06.png" /></a>
								<a class="anc link-1" href="#">トートバッグ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_07.png" /></a>
								<a class="anc link-1" href="#">ボストンバッグ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_08.png" /></a>
								<a class="anc link-1" href="#">ベルトポーチ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_09.png" /></a>
								<a class="anc link-1" href="#">レディースバッグ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_10.png" /></a>
								<a class="anc link-1" href="#">ボディバッグ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_11.png" /></a>
								<a class="anc link-1" href="#">セカンドバッグ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_12.png" /></a>
								<a class="anc link-1" href="#">カメラバッグ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_13.png" /></a>
								<a class="anc link-1" href="#">ランドセル</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_14.png" /></a>
								<a class="anc link-1" href="#">革財布</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_15.png" /></a>
								<a class="anc link-1" href="#">ステーショナリー</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_16.png" /></a>
								<a class="anc link-1" href="#">名刺・カード入れ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_17.png" /></a>
								<a class="anc link-1" href="#">ポーチ</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_18.png" /></a>
								<a class="anc link-1" href="#">パスケース</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_19.png" /></a>
								<a class="anc link-1" href="#">革小物</a>
							</li>
							<li class="li-item">
								<a class="anc-img" href="#"><img src="images/updt-common/category-thumb-_20.png" /></a>
								<a class="anc link-1" href="#">ストラップ・肩当て</a>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
					<br/><br/><br/>
					
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2 class="h">
							特集
							<span class="header-eng">-SPECIAL CONTENT-</span>
							<a class="anc anc-header-content" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />特集一覧</a>
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk iblk-4">
						<ul>
							<li class="li-item">
								<div class="col col-1">
									<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png" />
									<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png" />
									<a class="anc-img" href="#">
										<img class="" src="images/updt-common/special-content-thumb-1.jpg" />
									</a>
								</div>
								<div class="col col-2">
									<a class="anc link-1" href="#">リュックの試作 ～定番化を目指して 作り手：村松～</a>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png" />
									<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png" />
									<a class="anc-img" href="#">
										<img class="" src="images/updt-common/special-content-thumb-2.jpg" />
									</a>
								</div>
								<div class="col col-2">
									<a class="anc link-1" href="#">HERZなんでも実験室:雨に濡れてしまった！気になる雨染みどうなるの？</a>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png" />
									<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png" />
									<a class="anc-img" href="#">
										<img class="" src="images/updt-common/special-content-thumb-3.jpg" />
									</a>
								</div>
								<div class="col col-2">
									<a class="anc link-1" href="#">創業者とカバンを作ろう：第2弾試作編</a>
								</div>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
				</div>
				
				<br/><br/><br/>
				
				<header class="header-shoutout header-learn" >
					<div class="b-content">
						<h2>
							HERZとはドイツ語でハート(心)
						</h2>
						<h3>
							HERZ THE HEART
						</h3>
						<p>
							HERZ(ヘルツ)では「一生使える」をテーマに、<br/>
							流行に流されない味わいのある鞄を作り続けています。
						</p>
						<p>
							革の裁断から縫製まで全て自分たちの手で作り上げる<br/>
							本当のMADE IN JAPANにこだわり続けて40年以上。<br/>
							革鞄と共に過ごす、楽しさ。喜び。<br/>
							それをお伝えすることが、HERZの願いです。
						</p>
						<a class="anc anc-header-content" href="#">
							<img class="ico-anc" src="images/updt-common/ico-arrow-right-white.png" />HERZについて詳しく
						</a>
					</div>
				</header>
				
				<br/><br/><br/><br/><br/>
				
				<div class="cblk-1">
					
					<div class="iblk iblk-5">
						<ul>
							<li class="li-item">
								<header class="header-content">
									<div class="accent-1 mb15"></div>
									<h2>
										HERZの鞄
									</h2>
									<div class="accent-1 mt15"></div>
								</header>
								<div class="mb40"></div>
								<a class="anc-img" href="#"><img src="images/updt-common/learn-hertz-thumb-1.jpg" /></a>
								<p>
									選んでくださった皆さんに出来る限り永く使って欲しいからHERZの鞄はとにかく「丈夫であること」を大切に作っています。
								</p>
								<div class="clear-both"></div>
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />HERZの鞄</a>
							</li>
							<li class="li-item">
								<header class="header-content">
									<div class="accent-1 mb15"></div>
									<h2>
										HERZの歴史
									</h2>
									<div class="accent-1 mt15"></div>
								</header>
								<div class="mb40"></div>
								<a class="anc-img" href="#"><img src="images/updt-common/learn-hertz-thumb-2.jpg" /></a>
								<p>
									1973年創業のHERZ。そもそものはじまりは、創業者でもある近藤 晃理の、「作りたい！ 楽しい！」でした。
								</p>
								<div class="clear-both"></div>
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />HERZの歴史</a>
							</li>
							<li class="li-item last-item">
								<header class="header-content">
									<div class="accent-1 mb15"></div>
									<h2>
										HERZの鞄
									</h2>
									<div class="accent-1 mt15"></div>
								</header>
								<div class="mb40"></div>
								<a class="anc-img" href="#"><img src="images/updt-common/learn-hertz-thumb-3.jpg" /></a>
								<p>
									HERZでは、分業制ではなく、革の裁断を除く、工程の最初から最後までを一人の作り手が一つの鞄を作っています。
								</p>
								<div class="clear-both"></div>
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />工房について</a>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
				</div>
				
				<br/><br/><br/>
				
				<header class="header-shoutout header-retail" >
					<div class="b-content">
						<h2>
							作る場所と売る場所が一緒の空間
						</h2>
						<p>
							HERZの直営店舗は工房兼お店。<br/>
							自分たちの手でお届けすることを大切にしているためです。<br/>
							作り手一同、ご来店をお待ちしています。
						</p>
						<a class="anc anc-header-content" href="#">
							<img class="ico-anc" src="images/updt-common/ico-arrow-right-white.png" />直営店一覧
						</a>
					</div>
				</header>
				
				<br/><br/><br/><br/><br/>
				
				<div class="cblk-1">
					
					<div class="iblk iblk-1 carousel location-list-carousel">
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/location-list-thumb-1.jpg" /></a><br/>
							<a class="anc link-1" href="#">本店</a>
							<p>
								東京都渋谷区神宮前5-46-16 イルチェントロセレーノB1F<br/>
								<img class="ico" src="images/updt-common/ico-phone.png" />03-6427-2412 &nbsp; <img class="ico" src="images/updt-common/ico-clock.png" />11:00-19:00 <br/>
								<img class="ico" src="images/updt-common/ico-day.png" />水曜日
							</p>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/location-list-thumb-2.jpg" /></a><br/>
							<a class="anc link-1" href="#">FACTORY SHOP 2</a>
							<p>
								東京都渋谷区渋谷2-12-8 中村ビル1F<br/>
								<img class="ico" src="images/updt-common/ico-phone.png" />03-6427-2412 &nbsp; <img class="ico" src="images/updt-common/ico-clock.png" />11:00-19:00 <br/>
								<img class="ico" src="images/updt-common/ico-day.png" />日曜・祭日
							</p>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/location-list-thumb-3.jpg" /></a><br/>
							<a class="anc link-1" href="#">Organ</a>
							<p>
								東京都渋谷区渋谷2-12-6 三田ビル1F<br/>
								<img class="ico" src="images/updt-common/ico-phone.png" />03-6427-2412 &nbsp; <img class="ico" src="images/updt-common/ico-clock.png" />11:00-19:00 <br/>
								<img class="ico" src="images/updt-common/ico-day.png" />水曜日
							</p>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/location-list-thumb-4.jpg" /></a><br/>
							<a class="anc link-1" href="#">RESO.</a>
							<p>
								東京都渋谷区渋谷2-7-12<br/>
								<img class="ico" src="images/updt-common/ico-phone.png" />03-6427-2412 &nbsp; <img class="ico" src="images/updt-common/ico-clock.png" />11:00-19:00 <br/>
								<img class="ico" src="images/updt-common/ico-day.png" />月~木曜日
							</p>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/location-list-thumb-1.jpg" /></a><br/>
							<a class="anc link-1" href="#">本店</a>
							<p>
								東京都渋谷区神宮前5-46-16 イルチェントロセレーノB1F<br/>
								<img class="ico" src="images/updt-common/ico-phone.png" />03-6427-2412 &nbsp; <img class="ico" src="images/updt-common/ico-clock.png" />11:00-19:00 <br/>
								<img class="ico" src="images/updt-common/ico-day.png" />水曜日
							</p>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/location-list-thumb-2.jpg" /></a><br/>
							<a class="anc link-1" href="#">FACTORY SHOP 2</a>
							<p>
								東京都渋谷区渋谷2-12-8 中村ビル1F<br/>
								<img class="ico" src="images/updt-common/ico-phone.png" />03-6427-2412 &nbsp; <img class="ico" src="images/updt-common/ico-clock.png" />11:00-19:00 <br/>
								<img class="ico" src="images/updt-common/ico-day.png" />日曜・祭日
							</p>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/location-list-thumb-3.jpg" /></a><br/>
							<a class="anc link-1" href="#">Organ</a>
							<p>
								東京都渋谷区渋谷2-12-6 三田ビル1F<br/>
								<img class="ico" src="images/updt-common/ico-phone.png" />03-6427-2412 &nbsp; <img class="ico" src="images/updt-common/ico-clock.png" />11:00-19:00 <br/>
								<img class="ico" src="images/updt-common/ico-day.png" />水曜日
							</p>
						</div>
						<div>
							<a class="anc-img" href="#"><img src="images/updt-common/location-list-thumb-4.jpg" /></a><br/>
							<a class="anc link-1" href="#">RESO.</a>
							<p>
								東京都渋谷区渋谷2-7-12<br/>
								<img class="ico" src="images/updt-common/ico-phone.png" />03-6427-2412 &nbsp; <img class="ico" src="images/updt-common/ico-clock.png" />11:00-19:00 <br/>
								<img class="ico" src="images/updt-common/ico-day.png" />月~木曜日
							</p>
						</div>
					</div>
					
					<br/><br/>
					
					<div class="iblk-6">
						<div class="col col-1">
							<div class="mb40"></div>
							<img class="" src="images/updt-common/organ-logo-2.png" />
							<div class="mb40"></div>
						</div>
						<div class="col col-2">
							<div class="mb60"></div>
							<p>
								Organ(オルガン)は、HERZの新ラインを生み出す工房兼ショップです。<br/>
								”道具としての鞄”をよりシンプルな形で作り出しています。
							</p>
							<div class="mb30"></div>
						</div>
						<div class="clear-both"></div>
					</div>
					
				</div>
				
				<br/><br/><br/>
				
				<header class="header-shoutout header-material" >
					<div class="b-content">
						<h2>
							質の良い革をそのまま贅沢に使う
						</h2>
						<p>
							HERZでいう質の良い革とは、見た目が均一で綺麗な革ではありません。<br/>
							むしろその逆です。革本来のキズやシワを隠す表面加工を最小限にとどめ、<br/>
							植物タンニンで時間をかけて丹精になめしたオイルレザーを使っています。<br/>
							使い込み、時を経るごとに色に深みと艶が増し、<br/>
							使う人ならではの豊かで味わい深い表情を見せてくれます。
						</p>
						<a class="anc anc-header-content" href="#">
							<img class="ico-anc" src="images/updt-common/ico-arrow-right-white.png" />素材について詳しく見る
						</a>
					</div>
				</header>
				
				<br/><br/><br/><br/><br/>
				
				<div class="cblk-1">
					
					<div class="iblk iblk-7">
						<article class="article-img-left">
							<div class="col col-1">
								<header class="header-content">
									<div class="accent-1 mb10"></div>
									<h2 class="h">
										経年変化を楽しむ
										<span class="header-eng">-ENJOY THE AGING-</span>
									</h2>
									<div class="accent-1 mt10"></div>
								</header>
								<img class="article-img" src="images/updt-common/article-list-thumb-1.jpg" />
							</div>
							<div class="col col-2">
								<header class="header-content">
									<div class="accent-1 mb10"></div>
									<h2 class="h">
										経年変化を楽しむ
										<span class="header-eng">-ENJOY THE AGING-</span>
									</h2>
									<div class="accent-1 mt10"></div>
								</header>
								<div class="mb20"></div>
								<p>
									お届けしたカバンは未完成です、と聞いたら驚かれるでしょうか。私達がお作りするのは80%まで、残りの20％はお客様に使っていただく事によって仕上げられるのです。
								</p>
								<div class="mb25"></div>
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />経年変化を楽しむ</a>
								<div class="mb25"></div>
							</div>
							<div class="clear-both"></div>
						</article>
						<article class="article-img-right">
							<div class="col col-1">
								<header class="header-content">
									<div class="accent-1 mb10"></div>
									<h2 class="h">
										お手入れ方法
										<span class="header-eng">-CARE-</span>
									</h2>
									<div class="accent-1 mt10"></div>
								</header>
								<img class="article-img" src="images/updt-common/article-list-thumb-2.jpg" />
							</div>
							<div class="col col-2">
								<header class="header-content">
									<div class="accent-1 mb10"></div>
									<h2 class="h">
										お手入れ方法
										<span class="header-eng">-CARE-</span>
									</h2>
									<div class="accent-1 mt10"></div>
								</header>
								<div class="mb20"></div>
								<p>
									皮革製品のお手入れは少し面倒です。でもほんの少し手を掛けてあげる事によって、愛着が湧き、カバンの寿命はぐっと延びます。日常でのお手入れが、味のある革を育てる第一歩となるのです。お手入れしながら長く大切に使い続けていくこと。これこそが本当の贅沢だとHERZは考えています。
								</p>
								<div class="mb25"></div>
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />経年変化を楽しむ</a>
								<div class="mb25"></div>
							</div>
							<div class="clear-both"></div>
						</article>
						<article class="article-img-left">
							<div class="col col-1">
								<header class="header-content">
									<div class="accent-1 mb10"></div>
									<h2 class="h">
										修理について
										<span class="header-eng">-REPAIR-</span>
									</h2>
									<div class="accent-1 mt10"></div>
								</header>
								<img class="article-img" src="images/updt-common/article-list-thumb-3.jpg" />
							</div>
							<div class="col col-2">
								<header class="header-content">
									<div class="accent-1 mb10"></div>
									<h2 class="h">
										修理について
										<span class="header-eng">-REPAIR-</span>
									</h2>
									<div class="accent-1 mt10"></div>
								</header>
								<div class="mb20"></div>
								<p>
									丈夫で長く使える＝壊れないわけではありません。永くお使い頂中で、どうしても修理は必要になります。その時、お使いの鞄を拝見し、その状態にふさわしい方法で作り手が丁寧に修理いたします。HERZの鞄はお使いいただく限り、出来るだけの修理・加工をいたします。
								</p>
								<div class="mb25"></div>
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black.png" />経年変化を楽しむ</a>
								<div class="mb25"></div>
							</div>
							<div class="clear-both"></div>
						</article>
					</div>
					
					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
