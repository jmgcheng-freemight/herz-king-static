<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						会社概要
						<span class="header-eng">PROFILE</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				<br /><br />
				<div class="ablk-1">
					<div class="iblk-0">
						<img class="sp-img-wmax" src="images/company-profile/comp-profile-img1.png" />
					</div>
					<br/><br/><br/><br/>
					<header class="header-content">
						<h3>
							ヘルツの製品<br />
							オリジナルであること 丈夫であること<br />
							飽きのこないオーソドックスなデザインであること
						</h3>
						<div class="accent-1 mt15"></div>
					</header>
					<br /><br />
					<div class="iblk-0">
						<p>
							ヘルツ(HERZ)は、渋谷と博多、大阪、仙台に工房、直営店を構える鞄職人による、日本の手作り革鞄(かばん)工房です。革の裁断から縫製まで、一点一点全て自分達の手で作り上げています。本当のmade in japan(日本製)にこだわり続けて40年以上。革鞄(カバン)と共に過ごす、楽しさ。喜び。それをお伝えすることが、HERZの願いです。
						</p>
					</div>
					
					
									
					<div class="iblk-0">	
						<div class="comp-profile-row3">
							<div class="comp-table">
								<div class="table-1">
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												会社名
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												株式会社　ヘルツ
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												代表者
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												代表取締役　野口 裕明
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												所在地本社
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												 東京都渋谷区神宮前5-46-16
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												電話
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												 03-3406-1510（代）
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												FAX
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												 03-3406-1520
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												E-mail
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												  info@herz-bag.jp
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												直営店および工房
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13 mb5">
												本店 / 東京都渋谷区神宮前5-46-16
											</p>
											<p class="f13 mb5">
												FACTORY SHOP(渋谷工房) / 東京都渋谷区渋谷2-12-8
											</p>
											<p class="f13 mb5">
												Organ / 東京都渋谷区渋谷2-12-6
											</p>
											<p class="f13 mb5">
												RESO. / 東京都渋谷区渋谷2-7-12
											</p>
											<p class="f13 mb5">
												大阪店 / 大阪府大阪市西区南堀江2-4-4
											</p>
											<p class="f13 mb5">
												仙台店 / 宮城県仙台市青葉区本町2-10-33
											</p>
											<p class="f13 mb5">
												博多工房 / 福岡県福岡市博多区博多駅前3-16-10
											</p>
										</div>
									</div>
									
									
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												資本金
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												1,000万円
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												創業
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												1973年10月
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												設立
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												1987年12月
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												業種
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												企画製造販売業
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left table-cell-last">
											<p class="f13">
												取扱品目
											</p>
										</div>
										<div class="table-cell table-cell-right table-cell-last">
											<p class="f13">
												皮革製品（鞄、バッグ、生活雑貨）
											</p>
										</div>
									</div>
									

								</div>
							</div>
						</div>				

					</div>
					
					
					
					
					
						
				</div>
				
				
				
			</div>
			

			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
