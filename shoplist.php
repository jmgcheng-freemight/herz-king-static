<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-shoplist has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">HOME</a> > 年末年始お休みのお知らせ
				</p>
			</div>

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						取扱店
						<span class="header-eng">-AGENCY-</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br/><br/>
				
				<div class="cblk-1">
				
				
					<div class="iblk">
						<img class="w100p sp-img-wmax" src="images/updt-shoplist/img-1.jpg" />	
					</div>
					
					<br/><br/>
					
					<nav class="nav-grid nav-grid-shoplist">
						<ul>
							<li>
								<a class="anc anc-shoplist" href="#shoplist-retail">直営店</a>
							</li>
							<li>
								<a class="anc anc-shoplist" href="#shoplist-hokkaido">北海道 東北地方</a>
							</li>
							<li>
								<a class="anc anc-shoplist" href="#shoplist-kanto">関東地方</a>
							</li>
							<li>
								<a class="anc anc-shoplist" href="#shoplist-tokai">中部 東海地方</a>
							</li>
							<li>
								<a class="anc anc-shoplist" href="#shoplist-kansai">近畿 関西地方</a>
							</li>
							<li>
								<a class="anc anc-shoplist" href="#shoplist-shikoku">中国 四国地方</a>
							</li>
							<li>
								<a class="anc anc-shoplist" href="#shoplist-kyushu">九州地方</a>
							</li>
							<li>
								<a class="anc anc-shoplist" href="#shoplist-overseas">海外 その他</a>
							</li>
						</ul>
					</nav>
					
					<br/><br/><br/><br/>


					<header class="header-content">
						<h2 class="h" id="shoplist-retail">
							直営店
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/>
					
					<div class="iblk iblk-20 iblk-20-shoplist">
						<ul>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>HERZ本店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 03-3406-1471
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										東京都渋谷区神宮前5-46-16　イルチェントロセレーノB1F
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>FACTORY SHOP（渋谷工房）</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 03-6427-3250
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										東京都渋谷区渋谷2-12-8 中村ビル1Ｆ
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>Organ（オルガン）</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 03-3406-2010
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										東京都渋谷区渋谷2-12-6 三田ビル1F
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>大阪店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 06-6539-2525
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										大阪府大阪市西区南堀江2-4-4
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>仙台店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 022-395-7461
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										宮城県仙台市青葉区本町2-10-33
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<div class="col col-1">
									<p>
										<strong>公式通販サイト</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 03-6427-3250
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										東京都渋谷区神宮前5-46-16　イルチェントロセレーノB1F
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
						</ul>
					</div>
					
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h" id="shoplist-hokkaido">
							北海道・東北・上越地方
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/>
					
					<div class="iblk iblk-20 iblk-20-shoplist">
						<ul>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>ブルータグ</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 0166-50-1006
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										北海道旭川市旭町一条8丁目 MSグランベリー 1F
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>kasi-friendly</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 019-606-3810
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										岩手県盛岡市材木町3-8岩手ビル1階
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<div class="col col-1">
									<p>
										<strong>ボーデコール</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 0250-22-0195
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										新潟県新潟市秋葉区新津4462-1
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
						</ul>
					</div>
					
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h" id="shoplist-kanto">
							関東地方
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/>
					
					<div class="iblk iblk-20 iblk-20-shoplist">
						<ul>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>新盛堂鞄店本店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 03-3352-7033
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										新宿区新宿3-29-11
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>L.S.TOM</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 03-3836-5968
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										台東区上野6-10-7 アメ横プラザA棟53号永井商店内
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>マルキン商店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 03-3831-8423
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										台東区上野6-4-4 御徒町センタ－内
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>丸菱</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 03-3843-5366
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										台東区浅草1-19-8
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>ヤンピ</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 03-3352-8956
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										新宿区新宿3-17-7 紀伊国屋書店1F
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>鞄の専門店ヒロセ</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 0422-43-6293
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										三鷹市下連雀3-34-2
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>ヒロキポルタ店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 045-453-6336
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										横浜市西区高島2-16 横浜駅地下街ポルタ
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>ヒロキ元町店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 045-681-4741
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										横浜市中区元町2-96
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>サンペイ鞄店　館山店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 0470-22-1086
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										千葉県館山市北条1823
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<div class="col col-1">
									<p>
										<strong>サンペイ鞄店　木更津店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 0438-25-2550
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										千葉県木更津市大和1-3-16
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
						</ul>
					</div>
					
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h" id="shoplist-tokai">
							中部・東海地方
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/>
					
					<div class="iblk iblk-20 iblk-20-shoplist">
						<ul>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>クラフトA</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 0762-60-2495
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										金沢市武蔵町15-1 金沢名鉄丸越百貨店5F家庭用品課
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>BENLLY'S & JOB</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 076-234-5383
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										石川県金沢市新竪町3-34
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<div class="col col-1">
									<p>
										<strong>インク</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 055-971-4336
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										静岡県駿東郡清水町卸団地244
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
						</ul>
					</div>
					
					
					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h" id="shoplist-kansai">
							近畿・関西地方
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/>
					
					<div class="iblk iblk-20 iblk-20-shoplist">
						<ul>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>note</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 078-923-5431
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										兵庫県加西市北条町北条308-1 イオン加西北条SC1F
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>富士カバン店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 078-331-1042
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										兵庫県神戸市中央区三宮町1-6-12
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>Osmund Drive</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 0792-67-5717
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										兵庫県姫路市青山西5-7-12 1F
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<div class="col col-1">
									<p>
										<strong>鞄ヤ</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 06-6454-0038
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										大阪市北区梅田1-3-1 大阪駅前第1ビルB2F
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
						</ul>
					</div>
					
					
					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h" id="shoplist-shikoku">
							中国・四国地方
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/>
					
					<div class="iblk iblk-20 iblk-20-shoplist">
						<ul>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>CRAFT JOURNAL</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 082-555-0206
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										広島県広島市西区草津新町2-14-21
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<div class="col col-1">
									<p>
										<strong>レザーハウス</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 0888-25-2095
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										高知県高知市旭町3丁目61-1
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
						</ul>
					</div>
					
					
					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h" id="shoplist-kyushu">
							九州地方
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/>
					
					<div class="iblk iblk-20 iblk-20-shoplist">
						<ul>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>ポルコロッソ 楽天市場店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 092-434-7307
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										福岡県福岡市博多区博多駅前3-13-1 林英ビル6階
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>ポルコロッソ　天神店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 092-738-5885
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										福岡県福岡市中央区今泉2-5-30
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>ポルコロッソ キャナルシティ店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 092-263-2170
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										福岡県福岡市博多区住吉1-2-22キャナルシティーOPA1F
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>カバンのヒグチ</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 099-224-3357
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										鹿児島市東千石町15-5
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<div class="col col-1">
									<p>
										<strong>レボルセ</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> 099-812-6025
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										鹿児島市中央町1-1　アミュプラザ鹿児島3F
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
						</ul>
					</div>
					
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h" id="shoplist-overseas">
							海外・その他
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/>
					
					<div class="iblk iblk-20 iblk-20-shoplist">
						<ul>
							<li class="li-item last-item">
								<div class="col col-1">
									<p>
										<strong>台湾：「鞄工坊(Kaban Cobo)」</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<img class="ico" src="images/updt-common/ico-phone.png"/> (04) 2229 3506
									</p>
									<div class="mb20 content-pc"></div>
									<p>
										138-1, Yi-Chung Street, 1F North Dist., Taichung City
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
						</ul>
					</div>
					
					
					<br/><br/><br/>
					
					
					
					
					
					
					<br/><br/><br/>
					
					
					
					
					
				</div>
				
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
