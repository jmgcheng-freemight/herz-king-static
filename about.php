<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						HERZについて
						<span class="header-eng">ABOUT HERZ</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br /><br />
				
				<div class="cblk-1">
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/about/img1.png" />	
					</div>
					
					<br/><br/><br/><br/>

									
					<div class="iblk txtCenter">
		
						<p class="f15 font-ryumin p-adj pstyle1">
							HERZは国内に工房・店舗を構える革鞄工房です。<br/>
							1973年、創業者 近藤晃理の<br/>
							「鞄を作ることが楽しくて仕方がない！」<br/>
							そんな思いからHERZは始まりました。
						</p>
						<p class="f15 p-adj">
							一人の青年が始めた鞄屋も40年以上が経ち、少しずつ仲間が増え、<br/>
							現在作り手・スタッフ含め70名ほどがHERZを作っています。<br/>
							作り手は日々革を裁断し、ミシンを踏み、ハンマーを握る。<br/>
							スタッフはその出来たての鞄を皆さんに手渡す。<br/>
							毎日、そんな至極シンプルな活動を続けています。
						</p>
		
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							血の通う鞄
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>
							HERZではどんな小さな小物でも、お客様にご注文を頂いてから一つ一つお作りしています。そして、革の裁断以外の工程を分業はせずに最初から最後まで一人で作っています。「一人一本」それは創業からずっと変わらないHERZの制作スタイルです。<br/>
			少し時間はかかりますが、一本を通して作る鞄は一つ一つに作り手の人格が宿り、血の通った鞄になる。そう思っています。こうして作った鞄を「温かい」と感じてもらえたら。
						</p>
						<div class="clear-both"></div>
					</div>
					
					<br/><br/>
					
					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/about/img2.png" />
							<p class="p-adj">
								そして、いつか使う方の「愛着のある道具」となればこんなに嬉しいことはありません。革のカバンと共に過ごす楽しさ、喜び。<br/>
								それをお伝えすることが、HERZの願いです。
							</p>
							<br/>
							<p class="font-weight-bold p-adj">
								HERZはドイツ語でハート（心）。<br/>
								今までもこれからも、心ある鞄作りを続けていきます。
							</p>
						<div class="clear-both"></div>
					</div>
					
					
					<br/><br/><br/><br/>
					
					
					<header class="header-content">
						<h2 class="h">
							HERZ=丈夫
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					<br/><br/>
					<div class="iblk">
						<p>
			多くの中からせっかく選んで頂いた鞄。少しでも永く使って頂きたいから、私たちは道具として「丈夫」であることを一番に、鞄を作っています。<br/>
			新作を作る時も、日々の制作をする時も、常に考えるのは「丈夫かどうか」。「永く使える物づくり」はヘルツの芯であり、永く使えるようにするにはどう作ればいいのかからスタートするのです。<br/>
			もちろん、摩耗する金具は交換をしながら付き合っていく事が前提ですが、修理に耐えうる作りを心がけています。
						</p>
						<div class="clear-both"></div>
					</div>
					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/about/img3.png" />
							<p class="p-adj">
								丈夫かどうかを考えると、やっぱり革はより厚く、糸はより太く、金具はより大きいものが良い。だから、革はなるべく漉かずに厚いまま使用し、番手の大きな太い糸で力強く縫い上げ、目に留まる大きな金具でしっかりと留めています。
							</p>
							<br/>
							<p class="p-adj">
								革・ステッチ・金具、それはすべて丈夫さのための手段ですが、不思議とそれがデザインの一部として息づいています。だからHERZの鞄は「力強く生き生きしている」と言って頂けるのかもしれません。
							</p>
						<div class="clear-both"></div>
					</div>
					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/about/img4.png" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />HERZの鞄</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<img src="images/about/img5.png" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />HERZの歴史</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>

					
					
					
					
					<br/><br/><br/><br/>
					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
