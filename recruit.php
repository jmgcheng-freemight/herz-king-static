<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						採用情報
						<span class="header-eng">RECRUIT</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				<br /><br />
				<div class="ablk-1">
					<div class="iblk-0">
						<img class="sp-img-wmax" src="images/common/middle/middle-img-13.png" />
					</div>
					<br/><br/><br/><br/>
					<header class="header-content">
						<h2 class="h">
							HERZの仕事において大事にしていること
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					<br /><br />
					<div class="iblk-0">
						<p>
							株式会社ヘルツでは、下記の働く上での指針に共感し実行できる人材を求めています。<br>
  <br>
  以下、HERZ服務心得より抜粋
						</p>
					</div>
					
					<br /><br />
					<header class="header-content">
						<h3 class="font-ryumin mb20">
								服務の原則
							</h3>
					</header>
					<div class="iblk-0">
						<ul class="content-list mb50">
   <li>つくる喜びを伝え共有する。</li>
  </ul>
					</div>
					
					
					
					<header class="header-content">
						<h3 class="font-ryumin mb20">
								服務の原則
							</h3>
					</header>
					<div class="iblk-0">
					  <ul class="content-list">
					   <li>仕事の進め方はチームワークを基本とし、全体の中でのチームの役割、チームの中での自己の役割を理解すること。</li>
					   <li>それぞれが将来を託して働ける環境となるよう、協力し合うこと。</li>
					   <li>つくること、売ること、使っていただくことの繋がりを意識して自己の業務に責任を持つこと。</li>
					   <li>新しいコトに挑戦する気持ち、新しいカタチを生み出す気持ちを持ち続けること。</li>
					  </ul>
					</div>
					
					<br/><br/><br/><br/>
					<header class="header-content">
						<h2 class="h">
							雇用条件・待遇等
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<div class="iblk-0">
						<div class="table-style-1">
						   <div class="row">
							<div class="cell left">
							 雇用条件
							</div>
							<div class="cell right">
							 正社員
							</div>
						   </div>
						   <div class="row">
							<div class="cell left">
							 給与・待遇
							</div>
							<div class="cell right">
							 経験、能力を考慮の上、当社既定により優遇（※試用期間3ヶ月有り）　社会保険有り
							</div>
						   </div>
						   <div class="row">
							<div class="cell left">
							 賞与
							</div>
							<div class="cell right">
							 年2回支給
							</div>
						   </div>
						   <div class="row">
							<div class="cell left">
							 昇給
							</div>
							<div class="cell right">
							 年1回
							</div>
						   </div>
						   <div class="row">
							<div class="cell left">
							 休日・休暇
							</div>
							<div class="cell right">
							 週休2日制　年間120日前後　年次有給休暇あり
							</div>
						   </div>
						   <div class="row">
							<div class="cell left">
							 勤務時間
							</div>
							<div class="cell right">
							 就業規則に記載（※勤務地によって異なる）
							</div>
						   </div>
						</div>
					</div>
					
					<br/><br/><br/><br/>
					<header class="header-content">
						<h2 class="h">
							採用の流れ
						</h2>
						<div class="accent-1 mt15"></div>
					</header>

					<div class="iblk mt20">
						<img class="sp-img-wmax" src="images/common/middle/middle-img-14.png" />	
					</div>

					<br/><br/><br/><br/>
					<header class="header-content">
						<h2 class="h">
							勤務地・募集職種
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					<br />
					<div class="iblk">
					  <ul class="title-list">
					   <li><a href="#elem1">東京/HERZ本店・Organ</a></li>
					   <li><a href="#elem2">東京/オンラインショップ</a></li>
					   <li><a href="#elem3">HERZ仙台店/仙台店スタッフ</a></li>
					  </ul>
					</div>

					<br/><br/><br/><br/>
					<header class="header-content">
						<h2 class="h">
							東京/HERZ本店・Organ
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
									
					<div class="iblk-0">	
						<div class="comp-profile-row3">
							<div class="comp-table">
								<div class="table-1">
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												募集職種
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
													HERZ本店スタッフ・Organスタッフ
											</p>
											<p class="f13">
													店舗工房業務全般（制作、店舗管理、接客、企画
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												応募資格
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
													※中途・新卒問いません。
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												集人員
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												2名
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												雇用形態
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
													正社員
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												雇用条件
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												上記 『雇用条件・待遇等』 に記載
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												勤務開始時期
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												2016年1月～
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												応募書類送付先
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13 mb5">
													〒150-0001
											</p>
											<p class="f13 mb5">
												東京都渋谷区神宮前5-46-16　イルチェントロセレーノB1F
											</p>
											<p class="f13 mb5">
												HERZ本店・Organスタッフ採用担当 宛
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												連絡先
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13 mb5">
														採用担当：北野　
											</p>
											<p class="f13 mb5">
												Mail:herz.kitano@leatherbag.co.jp
											</p>
											<p class="f13 mb5">
												※採用に関するお問い合わせは上記宛先のみにお願いします。
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>				

					</div>
					

					<br/><br/><br/><br/>
					<header class="header-content">
						<h2 class="h">
							東京/オンラインショップ
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
									
					<div class="iblk-0">	
						<div class="comp-profile-row3">
							<div class="comp-table">
								<div class="table-1">
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												募集職種
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
													オンラインショップスタッフ
											</p>
											<p class="f13">
													お客様から頂くお問い合わせの対応、受注管理、発送業務がメイン
											</p>
											<p class="f13">
													(経験を重ねた後、商品写真の撮影やページ作り等の制作業務にも携わって頂きます。)
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												応募資格
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
													※中途・新卒問いません。
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												集人員
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
													1名
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												雇用形態
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
														正社員
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												雇用条件
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
													上記 『雇用条件・待遇等』 に記載
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												勤務開始時期
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
													2016年1月～
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												応募書類送付先
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13 mb5">
													〒150-0001 
											</p>
											<p class="f13 mb5">
												東京都渋谷区神宮前5-46-16 イルチェントロセレーノB1F
											</p>
											<p class="f13 mb5">
												HERZオンラインショップ 採用担当 宛
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												連絡先
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13 mb5">
														採用担当：小野寺
											</p>
											<p class="f13 mb5">
												Mail:herz.onodera@leatherbag.co.jp
											</p>
											<p class="f13 mb5">
												※採用に関するお問い合わせは上記宛先のみにお願いします。
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>				

					</div>
					

					<br/><br/><br/><br/>
					<header class="header-content">
						<h2 class="h">
							HERZ仙台店
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
									
					<div class="iblk-0">	
						<div class="comp-profile-row3">
							<div class="comp-table">
								<div class="table-1">
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												募集職種
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
													仙台店スタッフ
											</p>
											<p class="f13">
													店舗工房業務全般（制作、店舗管理、接客、企画）
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												応募資格
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												※中途・新卒問いません。
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												集人員
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
													1名
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												雇用形態
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
															正社員
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												雇用条件
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
														上記 『雇用条件・待遇等』 に記載
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												勤務開始時期
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												2016年1月～
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												応募書類送付先
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13 mb5">
													〒980-0014
											</p>
											<p class="f13 mb5">
												宮城県仙台市青葉区本町2-10-33 第二日本オフィスビル1F
											</p>
											<p class="f13 mb5">
												HERZ仙台店　採用担当 宛
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												連絡先
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13 mb5">
														採用担当：清水
											</p>
											<p class="f13 mb5">
												Mail:herz.sendai@leatherbag.co.jp
											</p>
											<p class="f13 mb5">
												※採用に関するお問い合わせは上記宛先のみにお願いします。
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>				

					</div>
					
					
					<br/><br/><br/><br/>
					
					
						
				</div>
				
				
				
			</div>
			

			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
