$(document).ready(function(){
	
	/*
	*/
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) 
		{
			$('.page-top-img').fadeIn();
		} else {
			$('.page-top-img').fadeOut();
		}
	});
	$('.page-top-img').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
	
	
	/*
		fix menu start
	*/
	var o_main_site_header_menu = $('#site-header-wrapper');
	$(window).scroll(function(){
		o_scroll = $(window).scrollTop();
		
		if(o_scroll >= 100)
		{
			if( !o_main_site_header_menu.hasClass('position-fixed') )
			{
				o_main_site_header_menu.addClass('position-fixed');
			}
		}
		else
		{
			o_main_site_header_menu.removeClass('position-fixed');
		}
	});
	/*
		fix menu end
	*/
	
	/*
		slick slider start
	*/
	$('.news-list-carousel').slick({
		infinite: true,
		slidesToShow: 4,
		variableWidth: true,
		responsive: [
			{
			  breakpoint: 1090,
			  settings: {
				arrows:false
			  }
			}
		  ]
	});
	$('.location-list-carousel').slick({
		infinite: true,
		slidesToShow: 4,
		variableWidth: true,
		responsive: [
			{
			  breakpoint: 1090,
			  settings: {
				arrows:false
			  }
			}
		  ]
	});
	
	
	$('.banner-bgimg-images-carousel').slick({
		infinite: true,
		slidesToShow: 1,
		fade: true,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows:false, 
		speed: 1000,
		fade: true,
		cssEase: 'linear'
		/*
		variableWidth: true,
		cssEase: 'linear'
		responsive: [
			{
			  breakpoint: 1090,
			  settings: {
				arrows:false
			  }
			}
		  ]
		*/
	});
	
	
	
	
	/*
	*/
	$(".header-hertz").bgswitcher({
		mainClass : "bgswitcher",
		subClass : "bgswitcher-header-hertz",
		images: [
			"images/updt-index/img-1-643.jpg", 
			"images/updt-index/img-2-643.jpg", 
			"images/updt-index/img-3-643.jpg",
			"images/updt-index/img-4-643.jpg",
			"images/updt-index/img-5-643.jpg",
			"images/updt-index/img-6-643.jpg",
			"images/updt-index/img-7-643.jpg",
			"images/updt-index/img-8-643.jpg",
			"images/updt-index/img-9-643.jpg",
			"images/updt-index/img-10-643.jpg",
			"images/updt-index/img-11-643.jpg"
		]
	});
	
	
	
	
	
	/*
		slick slider end
	*/
	
	/*
	setInterval( change_header_hertz_bg, 1000 );
	var a_header_hertz_bg_images = [
		"images/updt-index/img-1-643.jpg", 
		"images/updt-index/img-2-643.jpg", 
		"images/updt-index/img-3-643.jpg",
		"images/updt-index/img-4-643.jpg",
		"images/updt-index/img-5-643.jpg",
		"images/updt-index/img-6-643.jpg",
		"images/updt-index/img-7-643.jpg",
		"images/updt-index/img-8-643.jpg",
		"images/updt-index/img-9-643.jpg",
		"images/updt-index/img-10-643.jpg",
		"images/updt-index/img-11-643.jpg"
	];
	var i_header_hertz_bg_images = 0;
	function change_header_hertz_bg ()
	{
		$(".header-hertz").animate({opacity: 0}, 1000, 'swing', function(){
			$(".header-hertz").css('background-image','url(images/updt-index/img-2-643.jpg)').animate({opacity: 1}, 1000, 'swing');
		});
		console.log(a_header_hertz_bg_images[i_header_hertz_bg_images]);
		console.log(i_header_hertz_bg_images);
		i_header_hertz_bg_images++;
	}
	*/
	
	/*
	$( "#clickme" ).click(function() {
		$( "#book" ).animate(
			{
				opacity: 0.25,
				left: "+=50",
				height: "toggle"
			}, 
			5000, 
			function() {

			}
		);
	});
	*/
	
	
	
	
	
	/* sp menu start 
	*/
	$('#js-list').find('.js-menu').on('click',function(e){
		e.preventDefault();
		var $target = $(this).next();
		if(!$target.is(':visible')) {
			$target.slideDown('fast'); 
		} 
		else {  
			$target.slideUp('fast'); 
		}
	});
	
	$('#js-list').find('.js-sub-menu').on('click', function(e){
		e.preventDefault();
		o_ico_target = $(this).children('.sprite');
		var $target = $(this).next();
		if(!$target.is(':visible')) {
			$target.slideDown('fast'); 
			o_ico_target.removeClass('sprite-plus-box');
			o_ico_target.addClass('sprite-minus-box');
		}
		else {
			$target.slideUp('fast'); 
			o_ico_target.removeClass('sprite-minus-box');
			o_ico_target.addClass('sprite-plus-box');
		}
	});
	/* sp menu end */
	
});


    $(document).ready(function(){
        $('.bxslider2').bxSlider();
    });

    $(document).ready(function(){
        $('.slider1').bxSlider1({
            slideWidth: 206,
            minSlides: 2,
            maxSlides: 4,
            moveSlides: 1,
            slideMargin: 45
        });
    });


    $(document).ready(function(){
        $('.slider2').bxSlider1({
            slideWidth: 205,
            minSlides: 2,
            maxSlides: 4,
            moveSlides: 1,
            slideMargin: 45
        });
    });
