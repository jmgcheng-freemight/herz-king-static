<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						修理について
						<span class="header-eng">REPAIR</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br /><br />
				
				<div class="cblk-1">
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/repair/img1.png" />	
					</div>
					
					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h">
							HERZの鞄はお使いいただく限り、出来るだけの修理・加工をいたします
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<img class="float-left mxw341 sp-img-nofloat sp-img-wmax mr25 mb25" src="images/repair/img2.png" />
						<p>
							お使いの鞄を拝見し、その状態にふさわしい方法で丁寧に修理いたします。修理方法が何通りかある場合は、ご希望やご予算によって納得のいく方法をお選び頂いております。<br/><br/>
						ご購入頂いた革製品は、心を込めて出来るだけの修理をさせて頂きます。<br/>
						それがHERZの「こころ」です。
						</p>
						<div class="clear-both"></div>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							カバンの修理例
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>
							鞄の底が破れていたため、補強革を加えて強度が増す様に加工を施しました。
						</p>
						<div class="clear-both"></div>
					</div>
					<br/><br/>
					<div class="iblk">
						<div id="comparison"> 
							<div class="col2"> 
								<div class="col2-items box50">
									<div class="comparison-thumb"><img src="images/repair/img3.png" alt=""/></div>
									<p class="comparison-dtl font-weight-bold txtCenter">
										修理前
									</p>
								</div> 
								<div class="col2-items box50"> 
									<div class="comparison-thumb"><img src="images/repair/img4.png" alt=""/></div>
									<p class="comparison-dtl font-weight-bold txtCenter">
										修理後
									</p>
								</div>
								<div class="clear-both"></div>								
							</div>
						</div> 
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							修理の受付方法
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>
							直営店舗にご来店頂くか、メールにてお問い合わせ頂ければ受付可能です。<br/>
			その際、お見積り・納期等のご案内をさせて頂きます。<br/>
			※修理可否の判断の為、実際に現物の状態を拝見させて頂いてから詳細のご連絡をいたします。<br/>
			※尚、修理は当工房の製品のみ受付可能です。他メーカー様の商品はお断りしております。
						</p>
					</div>
					
					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/repair/img5.png" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />HERZ店舗一覧</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<img src="images/repair/img6.png" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />お問い合わせ</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
					<br/><br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							修理、加工の一例と費用
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					<br/><br/>
					
					<div class="iblk">
						<p>
							※お使いの鞄のタイプや使用状態によって、修理費用は変わります。
						</p>
					</div>

					<div class="iblk">
						<div id="comparison"> 
							<div class="col2"> 
								<div class="col2-items box50">
									<div class="comparison-thumb"><img src="images/repair/img7.png" alt=""/></div>
									<h4 class="comparison-title">金具交換</h4>
									<p class="comparison-dtl">
										金額: 540円～(税込)<br/>
										負担の大きい箇所は摩耗するので、交換が必要です。
									</p>
								</div> 
								<div class="col2-items box50"> 
									<div class="comparison-thumb"><img src="images/repair/img8.png" alt=""/></div>
									<h4 class="comparison-title">根革交換</h4>
									<p class="comparison-dtl">
										金額: 1,080円～(税込)<br/>
										鞄本体のショルダー・リュック結合部の革の交換も可能です。
									</p>
								</div>
								<div class="clear-both"></div>								
							</div>
						</div> 
						<div id="comparison"> 
							<div class="col2"> 
								<div class="col2-items box50">
									<div class="comparison-thumb"><img src="images/repair/img9.png" alt=""/></div>
									<h4 class="comparison-title">ファスナー交換</h4>
									<p class="comparison-dtl">
										金額: 4,320円～(税込)<br/>
						ファスナー・金具ともに修理は可能です。
									</p>
								</div> 
								<div class="col2-items box50"> 
									<div class="comparison-thumb"><img src="images/repair/img10.png" alt=""/></div>
									<h4 class="comparison-title">取っ手交換</h4>
									<p class="comparison-dtl">
										金額: 4,860円～(税込)<br/>
						革の消耗が激しい場合、新しい革で交換します。
									</p>
								</div>
								<div class="clear-both"></div>								
							</div>
						</div> 
						<div id="comparison"> 
							<div class="col2"> 
								<div class="col2-items box50">
									<div class="comparison-thumb"><img src="images/repair/img11.png" alt=""/></div>
									<h4 class="comparison-title">錠前交換</h4>
									<p class="comparison-dtl">
										金額: 4,320円～(税込)<br/>
						開閉を重ねることで、錠前も交換が必要になることもあります。
									</p>
								</div> 
								<div class="col2-items box50"> 
									<div class="comparison-thumb"><img src="images/repair/img12.png" alt=""/></div>
									<h4 class="comparison-title">やぶれ補修</h4>
									<p class="comparison-dtl">
										金額: 5,400円～(税込)<br/>
						角部分の革がやぶれた場合、新たな革を被せて補修します。
									</p>
								</div>
								<div class="clear-both"></div>								
							</div>
						</div> 
						<div class="comparison"> 
							<div class="col2"> 
								<div class="col2-items box50">
									<div class="comparison-thumb"><img src="images/repair/img13.png" alt=""/></div>
									<h4 class="comparison-title">角部分の剥がれ</h4>
									<p class="comparison-dtl">
										金額: 540円～(税込)<br/>
						糊づけをしている部分は使ってゆくうちに剥がれることがございます。その場合、新たに糊づけする補修が可能です。
									</p>
								</div> 
								<div class="col2-items box50"> 
									<div class="comparison-thumb"><img src="images/repair/img14.png" alt=""/></div>
									<h4 class="comparison-title">ホック交換</h4>
									<p class="comparison-dtl">
										金額: 540円～(税込)<br/>
						留め外しを繰り返すと、ホックの頭側にあるバネが外れてしまうことがあります。<br/>
						受け側も交換の場合は1,080円～となります。
									</p>
								</div>
								<div class="clear-both"></div>								
							</div>
						</div> 
					</div>
					
					
					
					
					
					
					<br/><br/><br/><br/>
					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
