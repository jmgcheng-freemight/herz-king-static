<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						製品について知ってほしいこと
						<span class="header-eng">THINGS WE WOULD YOU TO KNWOW</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br /><br />
				
				<div class="cblk-1">
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/attention/img1.png" />	
					</div>
					
					<br/><br/>

					
					<div class="iblk">
						<p>
							HERZの革製品は、良くも悪くも癖があります。革のキズやシワも同じ商品でかなり個体差があります。また、<strong>作り手の手によって一点一点仕上げているので、クラフトならではの製品の特徴もあります</strong>。 そういった意味も含めて、一点モノであることをご理解いた上で、当工房の革製品をご愛用頂ければ嬉しく思います。
						</p>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							革について
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/>
					
					<div class="iblk">
						<br />
						<h3 class="h">
							天然革の持つ豊かな表情
						</h3>
						<br />
						<img class="sp-img-wmax" src="images/attention/img2.png" />
						<br /><br /><br />
						<h4>血筋やバラキズ</h4>
						<p>
							表面加工を抑えて、自然の表現を生かした仕上げをしています。革の表面（銀面）、裏面（床面）に関わらず、血筋や バラキズといった自然の跡が入ります。少し深めのバラキズがあった場合も表面の革質が良かったり、通常使用す る上で負荷のかかりにくい位置を選んで裁断しています。使い込み、時を経るごとに色に深みと艶が増し、キズや シワも使う人ならではの味となり、カバンの一部となっていきます。
						</p>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img3.png" width="100%" />
						</div>
						<div class="box50">
							<h4>トラと呼ばれる縞模様</h4>
							<br />
							<p>首や肩の周りや背中など、革に元々あるシワが寄り やすい部位に筋状の縞模様が入ります。 これを「トラ」と呼びますが、製品によっては、大きく トラが入る場合もあります。「トラ」は、亀裂が入って いる訳ではありませんので、他の箇所と較べ強度面 は劣りません。</p>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<h4>シミや色ムラ</h4>
							<br />
							<p>人の肌と同じように革にも元々ついているシミやムラ があります。商品によっては、ポツポツしたシミやムラ が入ることがあります。 表面の染料も薄化粧で仕上げていますので、革の表 面に色ムラ等が出る場合もあります。</p>
						</div>
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img4.png" width="100%" />
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img5.png" width="100%" />
						</div>
						<div class="box50">
							<h4>革独特のにおいがあります</h4>
							<br />
							<p>環境に優しい手法で作られた植物タンニンなめしの 革を使用しています。 植物由来のタンニンでなめされた革は、革独特のに おいがあります。 お使い頂く中で、多少やわらいでいきますが、完全に 消えるということはありません。</p>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<h4>色落ち・色移りがあります</h4>
							<br />
							<p>革本来の表情をお楽しみ頂くために、色止め等の表面 加工を最小限に抑えています。衣服とのこすれや水ぬ れ等により、他の物に色移りしますのでご注意下さい。</p>
							<p class="red-highlight">※薄色の衣服での使用には特にご注意下さい。<br />
※色移りは革の表面、裏面ともに起きます。<br />
※衣服が濡れている時は特に色移りしやすいです。</p>
						</div>
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img6.png" width="100%" />
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img7.png" width="100%" />
						</div>
						<div class="box50">
							<h4>色味の違い（同一色の本体部）</h4>
							<br />
							<p>革の染色も、職人が手作業で行なっているため、同じ 革色でも色味の違いが生じます。</p>
							<p class="red-highlight">※全ての革色で、色味や風合いの違いはあります。<br />
※同じ革色を複数点ご注文頂いたとしても、色味が変わる事があります。<br />
※ご注文時に革の色味はご指定出来ません。</p>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<h4>色味の違い（同一色のパーツ部）</h4>
							<br />
							<p>一つの製品を作るにあたり、同じ革色でも厚みや質の異なる革パーツを使用していますので、色味や風合いの違いが生じます</p>
							<p class="red-highlight">※写真はキャメルですが、他の革色においても、色味や風合いの違いはあります</p>
						</div>
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img8.png" width="100%" />
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img9.png" width="100%" />
						</div>
						<div class="box50">
							<h4>革の裏（床面）</h4>
							<br />
							<p>HERZ の製品は裏地をつけていないので、一枚革の裏 面（床面）をそのまま使っています。普通は顔料を塗って、革特有のシミや模様を隠しますが、あえて隠さずに、そのまま使用しています。商品によって個体差はありますが、シミや色ムラ等の模様もあります。</p>
							
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<h4>革の毛羽立ち</h4>
							<br />
							<p>ストラップは一枚革で仕上げているため、床面の繊維が立ち上がる現象（毛羽立ち）が生じます。 また、鞄本体の床面も、使い続けていく中で擦れてカスが落ちたり、衣服につくことがあります。</p>
							
						</div>
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img10.png" width="100%" />
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img11.png" width="100%" />
						</div>
						<div class="box50">
							<h4>革の大きさを表すデシ数の刻印</h4>
							<br />
							<p>一枚の革につき一箇所のみ。
革の床面に、仕上がった革の面積を示すデシ数が刻印されています。一部商品には刻印が入っている場合があります。刻印有無の選択は出来ませんので、予めご理解の程、宜しくお願いいたします。</p>
							
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							製品の特徴
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/>
					
					<div class="iblk">
						<br />
						<h3 class="h">
							長くご愛用して頂くために
						</h3>
						<br />
						<img class="sp-img-wmax" src="images/attention/img12.png" />
						<br /><br /><br />
						<h4>HERZ製品には内張りがありません</h4>
						<p>
							長く使い続けてもらうために、修理をすることも念頭に入れた製品作りを心掛けています。 長い間、鞄を使い続けていくと、どうしても耐久性の弱い内張りから痛んできてしまうからです。
						</p>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img13.png" width="100%" />
						</div>
						<div class="box50">
							<h4>経年変化による変形があります</h4>
							<br />
							<p>一枚革で仕上げている為、厚みのあるハードレザーの バッグでもお使い頂くうちに革が柔らかくなり形が変 わっていきます。 使用状況によって、それぞれの癖が鞄に現れますので、 それがHERZの鞄を楽しむ一つの醍醐味でもあります。</p>
							
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img14.png" width="100%" />
						</div>
						<div class="box50">
							<h4>定期的なオイルメンテナンス</h4>
							<br />
							<p>革は乾燥すると、ひび割れや破れといった現象に繋がります。定期的なオイル塗布で大事な鞄は長持ちしてくれます。尚、一部製品ではオイル塗布の必要がない製品もあります。詳細は下記のページをご覧ください。</p>
							<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" width="16">お手入れ方法について詳しく見る</a>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img15.png" width="100%" />
						</div>
						<div class="box50">
							<h4>修理をしながら長く使う</h4>
							<br />
							<p>鞄に使用しているパーツ（特に金具類）はお使い頂く中で、どうしても消耗していきますので、定期的に交換が必要です。製品の修理は常時受付しています。</p>
							<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" width="16">修理について詳しく見る</a>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img16.png" width="100%" />
						</div>
						<div class="box50">
							<h4>差し込み金具の開閉方法</h4>
							<br />
							<p>開ける時はベルトの下に指を通して金具を押し上 げます。閉める時は写真のように、金具の脇をつまみながら差し込むと閉めやすいです。金具を差し込んだ状態で無理に引っ張ると、破損につながりますので、ご注意ください。</p>
							<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" width="16">修理について詳しく見る</a>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img17.png" width="100%" />
						</div>
						<div class="box50">
							<h4>錠前の開け方</h4>
							<br />
							<p>鍵穴が付いている突起部分を写真のように、下に 向かって押すとベルトが外れます</p>
							<p class="red-highlight">※お届け時は鍵をかけておりません。 開ける前に鍵を回してしまった場合は、再度、戻して下さい。</p>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img18.png" width="100%" />
						</div>
						<div class="box50">
							<h4>金具のエイジング</h4>
							<br />
							<p>一部使用している真鍮金具は、革と同様、より無垢 に近い状態に仕上げています。 そのため表面の変化が早く、すぐに色がくすみ、 アンティークな雰囲気に様変わります。お届けしたばかりの金具も錆のようなくすみが 見られますが、金具自体に問題ございません</p>
						</div>
						<div class="clear-both"></div>
					</div>
					
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							製品の特徴
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/>
					
					<div class="iblk">
						<br />
						<h3 class="h">
							作りについて
						</h3>
						<br />
						<img class="sp-img-wmax" src="images/attention/img19.png" />
						<br /><br /><br />
						<h4>ミシン跡</h4>
						<p>
							製作工程上、革の表面・裏面問わずにミシン機材の跡がつくことがあります。
						</p>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img20.png" width="100%" />
							<br /><br />
							<h4>内縫いの針穴</h4>
							<p>内縫いの商品は、糸が出ていたり、針穴が若干拡がっ ているものがあります。HERZでは厚い革をなるべく 薄く漉かず、太い糸と針で縫うので、革を折り込んで いる部分から糸や針穴が見える場合があります。</p>
						</div>
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img21.png" width="100%" />
							<br /><br />
							<h4>ひっくり返しによるシワ</h4>
							<p>内縫いの商品は縫製後に、革を表面にひっくり返す 工程があります。 その際、程度に差はありますが、革にシワが入ります。</p>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img22.png" width="100%" />
							<br /><br />
							<h4>ピン穴による窪み</h4>
							<p>一部商品においては、写真のように僅かな窪み（ピ ン穴）が表立つことがあります。</p>
						</div>
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img23.png" width="100%" />
							<br /><br />
							<h4>パイピングの継ぎ目</h4>
							<p>革のパイピングを入れて縫う製法（玉縫い）の商品は、 鞄の表裏側ともに、パイピングを継ぐ箇所で僅かな 空きが生じます。</p>
						</div>
						<div class="clear-both"></div>
					</div>
					
					<br/>
					
					<div class="iblk">
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img24.png" width="100%" />
							<br /><br />
							<h4>革質による形</h4>
							<p>一枚の革でも革質は均一ではありません。 盤面で革質が異なると出来上がりも均一な形を保つ ことが難しい場合があります。</p>
						</div>
						<div class="box50">
							<img class="sp-img-wmax" src="images/attention/img25.png" width="100%" />
							<br /><br />
							<h4>底面のつくり</h4>
							<p>底鋲を付けている鞄でもコバ面は地面に接地しま す。底鋲は地面から離す役割ではなく、鞄全体の型 崩れを防ぐためとして取り付けています。 構造的にもコバは最も丈夫であることと重量を面 で支える形になっています。</p>
						</div>
						<div class="clear-both"></div>
					</div>
					<br/>
					<div class="iblk">
						<img class="sp-img-wmax" src="images/attention/img26.png" />
					</div>
					
					<br/><br/><br/><br/>
					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
