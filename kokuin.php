<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-kokuin has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">HOME</a> > HERZ刻印について
				</p>
			</div>

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">

				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						HERZ刻印について
						<span class="header-eng">-ABOUT ENGRAVED-</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br/><br/>
				
				<div class="cblk-1">
					
					<div class="iblk">
						<img class="w100p sp-img-wmax" src="images/updt-kokuin/img1.jpg" />	
					</div>
				
					<br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							HERZ刻印の仕様変更について(2012年11月1日～)
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>
							HERZ刻印の仕様を変更しました。今後、製作する商品については、下記写真のようにHERZロゴの下にMADE IN JAPANの刻印が入る様になります。 
						</p>
						
						<img class="float-left  mb30 mr60 sp-img-nofloat sp-img-wmax w45p" src="images/updt-kokuin/img2.jpg" />

						<img class="float-left  mb30 sp-img-nofloat sp-img-wmax w45p" src="images/updt-kokuin/img3.jpg" />
						
						<div class="clear-both"></div>
						
						<p>
							各商品ページの写真は今までの刻印で掲載されているモデルもありますが、出来上がりは新しい刻印が入ります。<br/>
							<strong class="fcred">※一部商品によっては、配置の関係上、HERZロゴのみの刻印になる商品もあります。</strong> <br/>
							<strong class="fcred">※刻印のデザインを選びことは出来ません。</strong>
						</p>
						
						<br/>
						
						<img class="w100p" src="images/updt-common/herz-online-banner.jpg" />
						
						<br/><br/>
						
					</div>
					
				</div>
				

			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
