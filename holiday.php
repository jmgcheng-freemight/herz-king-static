<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-holiday has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">HOME</a> > 年末年始お休みのお知らせ
				</p>
			</div>

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						年末年始お休みのお知らせ
						<span class="header-eng">-HOLIDAY-</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br/><br/>
				
				<div class="cblk-1">
				
				
					<div class="iblk">
						<img class="w100p sp-img-wmax" src="images/updt-holiday/img1.jpg" />	
						<br/><br/><br/>
						<p>
							いつもHERZをご愛顧頂き、誠にありがとうございます。<br/>
							誠に勝手ながら下記の期間中、年末年始による休業とさせていただきます。<br/>
							各店舗ごとで休業期間が異なりますので、事前にご確認を宜しくお願いいたします。
						</p>
						<p>
							<strong>【オンラインショップの休業期間 ： 10月6日（火） ～ 10月7日（水）】</strong><br/>
							休業中も通常通り、お買い物が可能です。<br/>
							ただ、ご注文確認やお問い合わせ等のご返事は10月8日（木）以降、順次ご返信となります。<br/>
							予めご理解の程、宜しくお願い致します。
						</p>
					</div>
					
					<br/><br/>
					
					<div class="iblk iblk-20">
						<ul>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>本店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<strong class="fcred">10月6日（火）</strong> ※10月8日（木）より通常営業となります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>Organ</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<strong class="fcred">10月6日（火）～ 10月7日（水）</strong> ※10月8日（木）より通常営業となります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>FACTORY SHOP</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<strong class="fcred">10月6日（火）</strong> ※10月8日（木）より通常営業となります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>RESO.</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<strong class="fcred">10月6日（火）</strong> ※10月8日（木）より通常営業となります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>大阪店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<strong class="fcred">10月6日（火）</strong> ※10月8日（木）より通常営業となります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<p>
										<strong>仙台店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<strong class="fcred">10月6日（火）</strong> ※10月8日（木）より通常営業となります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item" >
								<div class="col col-1">
									<p>
										<strong>名古屋店</strong>
									</p>
								</div>
								<div class="col col-2">
									<p>
										<strong class="fcred">10月6日（火）～ 10月7日（水）</strong> ※10月8日（木）より通常営業となります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
						</ul>
					</div>
					
					<br/><br/>
					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/updt-holiday/img2.jpg" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />本店</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<img src="images/updt-holiday/img3.jpg" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />Organ</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<img src="images/updt-holiday/img4.jpg" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />FACTORY SHOP</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<img src="images/updt-holiday/img5.jpg" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />RESO.</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<img src="images/updt-holiday/img6.jpg" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />大阪店</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<img src="images/updt-holiday/img7.jpg" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />仙台店</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<img src="images/updt-holiday/img8.jpg" />
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png" />名古屋店</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
					
					<div class="iblk w100p content-pc">
						<br/><br/><br/><br/>
						<img src="images/updt-common/herz-online-banner.jpg" />
					</div>
					
					
					<br/><br/><br/>
					
					
					
					
					
				</div>
				
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
