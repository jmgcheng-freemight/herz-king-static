<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						お手入れ方法
						<span class="header-eng">HOW TO CARE</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				<br /><br />
				<div class="ablk-1">
					
					
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/care/img1.png">	
					</div>					
					
					<br/><br/><br/><br/>
					<header class="header-content">
						<h2 class="h">
							革は乾燥すると弱くなります。そのままにしておくとひび割れ・やぶれなどに発展します
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					<br /><br />
					<div class="iblk-0">
						<p>
							HERZで通常使用しているラティーゴ・スターレと呼んでいるオリジナルレザーは、製品の特性上、適度な革の張り、硬さを持たす為、革をなめす際に含ませるオイル、グリースの量を調整して仕上げを行っています。
			<br/>
			このため、製品をご使用いただく上で革が乾燥してくることがあります。
						</p>

					</div>
					
					
					<br/><br/><br/><br/>
					<header class="header-content">
						<h2 class="h">
							オイルメンテナンス
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					<br /><br />
					<div class="iblk-0">
						<p>
						まず製品をご使用いただく前に、革の表面全体に保革用のオイル（ラナパー、ミンクオイル等）を塗っていただくことをお勧めします。
						<br/>
						また、ご使用いただく中で表面が乾燥した際にも保革用オイルをご使用下さい。オイルを塗る度に、色が濃くなり表面にツヤが出てくる革独特の変化をお楽しみいただけます。
						</p>
					</div>
					
					
					<div class="iblk-0">
						
						<div class="rlistf-items">
							<div class="box50"> <img src="images/care/img2.png" alt=""> </div>
							<div class="box50 padLeft1"> 
								<h3 class="font-ryumin mb20">
									1.ブラッシング、乾拭き
								</h3>
								<p class="p-adj">
									使用後の鞄をブラッシングまたは乾拭きして、鞄に付いた ホコリや汚れを落とします
								</p>
							</div> 
						</div>
						
						<div class="rlistf-items">
							<div class="box50"> <img src="images/care/img3.png" alt=""> </div>
							<div class="box50 padLeft1"> 
								<h3 class="font-ryumin mb20">
									2.オイル塗布
								</h3>
								<p class="p-adj">
									ごく少量を柔らかい布など（ラナバーの場合は付属のスポンジ）にとり、 薄く全体に均等に延ばすように塗ります。 初めてオイルを塗る方は、目立たない底部などから塗って頂くと良いです。
								</p>
							</div> 
						</div>
						
						<div class="rlistf-items">
							<div class="box50"> <img src="images/care/img4.png" alt=""> </div>
							<div class="box50 padLeft1"> 
								<h3 class="font-ryumin mb20">
									3.鞄に油分を浸透させる
								</h3>
								<p class="p-adj">
									オイル塗布直後は鞄が多少べたついています。油分が浸透するまで鞄をおいておきましょう。
								</p>
							</div> 
						</div>
						
						<div class="clear"></div>
					</div>

					
					
					
					
					
					<header class="header-content">
						<h3>
							HERZの道具
						</h3>
						<div class="accent-1 mt15"></div>
					</header>
					<br /><br />
					<div class="iblk-0">
						<p class="f15 p-adj">
							クラフトで一般的な用途で使われている道具たちは、HERZの鞄を作るために使うとだいたい耐えられません。厚い革と太い糸で武骨に仕上げるHERZの鞄作りを実現するには、通常とは一線を画した、ある意味、強引な道具の使い方をする時も間々あります。
						</p>
						<p class="f15 p-adj">
							その作業に「普通は」使わないアイテムから骨董市や金物屋さんで見つけた掘り出し物、ホームセンターでよくある工具、はたまた自分で加工して使いやすくした逸品まで作り手の「手」となる工具を一部ご紹介します。
						</p>
					</div>

					
					
					<div class="iblk-0">
					
						<div class="l-desc rlistf mb30">
							<div class="rlistf-items mb50 mt40">
								<h3 class="font-ryumin mb30">
									ハンマー
								</h3>
								<div class="box50"> <img src="images/middle/row2-middle-img1.png" alt=""> </div>
								<div class="box50 padLeft1">
									<p class="p-adj">
										カシメやホックを留めたり、革パーツどうしの糊づけを定着させたり。特に金具を留める際のハンマーは、HERZの分厚い革を挟んで打つにはかなりの力が必要です。<br>
										工程によって、小さいものやプラスティックのものなど使い分けています。
									</p>
								</div>
							</div>
							<div class="rlistf-items mb50 mt40">
								<h3 class="font-ryumin mb30">
									ポンチ、楕円、R取り
								</h3>
								<div class="comparison">
									<div class="col2 ohidden">
										<div class="col2-items">
											<div class="comparison-thumb"><img src="images/middle/row2-middle-img2.png" alt=""></div>
										</div> 
										<div class="col2-items"> 
											<div class="comparison-thumb"><img src="images/middle/row2-middle-img3.png" alt=""></div>
										</div> 
									</div> 
								</div>
								<p class="fontsmall1">
									穴を開けたり、革の端をカットしたりする時に使います。大きさと形状によって、十数種類あり、仕上がった時の見た目を左右する重要なパーツです。
								</p>
							</div>
							<div class="rlistf-items mb50 mt40">
								<h3 class="font-ryumin mb30">
									トウフ
								</h3>
								<div class="box50"> <img src="images/middle/row2-middle-img4.png" alt=""> </div>
								<div class="box50 padLeft1">
									<p class="p-adj">
										金具を留めるために下に当てる金属製の台。直方体が多いことからいつの間にかその名前に。まさに縁の下の力持ちといったアイテム。
									</p>
								</div>
							</div>
							<div class="rlistf-items mb50 mt40">
								<h3 class="font-ryumin mb30">
									打ち棒
								</h3>
								<div class="box50"> <img src="images/middle/row2-middle-img5.png" alt=""> </div>
								<div class="box50 padLeft1">
									<p class="p-adj">
										カシメやホックを留める際に、金具に当てて打つためのもの。長年打ち込むと、上部が変形し、きのこような形になります。
									</p>
								</div>
							</div>
							<div class="rlistf-items mb50 mt40">
								<h3 class="font-ryumin mb30">
									罫書き、デバイダー
								</h3>
								<div class="comparison">
									<div class="col2 ohidden">
										<div class="col2-items">
											<div class="comparison-thumb"><img src="images/middle/row2-middle-img6.png" alt=""></div>
										</div>
										<div class="col2-items">
											<div class="comparison-thumb"><img src="images/middle/row2-middle-img7.png" alt=""></div>
										</div>
									</div>
								</div>
								<p class="fontsmall1">
									革を裁断したり、金具を取り付けるための印をつける道具。デバイダーは、平行線を引く時に使うけがき。骨董市で調達する作り手も多いです。
								</p>
							</div>
							<div class="rlistf-items mb50 mt40">
								<h3 class="font-ryumin mb40">
									ピーラー、鉋
								</h3>
								<div class="box50"> <img src="images/middle/row2-middle-img8.png" alt=""> </div>
								<div class="box50 padLeft1">
									<p class="p-adj">
										革の縁をななめに、時に垂直に削る時の道具。ななめの削りは特にウォッシュ加工など、ナチュラルな雰囲気の鞄やカジュアル感のある鞄によく使います。
									</p>
								</div> 
							</div>
							<div class="rlistf-items mb50 mt40">
								<h3 class="font-ryumin mb30">
									やっとこ、ペンチ
								</h3>
								<div class="box50"> <img src="images/middle/row2-middle-img9.png" alt=""> </div>
								<div class="box50 padLeft1"> 
									<p class="p-adj">
										革パーツを糊付けして貼りあわせる時などに使用します。革が傷つかないよう、先端を丸く削ったり革を当てたり各々で加工しています。
									</p>
								</div> 
							</div>
							<div class="rlistf-items mb50 mt40">
								<h3 class="font-ryumin mb40">
									ガリガリ、糊ケース
								</h3>
								<div class="box50"> <img src="images/middle/row2-middle-img10.png" alt=""> </div>
								<div class="box50 padLeft1"> 
									<p class="p-adj">
										糊をつけやすくするために革の床面を「ガリガリ」加工する道具。金属やすりだったり、金属ブラスだったり、謎の骨董品だったり。革を貼りあわせる糊の専用ケースは、日本では珍しいようです。
									</p>
								</div> 
							</div>
						</div>					
					</div>	

					<header class="header-content">
						<h3>
							博多工房について
						</h3>
						<div class="accent-1 mt15"></div>
					</header>
					<br /><br />
					<div class="iblk-0">
						<div class="image-wrapper mt30">
							<img src="images/middle/row4-middle-img1.png" alt="">
						</div>
						<div class="mid-row4-cat2">
							<img class="mr43" src="images/middle/row4-middle-img2.png" alt="" >
							<img class="mr43" src="images/middle/row4-middle-img3.png" alt="" >
							<img class="mr43-2" src="images/middle/row4-middle-img4.png" alt="" >
						</div>
						<p class="f15 mt20">
							感度の高い場所に工房を構えることで、HERZらしさを磨いていく。
						</p>						
						<p class="f15 mt20">
							HERZ(ヘルツ)は、博多にも工房を構えています。<br>
							博多にあって、渋谷にないもの。<br>
							それは都会の便利さと自然の豊かさが混在していることです。感度の高い街々に工房を構えることで、色々なモノ・コトを吸収し、HERZらしさを磨いていく。変わらない心でモノ作りをするために必要なことだと思っています。			
						</p>						
					</div>
					
					<div class="iblk-0">
					
						<div class="comp-profile-row3">
							<div class="comp-table">
								<div class="table-1">
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												住所
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												福岡県福岡市博多区博多駅前3-16-10
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left">
											<p class="f13">
												定休日
											</p>
										</div>
										<div class="table-cell table-cell-right">
											<p class="f13">
												日曜・祭日
											</p>
										</div>
									</div>
									<div class="table-row-1">
										<div class="table-cell table-cell-left table-cell-last">
											<p class="f13">
												営業時間
											</p>
										</div>
										<div class="table-cell table-cell-right table-cell-last">
											<p class="f15 mb20">
												平日 11:00-19:00
											</p>
											<p class="f15 mb20">
												土曜 11:00-19:00
											</p>
											<p class="f15 mb20 font-weight-bold">
												商品の販売・修理の受付等は対応しておりません。
											<p class="f15">
											<p>
												時間はその日の製作時間によって変わります。							
											</p>
										</div>
									</div>
									

								</div>
							</div>
						</div>				

					</div>
					
					
					
					
					
						
				</div>
				
				
				
			</div>
			
					<br /><br /><br /><br />
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
