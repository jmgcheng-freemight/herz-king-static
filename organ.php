<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-after-care has-aside">
			
			<div class="iblk iblk-19">
				<p class="breadcrumb">
					<a class="anc link-1" href="#">鞄・バッグ｜HERZトップ</a> > 会社概要
				</p>
			</div>
			

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar.php');
			?>

			<div class="site-content">
				
				
				<header class="header-content">
					<div class="accent-1 mb10"></div>
					<h2 class="h">
						Organ イタリアンレザー
						<span class="header-eng">ITALIAN LEATHER</span>
					</h2>
					<div class="accent-1 mt10"></div>
				</header>
				
				<br /><br />
				
				<div class="cblk-1">
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/organ/organ-cover.png" />	
						<br /><br />
						<p>Organモデルで使用されている革は植物タンニンなめしのイタリアンレザーを使用しています。トラ、色ムラなどの個体差をあえて隠さず、革素材本来の魅力を活かした仕上げです。多くのタンナーが存在する革の街､トスカーナ州サンタクローチェで培われた伝統的な製法で作られており、加工に時間がかかり脂が浸透しにくい反面、使い込んだ時に独特の色艶がでることと、一旦加脂したオイルが抜けにくいという特徴があります。</p>
						<p>Organでは、鞄のデザインによって、数種類のイタリア革を使い分けています。</p>
					</div>
					
					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h">
							ミネルバボックス
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<img class="sp-img-wmax" src="images/organ/organ-image00.png" />	
						<br /><br />
						<p>
							ドライドラムで革をほぐすことで出来るシボと呼ばれる皺と柔らかさが特徴。<br>
   部位による革質差により、それぞれシボの入り具合が異なり、豊かな表情が生まれます。
						</p>
					</div>
					
					
					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/organ/organ-thumb00.png">
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">ミネルバボックスについて詳しく見る</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>	

					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h">
							ミネルバリスシオ
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>

					<div class="iblk">
						<img class="sp-img-wmax" src="images/organ/organ-image01.png" />
						<br /><br />
						<p>リスシオとは、イタリア語で滑らかという意味があり、革の表情は植物タンニンなめし特有の温かみ、透明感を感じさせるスムースな仕上がりになっています。</p>
					</div>
					
					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/organ/organ-thumb02.png">
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">リバースについて詳しく見る</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>	
					
					

					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h">
							ユーフラテ
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>

					<div class="iblk">
						<img class="sp-img-wmax" src="images/organ/organ-image03.png" />
						<br /><br />
						<p>使い込まれた革のような独特の表情が特徴。<br>
「ウェットバック」と呼ばれる水を入れたドラムの中で繊維をほぐしながらシボを出し、乾燥後に銀面をガラス玉で擦る「グレージング加工」を施すことで艶を出しています。</p>
					</div>
					
					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/organ/organ-thumb03.png">
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">ユーフラテについて詳しく見る</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>	
					
					

					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h">
							ホリデイ
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>

					<div class="iblk">
						<img class="sp-img-wmax" src="images/organ/organ-image04.png" />
						<br /><br />
						<p>柔らかく揉んで、自然に生まれるシボやシワの不規則さが革本来の個性を感じる革。<br>
エイジングのスパンが長く、ゆっくりと変化を楽しめるのが特徴です。</p>
					</div>
					
					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/organ/organ-thumb04.png">
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">ホリデイについて詳しく見る</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>	
					

					<br/><br/><br/><br/>

					<header class="header-content">
						<h2 class="h">
							ダコタ
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>

					<div class="iblk">
						<img class="sp-img-wmax" src="images/organ/organ-image05.png" />
						<br /><br />
						<p>強度が高く厚みのある力強さ、平滑で張りのあるテクスチャーが特徴。<br>
こちらもエイジングのスパンが長く、上品な風合いをゆっくり味わえます。</p>
					</div>
					
					
					<div class="iblk iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/organ/organ-thumb05.png">
								<a class="anc link-1" href="#"><img class="ico-anc" src="images/updt-common/ico-arrow-right-black-2.png">ダコタについて詳しく見る</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>	
					
					
					<br/><br/><br/><br/>
					
					<header class="header-content">
						<h2 class="h">
							イタリアンレザーのお手入れ・色移りについて
						</h2>
						<div class="accent-1 mt15"></div>
					</header>
					
					<br/><br/>
					
					<div class="iblk">
						<p>お手入れについては、革に上質な油分が十分に含まれておりますので、空拭きのみで問題ありません。表面に大きな加工をしていないので、小さなキズやシワが隠れにくく、爪傷が付きやすいという特徴がありますが、指で軽く揉み込んであげるとキズが目立ちにくくなります。<br>
  <br>
天然の染料を使用し、革の表面加工を抑えていますので、革の表面や内側の摩擦や水ぬれによる衣類等への色移りがあります。特に薄い色の衣服でのご使用はご注意ください。</p>
<br /><br /><br />
<a href="">Organモデル販売ページ</a>
<br /><br /><br />
<img class="sp-img-wmax" src="images/organ/organ-btm-image.png" />

					</div>
					
					
<br/><br/><br/><br/>
					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
