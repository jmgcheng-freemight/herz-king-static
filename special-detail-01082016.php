<?php
	include('includes/updt-header.php');
?>


		<div id="site-main" class="p-special-detail has-aside">
			
			<div class="ablk-1 header-breadcrumb">
				<p class="breadcrumb">
					<a class="anc link-3" href="#">HOME</a> > 特集
				</p>
			</div>

			<!-- site-main should have has-aside class if it has a sidebar -->
			<?php
				include('includes/updt-sidebar-special.php');
			?>

			<div class="site-content">
				
				<div class="ablk-1 site-content-breadcrumb">
					<p class="breadcrumb">
						<a class="anc link-3" href="#">HOME</a> > 特集
					</p>
				</div>
				
				<div class="ablk-1 special-detail">
					<header class="header-content">
						<div class="accent-1 mb10"></div>
						<h2>
							見出し1が入ります
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					
					<div class="iblk-9">
						<article>
							<div class="article-feature-image taped">
								<img class="anc-img-tape tape-left" src="images/updt-common/ico-tape-1.png" />
								<img class="anc-img-tape tape-right" src="images/updt-common/ico-tape-2.png" />
								<a class="anc-img" href="#">
									<img src="images/updt-special-list/article-img1.jpg" />
								</a>
							</div>
							<h3 class="header-category">
								2015/00/00 &nbsp;&nbsp;
								<a class="anc link-2" href="#"><img src="images/updt-common/ico-folder-brown.png" />商品企画</a>
							</h3>
							<div class="iblk-11 social-box">
								<nav class="site-menu social-menu">
									<ul>
										<li class="li-item">
											<a href="#">
												<img src="images/updt-common/ico-fb-2.png" />
											</a>
										</li>
										<li class="li-item">
											<a href="#">
												<img src="images/updt-common/ico-twitter-2.png" />
											</a>
										</li>
										<li class="li-item last-item">
											<a href="#">
												<img src="images/updt-common/ico-google-plus.png" />
											</a>
										</li>
										<div class="clear-both"></div>
									</ul>
									<div class="clear-both"></div>
								</nav>
							</div>
						</article>
					</div>
					
					<div class="iblk-12">
						<header class="header-content">
							<h2>
								目次
							</h2>
							<div class="accent-1 mt10"></div>
						</header>
						<ul>
							<li>
								目次1が入ります。
								<ul>
									<li>
										目次1の子要素1
									</li>
									<li>
										目次1の子要素1
									</li>
									<li>
										目次1の子要素3
									</li>
								</ul>
							</li>
							<li>
								目次2が入ります。
							</li>
							<li>
								目次3が入ります。
							</li>
							<li>
								目次4が入ります。
							</li>
							<li>
								目次5が入ります。
							</li>
						</ul>
					</div>
					
					<br/><br/><br/><br/>
					
					<div class="iblk-0">
						<h3>
							見出し2
						</h3>
						<div class="accent-1 mt15 mb15"></div>
						<p>
							ご注文いただいた商品と一緒にペーパークラフトの型紙を送ります。型紙には説明文も記載していますので、その内容に沿って、工作してみて下さい！！定規とカッターと糊があれば完成します！！
						</p>
						
						<br/><br/><br/><br/>
						
						<h3>
							見出し2
						</h3>
						<div class="accent-1 mt15 mb15"></div>
						<img class="w100p" src="images/updt-special-detail/img-1.jpg" />
						
						<br/><br/><br/><br/>
						
						<h3>
							見出し3
						</h3>
						
						<br/><br/>
						
						<p>
							テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
						</p>
						<p>
							テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
						</p>
						
						<img class="float-left mr30 mb20 w50p" src="images/updt-special-detail/img-2.jpg" />
						
						<p>
							テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。 テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
						</p>
						<p>
							テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。 テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
						</p>
						
						<div class="clear-both"></div>
					</div>
					
					<header class="header-content">
						<h2>
							見出し2
						</h2>
						<div class="accent-1 mt10"></div>
					</header>
					
					<div class="iblk-13">
						<ul>
							<li class="li-item">
								<article>
									<div class="article-feature-image">
										<a class="anc-img" href="#">
											<img src="images/updt-special-detail/img-3.jpg" />
										</a>
									</div>
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
								</article>								
							</li>
							<li class="li-item">
								<article>
									<div class="article-feature-image">
										<a class="anc-img" href="#">
											<img src="images/updt-special-detail/img-4.jpg" />
										</a>
									</div>
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
								</article>								
							</li>
							
							<div class="clear-both"></div>
						</ul>
					</div>
					
					<div class="iblk-14">
						<header class="header-content">
							<h2>
								見出し2
							</h2>
							<div class="accent-1 mt10"></div>
						</header>
						
						<img class="float-right ml30 mb20 w50p" src="images/updt-special-detail/img-2.jpg" />
						
						<p>
							テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。 テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
						</p>
						<p>
							テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。 テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
						</p>
						<div class="clear-both"></div>
					</div>
					
					<br/><br/><br/><br/><br/>
					
					
					<div class="iblk-15">
						<ul>
							<li class="li-item">
								<div class="col col-1">
									<img class="" src="images/updt-special-detail/img-5.jpg" />
								</div>
								<div class="col col-2">
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item">
								<div class="col col-1">
									<img class="" src="images/updt-special-detail/img-6.jpg" />
								</div>
								<div class="col col-2">
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<div class="col col-1">
									<img class="" src="images/updt-special-detail/img-7.jpg" />
								</div>
								<div class="col col-2">
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
					
					<br/><br/><br/><br/><br/>
					
					
					<div class="iblk-16 comment-list">
						<ul>
							<li class="li-item sender">
								<div class="col col-1">
									<p>
										名前が入ります。
									</p>
								</div>
								<div class="col col-2">
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item receiver">
								<div class="col col-1">
									<p>
										名前が入ります。
									</p>
								</div>
								<div class="col col-2">
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item sender">
								<div class="col col-1">
									<p>
										名前が入ります。
									</p>
								</div>
								<div class="col col-2">
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item receiver">
								<div class="col col-1">
									<p>
										名前が入ります。
									</p>
								</div>
								<div class="col col-2">
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。<br/>
										テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item sender">
								<div class="col col-1">
									<img class="" src="images/updt-special-detail/img-8.jpg" />
									<p>
										名前が入ります。
									</p>
								</div>
								<div class="col col-2">
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<li class="li-item receiver">
								<div class="col col-1">
									<img class="" src="images/updt-special-detail/img-9.jpg" />
									<p>
										名前が入ります。
									</p>
								</div>
								<div class="col col-2">
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
									<p>
										テキストが入ります。テキストが入ります。テキストが入ります。
									</p>
								</div>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
					
					
					<h3>
						見出し3
					</h3>
					<br/><br/>
					
					<div class="iblk-0">
						<ul>
							<li>箇条書きリスト1の体裁が入ります。</li>
							<li>箇条書きリスト2の体裁が入ります。</li>
							<li>箇条書きリスト3の体裁が入ります。</li>
							<li>箇条書きリスト4の体裁が入ります。</li>
							<li>箇条書きリスト5の体裁が入ります。</li>
						</ul>
						<ol>
							<li>番号付きリスト</li>
							<li>番号付きリスト</li>
							<li>番号付きリスト</li>
							<li>番号付きリスト</li>
							<li>番号付きリスト</li>
						</ol>
						<p>
							<strong>太字の体裁はこの体裁。wordpressで「B」を選択した時の体裁</strong>
						</p>
						<blockquote>
							<p>
								引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。
								引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。
							</p>
							<p>
								引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。引用文の体裁はこの様な体裁になります。
							</p>						
						</blockquote>
						<br/>
						<p>
							ご注文いただいた商品と一緒にペーパークラフトの型紙を送ります。型紙には説明文も記載していますので、その内容に沿って、工作してみて下さい！！  <a href="#" class="anc link-2">文章中にリンクが貼られるとこんな体裁になります</a>     定規とカッターと糊があれば完成します！！
						</p>
						<p>
							<del>
								打ち消し線の場合の表示
							</del>
						</p>
						<h4>
							見出し4
						</h4>
						<br/>
						<p>
							ご注文いただいた商品と一緒にペーパークラフトの型紙を送ります。型紙には説明文も記載していますので、その内容に沿って、工作してみて下さい！！定規とカッターと糊があれば完成します！！
						</p>

						<h5>
							見出し5
						</h5>
						<br/>
						<p>
							ご注文いただいた商品と一緒にペーパークラフトの型紙を送ります。型紙には説明文も記載していますので、その内容に沿って、工作してみて下さい！！定規とカッターと糊があれば完成します！！
						</p>

						<h6>
							見出し6
						</h6>
						<br/>
						<p>
							ご注文いただいた商品と一緒にペーパークラフトの型紙を送ります。型紙には説明文も記載していますので、その内容に沿って、工作してみて下さい！！定規とカッターと糊があれば完成します！！
						</p>

						<p>
							<a class="anc link-2" href="#"><img src="images/updt-common/ico-arrow-right-black.png">作り手が教えるメンテナンス</a>
						</p>
					</div>
					
					
					
					<h3>
						バックナンバー
					</h3>
					<div class="accent-1 mt10"></div>
					
					
					
					<br/>
					<div class="iblk-17">
						<ul>
							<li class="li-item">
								<img src="images/updt-special-detail/img-10.jpg">
								<a class="anc link-2" href="#"><img src="images/updt-common/ico-arrow-right-black-2.png">創業者とカバン作ろう第２弾 ～作り手：ナカムラ編 vol.1～</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<img src="images/updt-special-detail/img-11.jpg">
								<a class="anc link-2" href="#"><img src="images/updt-common/ico-arrow-right-black-2.png">創業者とカバン作ろう第２弾 ～作り手：ナカムラ編 vol.2～</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
					<br/><br/>
					
					<div class="iblk-8 related-links">
						<h5>
							関連リンク
						</h5>
						<ul>
							<li class="li-item">
								<img src="images/updt-special-detail/img-12.jpg">
								<a class="anc link-2" href="#"><img src="images/updt-common/ico-arrow-right-black-2.png">HERZ仙台店 in 南相馬</a>
								<div class="clear-both"></div>
							</li>
							<li class="li-item last-item">
								<img src="images/updt-special-detail/img-13.jpg">
								<a class="anc link-2" href="#"><img src="images/updt-common/ico-arrow-right-black-2.png">創業者とカバン作ろう</a>
								<div class="clear-both"></div>
							</li>
							<div class="clear-both"></div>
						</ul>
					</div>
					
					<br/><br/>
					
					<div class="iblk-11 social-box">
						<nav class="site-menu social-menu">
							<ul>
								<li class="li-item">
									<a href="#">
										<img src="images/updt-common/ico-fb-2.png" />
									</a>
								</li>
								<li class="li-item">
									<a href="#">
										<img src="images/updt-common/ico-twitter-2.png" />
									</a>
								</li>
								<li class="li-item last-item">
									<a href="#">
										<img src="images/updt-common/ico-google-plus.png" />
									</a>
								</li>
								<div class="clear-both"></div>
							</ul>
							<div class="clear-both"></div>
						</nav>
					</div>
					
					<br/>
					
					<hr/>
					
					<br/><br/>
					
					<div class="iblk-18 fb-app">
						<img src="images/updt-common/fb-app-1.jpg" />
					</div>
					
					<br/><br/>
					
				</div>
				
			</div>
			
			
			<div class="clear-both"></div>
		</div>
		
		
<?php
	include('includes/updt-footer.php');
?>
